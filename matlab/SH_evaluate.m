%% evaluate segment-based / sub-path based method

d1 = importdata('../java/prediction-sampled');

size(d1)

gnd_all = d1(:,1);
size(gnd_all)
segment = d1(:,2);

mae = sum(abs(gnd_all-segment)) / length(gnd_all);
mre = sum(abs(gnd_all-segment)) / sum(gnd_all);
rmse = sqrt( sum((gnd_all-segment).^2) / length(gnd_all) );
medianE = median(abs(gnd_all-segment));
medianMRE = median(abs(gnd_all-segment)./ gnd_all);


subpath = d1(:,3);

mae2 = sum(abs(gnd_all-subpath)) / length(gnd_all);
mre2 = sum(abs(gnd_all-subpath)) / sum(gnd_all);
rmse2 = sqrt( sum((gnd_all-subpath).^2) / length(gnd_all) );
medianE2 = median(abs(gnd_all-subpath));
medianMRE2 = median(abs(gnd_all-subpath) ./ gnd_all);

fprintf('Segment\t%g\t%g\t%g\t%g\n', mae, mre, medianE, medianMRE);
fprintf('Subpath\t%g\t%g\t%g\t%g\n', mae2, mre2, medianE2, medianMRE2);

size(gnd_all)

figure();
l1 = cdfplot(gnd_all);
set(l1, 'linewidth', 3);
hold on;
l2 = cdfplot(segment);
set(l2, 'color', 'red', 'linestyle', '--', 'linewidth', 3);
set(gca, 'xscale', 'log');
legend({'Ground truth', 'Prediction'}, 'fontsize', 16, 'location', 'best');
xlabel('Trip travel time (s)');
ylabel('CDF');
title(sprintf('CDF of %d trips', length(gnd_all)));


%% evaluate on complete-trip-based method
d = importdata('../../../dataset/SH_taxi_eval-sampled');
% [~, idx] = ismember(c(:,4), d(:,7));
% d = d(idx, :);

gnd_all = d(:,1);
% avg = d(:,2);
avg = ones(size(d(:,1))) * 61;
medi = d(:,3);
relaP = d(:,4);
absP = d(:,5);

mae1 = sum(abs(gnd_all-medi)) / length(gnd_all);
mre1 = sum(abs(gnd_all-medi)) / sum(gnd_all);
rmse1 = sqrt( sum((gnd_all-medi).^2) / length(gnd_all) );
medianE1 = median(abs(gnd_all-medi));
medianMRE1 = median(abs(gnd_all-medi) ./ gnd_all);


mae2 = sum(abs(gnd_all-avg)) / length(gnd_all);
mre2 = sum(abs(gnd_all-avg)) / sum(gnd_all);
rmse2 = sqrt( sum((gnd_all-avg).^2) / length(gnd_all) );
medianE2 = median(abs(gnd_all-avg));
medianMRE2 = median(abs(gnd_all-avg) ./ gnd_all);


maeR = sum(abs(gnd_all - relaP)) / length(gnd_all);
mreR = sum(abs(gnd_all - relaP)) / sum(gnd_all);
rmseR = sqrt(sum((gnd_all - relaP).^2) / length(gnd_all) );
medianER = median(abs(gnd_all - relaP));
medianRER = median(abs(gnd_all - relaP) ./ gnd_all);

maeA = sum(abs(gnd_all - absP)) / length(gnd_all);
mreA = sum(abs(gnd_all - absP)) / sum(gnd_all);
rmseA = sqrt(sum((gnd_all - absP).^2) / length(gnd_all) );
medianEA = median(abs(gnd_all - absP));
medianREA = median(abs(gnd_all - absP) ./ gnd_all);


% nn = d(:,6);
% figure();
% l = cdfplot(nn);
% set(l, 'color', 'blue', 'linewidth', 3);
% set(gca, 'xscale', 'log');
% xlabel('Number of neighbors');
% ylabel('CDF');
% title(sprintf('CDF of %d trips', length(gnd)));

fprintf('Median\t%g\t%g\t%g\t%g\n', mae1, mre1, medianE1, medianMRE1);
fprintf('AVG\t%g\t%g\t%g\t%g\n', mae2, mre2, medianE2, medianMRE2);
fprintf('TEMP_rela\t%g\t%g\t%g\t%g\n', maeR, mreR, medianER, medianRER);
fprintf('TEMP_abs\t%g\t%g\t%g\t%g\n', maeA, mreA, medianEA, medianREA);


figure();
l1 = cdfplot(gnd_all);
set(l1, 'linewidth', 3);
hold on;
l2 = cdfplot(avg);
l3 = cdfplot(absP);
set(l2, 'color', 'red', 'linestyle', '--', 'linewidth', 3);
set(l3, 'color', [0, 0.8, 0], 'linewidth', 3, 'linestyle', '--');
set(gca, 'xscale', 'log', 'fontsize', 16);
legend({'Ground truth', 'AVG', 'RELA-abs'}, 'fontsize', 16, 'location', 'best');
xlabel('Trip travel time (s)', 'fontsize', 16, 'linewidth', 3);
ylabel('CDF', 'fontsize', 16);
title(sprintf('CDF of %d trips', length(gnd_all)));
title('');
fn = 'cdf-biased.eps';
print('-dpsc2', fn);
system(['epstopdf ', fn]);
delete(fn);



%{ 
% error w.r.t trip length
nd = [d, abs(gnd-medi) ./ gnd, abs(gnd-segment) ./ gnd, abs(gnd-relaP) ./ gnd, abs(gnd-absP) ./ gnd ];
[~,idx] = sort(nd(:,1));
nd = nd(idx, :);
figure();
xlabel('Trip time (seconds)', 'fontsize', 12);
ylabel('Relative Error', 'fontsize', 12);

subplot(2,2,1);
plot(nd(:,1), nd(:,7), 'linewidth', 1);
title('Median Estimator', 'fontsize', 12);
set(gca, 'ylim', [0, 15]);
subplot(2,2,2);
plot(nd(:,1), nd(:,8), 'linewidth', 1);
title('AVG Estimator', 'fontsize', 12);
set(gca, 'ylim', [0, 15]);
subplot(2,2,3);
plot(nd(:,1), nd(:,9), 'linewidth', 1);
title('TEMP-rela Estimator', 'fontsize', 12);
set(gca, 'ylim', [0, 15]);
subplot(2,2,4);
plot(nd(:,1), nd(:,10), 'linewidth', 1);
title('TEMP-abs Estimator', 'fontsize', 12);
set(gca, 'ylim', [0, 15]);
%}



%%  evaluate the Baidu map service

% raw data -- time, AVG, Median, TEMP_rela, TEMP_abs, #neighbors, ID
e = importdata('../../../dataset/SH_taxi_eval-sampled');

% Baidu map data -- ID, map duration, map distance
f = importdata('../../../dataset/SH_taxi_trip_Baidu');

% ID sets
[ids, idx] = intersect(e(:,7), f(:,1));

if sum(ids - e(idx,7)) == 0 && sum(ids - f(:,1)) == 0
    gnd_all = e(idx, 1);
    bdmap = f(:, 2);
    
    mae = sum(abs(gnd_all-bdmap)) / length(gnd_all);
    mre = sum(abs(gnd_all-bdmap)) / sum(gnd_all);
    rmse = sqrt( sum((gnd_all-bdmap).^2) / length(gnd_all) );
    medianE = median(abs(gnd_all-bdmap));
    medianRE = median(abs(gnd_all-bdmap) ./ gnd_all);

    dist = e(idx, 2);
    p = polyfit( log(gnd_all), log(dist), 1);
    ybar = exp(polyval( p, log(dist) ) );
    
    
    
    mae2 = sum(abs(gnd_all-ybar)) / length(gnd_all);
    mre2 = sum(abs(gnd_all-ybar)) / sum(gnd_all);
    rmse2 = sqrt( sum((gnd_all-ybar).^2) / length(gnd_all) );
    medianE2 = median(abs(gnd_all-ybar));
    medianMRE2 = median(abs(gnd_all-ybar)) / median(gnd_all);
    
    fprintf('Baidu\t%g\t%g\t%g\t%g\n', mae, mre, medianE, medianRE);
end



%% find the cases with largest error
d = importdata('../../../dataset/SH_taxi_eval');
d = d(d(:,1)<3600,:);
d = d(d(:,2)>0,:);

gnd_all = d(:,1);
avg = d(:,2);
relaP = d(:,4);
absP = d(:,5);

err_rela = abs(relaP - gnd_all);
err_avg = abs(avg - gnd_all);
diff = err_rela - err_avg;
[~, idx] = sort(diff, 'descend');
d1 = [d, diff];
d1 = d1(idx, :);


%% get the distribution converting vector
d = importdata('../../../dataset/SH_taxi_eval_all');
d = d(d(:,1)<3600,:);
gnd_all = d(:,1);

gnd_all_min = floor(gnd_all ./ 60);
[cnt_all, center_all] = hist(gnd_all_min, 59);
cnt_all = cnt_all(1:25);
cnt_all = cnt_all / sum(cnt_all);

d = importdata('../../../dataset/SH_taxi_eval-bias');
d = d(d(:,1)<3600,:);
gnd_bias = d(:,1);

gnd_bias_min = floor(gnd_bias ./ 60);
[cnt_bias, center_bias] = hist(gnd_bias_min, 25);
cnt_bias = cnt_bias + 1;
cnt_bias = cnt_bias / sum(cnt_bias);


t = cnt_all ./ cnt_bias;

d = importdata('../../../dataset/SH_taxi_eval-sampled');
gnd_samp = d(d(:,1));



figure();
hold on;
box on;


l1 = cdfplot(gnd_all);
l2 = cdfplot(gnd_bias);
l3 = cdfplot(gnd_samp);
set(l1, 'color', 'red', 'linewidth', 3);
set(l2, 'color', 'blue', 'linewidth', 3);
set(l3, 'color', [0, 0.8, 0], 'linewidth', 3, 'linestyle', '--');

title('');

legend({'Whole Dataset', 'Biased Subset', 'Sampled Subset'}, 'fontsize', 16, 'location', 'southeast');
xlabel('Trip travel time (s)', 'fontsize', 16, 'linewidth', 3);
ylabel('CDF', 'fontsize', 16);
set(gca, 'fontsize', 16);
title('');
fn = 'cdf-need-sample.eps';
print('-dpsc2', fn);
system(['epstopdf ', fn]);
delete(fn);


%% evaluate on linear regression estimator

% fields: travel time, L1 distance, actual distance, ID
% e = importdata('../../../dataset/sh-all-trips');
% no_ids = importdata('../../../dataset/sh-all-trips-nonoutlier');

% get IDs for all non-outlier trips
[ids, idx] = intersect(e(:,4), no_ids);

if  sum(ids - no_ids) == 0 && sum(ids - e(idx, 4)) == 0
    all_time = e(idx, 1);
    all_dist = e(idx, 3);
    p = polyfit(all_dist, all_time, 1);
end


% fields: travel time, SEGMENT, SUBPATH, ID
d = importdata('../java/prediction-sampled');
test_gnd = d(:,1);

% get IDs for all testing trips
[ids, idx] = intersect(e(:,4), d(:,4));

if sum(ids - e(idx,4)) == 0 && sum(ids - d(:,4)) == 0
    test_dist = e(idx, 3);
    ybar = polyval(p, test_dist);
    
    mae = sum(abs(test_gnd-ybar)) / length(test_gnd);
    mre = sum(abs(test_gnd-ybar)) / sum(test_gnd);
    rmse = sqrt( sum((test_gnd-ybar).^2) / length(test_gnd) );
    medianE = median(abs(test_gnd-ybar));
    medianRE = median(abs(test_gnd-ybar) ./ test_gnd);


    fprintf('LR\t%g\t%g\t%g\t%g\n', mae, mre, medianE, medianRE);
end


%% number of GPS samples per edge -- distribution

d = importdata('../java/edge-sample');

l = cdfplot(d);

set(l, 'linewidth', 6, 'linestyle', '--', 'color', [0.8, 0, 0]);
xlabel('#GPS samples per edge', 'fontsize', 24);
ylabel('CDF', 'fontsize', 24);
set(gca, 'linewidth', 4, 'fontsize', 20, 'xscale', 'log', 'xtick', [1, 10^3, 10^6, 10^9]);
legend({'#GPS samples'}, 'fontsize', 24, 'location', 'southeast');
title('');
fn = 'SH-cdf-gps.eps';
print('-dpsc2', fn);
system(['epstopdf ', fn]);
delete(fn);


%% Trip time CDF

% d = importdata('../../../dataset/sh-all-trips');
time = d(:,1);


figure();
l1 = cdfplot(time / 60);
set(l1, 'linewidth', 6, 'linestyle', '--', 'color', [0, 0, 0.8]);
xlabel('Trip time (minute)', 'fontsize', 24);
ylabel('CDF', 'fontsize', 24);
set(gca, 'linewidth', 4, 'fontsize', 20, 'xlim', [0,80]);
legend({'travel time'}, 'fontsize', 24, 'location', 'southeast');
title('');
fn = 'SH-cdf-time.eps';
print('-dpsc2', fn);
system(['epstopdf ', fn]);
delete(fn);
