% compare different reference methods: duration / speed / distance
% 
% d = importdata('../../../dataset/goodrecs-wID-250k-mapinfo.teval');
% l = size(d,1);
% d = d(d(:,7)>0, :);
% l1 = size(d,1);


% load the prediction of 6 different methods
% 1. use average
% 2. use relative reference (24 * 7)
% 3. use absolute reference (oracle)
% 4. use absolute reference (ARIMA guess)
% 5. use absolute reference + regional difference
% 6. use distance instead of time * v
load speed-reference-6-method-10m;


gnd = d(:,1);
mae = zeros(1,6);
mederr = zeros(1,6);

for i = 1:6
    mae(i) = sum(abs(d(:,i+1)-gnd)) / l;
    mederr(i) = median( abs(d(:,i+1)-gnd) );
end