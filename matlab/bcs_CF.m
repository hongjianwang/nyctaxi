
function [U, V]=bcs_CF(trainset, lambda_1, lambda_2, lambda_3, rnk, max_iter,sizeX)
%function [U, V]=bcs_CF(xtrain, lambda_u, lambda_v, rnk, max_iter)

%FUNCTION FOR SOLVING BCS FORMULATION FOR RECOMMENDER SYSTEMS
%minimize_(U,V) ||Y-A(UV)||_2 + lambda_3||U||_F + lambda_1||V||_1 + lambda_2||V||_F

%EXTRA REQUIREMENT
%Sparco toolbox http://www.cs.ubc.ca/labs/scl/sparco/

%INPUTS
%trainset is the training set (observed rating matrix)
%lambda_1, lambda_2, lambda_3 are the regularization parameters
%rnk is the number of latent factors or rank of rating matrix
%max_iter is the maximum number of iterations

%OUTPUTS
%U is the user latent factor matrix
%V is the item latent factor matrix

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Find sampled (available rating) points
sampled_pts=find(trainset~=0);
A = opRestriction(prod(sizeX),sampled_pts);

%Compute vector of available ratings
y = A(trainset(:),1);

%Set initial values of output matrices
U = randn(sizeX(1),rnk);
V = rand(rnk,sizeX(2)); 
x_guess=U*V;
obj_func=[];


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Begin Iterations

for iter = 1:max_iter
   
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     %Solving subproblem 2 
     %minimize_(U) ||Y-A(UV)||_2 + lambda_3||U||_F
     ynew=x_guess(:)+A((y-A(x_guess(:),1)),2);
     Ymat=reshape(ynew,sizeX);
     
     aa=V*V'+lambda_3*eye(rnk);
     yy=Ymat*V';
     U=yy/aa;
   
       
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     %Solving subproblem 1 
     %minimize_(V) ||Y-A(UV)||_2 + lambda_1||V||_1 + lambda_2||V||_F
     x_guess=U*V;
     ynew=x_guess(:)+A((y-A(x_guess(:),1)),2);
     Ymat=reshape(ynew,sizeX);
         
     alpha=max(eig(U'*U));
     sigma=(V*alpha)/(alpha+lambda_2) +(1/(alpha+lambda_2))*(U'*(Ymat-U*V));
     V=soft(sigma,(lambda_1)/(2*(alpha+lambda_2)));
      
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   % Calculate Objective function

    x_guess=U*V;
    data_c = norm((A(x_guess(:),1)-y),2); 
    item_m = norm(V(:),1);
    user_m = norm(U(:),2);
    item_m2 = norm(V(:),2);
    cost = data_c +lambda_3*user_m+lambda_1*item_m+lambda_2*item_m2;
    obj_func = [obj_func,cost];
    
    if iter>1
    if  abs((obj_func(iter)-obj_func(iter-1)))<1e-7
        break
    end
    end

end

end

  