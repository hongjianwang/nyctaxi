%% evaluate December by each Month (exact one month as training)

method = {'AVG', 'TEMP-rela', 'TEMP-rela+R', 'TEMP-abs', 'TEMP-ARIMA', ...
    'TEMP-abs+R', 'TEMP-ARIMA+R', 'Dist/vref'};

mae = zeros(11, 8);
mre = zeros(11, 8);
medianE = zeros(11, 8);
medianRE = zeros(11, 8);


for mon = 1:11
    fprintf('Evaluate December by month %d.\n', mon);
    % input data has 11 fileds
    % 1. actual travel time
    % 2. AVG
    % 3. TEMP-rela
    % 4. TEMP-rela+R
    % 5. TEMP-abs
    % 6. TEMP-ARIMA
    % 7. TEMP-abs+R
    % 8. TEMP-ARIMA+R
    % 9. use $dist / vref$ instead of $time * vref$ 
    % 10. neighbor count
    % 11. actual distance
    d = importdata(sprintf('../../../dataset/7filter/2013-12.%d.eval', mon));
    d = d(d(:,10) > 0,:);

    % [~, idx] = ismember(c(:,4), d(:,7));
    % d = d(idx, :);

    gnd_all = d(:,1);

    for i = 1:8
        est = d(:,i+1);
        mae(mon, i) = sum(abs(gnd_all-est)) / length(gnd_all);
        mre(mon, i) = sum(abs(gnd_all-est)) / sum(gnd_all);
        medianE(mon, i) = median(abs(gnd_all-est));
        medianRE(mon, i) = median(abs(gnd_all-est) ./ gnd_all);
        fprintf('%s\t%g\t%g\t%g\t%g\n', method{i}, mae(mon, i), mre(mon, i), medianE(mon, i), medianRE(mon, i));
    end

end


%% we sample p% of the 11 month training data to evaluate on December


method = {'AVG', 'TEMP-rela', 'TEMP-rela+R', 'TEMP-abs', 'TEMP-ARIMA', ...
    'TEMP-abs+R', 'TEMP-ARIMA+R', 'Dist/vref'};
METHOD = {'AVG', '$\texttt{TEMP}_{\texttt{rel}}$', ...
    '$\texttt{TEMP}_{\texttt{rela}}+\texttt{R}$', ...
    '$\texttt{TEMP}_{\texttt{abs}}$', ...
    '$\texttt{TEMP}_{\texttt{abs}}+\texttt{R}$'};
IDX = [2, 3, 4, 6, 8];


mae = zeros(5, 5);
mre = zeros(5, 5);
medianE = zeros(5, 5);
medianRE = zeros(5, 5);


for s = 20:20:100
    fprintf('Evaluate December by sample %d%%.\n', s);
    % input data has 11 fileds
    % 1. actual travel time
    % 2. AVG
    % 3. TEMP-rela
    % 4. TEMP-rela+R
    % 5. TEMP-abs
    % 6. TEMP-ARIMA
    % 7. TEMP-abs+R
    % 8. TEMP-ARIMA+R
    % 9. use $dist / vref$ instead of $time * vref$ 
    % 10. neighbor count
    % 11. actual distance
    if s < 100
        d = importdata(sprintf('../../../dataset/7filter/2013-12.s%d.eval', s));
    elseif s == 100
        d = importdata('../../../dataset/7filter/2013-12.eval');
    end
    d = d(d(:,10) > 0,:);

    % [~, idx] = ismember(c(:,4), d(:,7));
    % d = d(idx, :);

    gnd_all = d(:,1);

    for i = 1:length(IDX)
        est = d(:,IDX(i));
        mae(s/20, i) = sum(abs(gnd_all-est)) / length(gnd_all);
        mre(s/20, i) = sum(abs(gnd_all-est)) / sum(gnd_all);
        medianE(s/20, i) = median(abs(gnd_all-est));
        medianRE(s/20, i) = median(abs(gnd_all-est) ./ gnd_all);
        fprintf('%s\t%g\t%g\t%g\t%g\n', method{IDX(i)-1}, mae(s/20, i), mre(s/20, i), medianE(s/20, i), medianRE(s/20, i));
    end

end



figure();
hold on;
box on;
grid on;

l = plot(20:20:100, mae, 'linewidth', 4, 'markersize', 12);
xlabel('Sample rate (%)', 'fontsize', 24);
ylabel('MAE (second)', 'fontsize', 24);
set(gca, 'linewidth', 3, 'fontsize', 20);
set(l(1), 'marker', 'd');
set(l(2), 'marker', '^');
set(l(3), 'marker', 'v', 'linestyle', '--');
set(l(4), 'marker', 'o');
set(l(5), 'marker', 'x', 'linestyle', '--');
legend(METHOD, 'fontsize', 20, 'interpreter', 'latex', 'location', 'best');

fn = 'nyc-sampled.eps';
print(gcf, fn, '-dpsc');
system(['epstopdf ', fn]);
delete(fn);



%% sample 1,3,5,7,9,11 months to estimate December
% we can load the following files
% NYC_evaluate_sample_month.mat


method = {'AVG', 'TEMP-rela', 'TEMP-rela+R', 'TEMP-abs', 'TEMP-ARIMA', ...
    'TEMP-abs+R', 'TEMP-ARIMA+R', 'Dist/vref'};
METHOD = {'AVG', '$\texttt{TEMP}_{\texttt{rel}}$', ...
    '$\texttt{TEMP}_{\texttt{rel}}+\texttt{R}$', ...
    '$\texttt{TEMP}_{\texttt{abs}}$', ...
    '$\texttt{TEMP}_{\texttt{abs}}+\texttt{R}$'};
IDX = [2, 3, 4, 6, 8];


% mae = zeros(6, 5);
% mre = zeros(6, 5);
% medianE = zeros(6, 5);
% medianRE = zeros(6, 5);


for j = 17
    fprintf('Evaluate December by Month %d-11.\n', 12-j);
    % input data has 11 fileds
    % 1. actual travel time
    % 2. AVG
    % 3. TEMP-rela
    % 4. TEMP-rela+R
    % 5. TEMP-abs
    % 6. TEMP-ARIMA
    % 7. TEMP-abs+R
    % 8. TEMP-ARIMA+R
    % 9. use $dist / vref$ instead of $time * vref$ 
    % 10. neighbor count
    % 11. actual distance
    if j < 11 && j >= 1
        d = importdata(sprintf('../../../dataset/7filter/2013-12.%d-11.eval', 12-j));
    elseif j == 11
        d = importdata('../../../dataset/7filter/2013-12.eval');
    elseif j >= 13
        d = importdata(sprintf('../../../dataset/7filter/2013-12.11w%d.eval', (j-11)/2));
    end
    d = d(d(:,10) > 0,:);
    
    fprintf('number of testable trips: %d.\nTotal number of neighbors: %d.\n', size(d,1), sum(d(:,10)));

    % [~, idx] = ismember(c(:,4), d(:,7));
    % d = d(idx, :);

    gnd_all = d(:,1);

    for i = 1:length(IDX)
        est = d(:,IDX(i));
        mae((j+1)/2, i) = sum(abs(gnd_all-est)) / length(gnd_all);
        mre((j+1)/2, i) = sum(abs(gnd_all-est)) / sum(gnd_all);
        medianE((j+1)/2, i) = median(abs(gnd_all-est));
        medianRE((j+1)/2, i) = median(abs(gnd_all-est) ./ gnd_all);
        fprintf('%s\t%g\t%g\t%g\t%g\n', method{IDX(i)-1}, mae((j+1)/2, i), ...
            mre((j+1)/2, i), medianE((j+1)/2, i), medianRE((j+1)/2, i));
    end

end

num_predictable_trips = [10191009, 10503012, 10578178, ...
    10623704, 10657467, 10692102, 10697241, 10699964, 10701413 ];
num_predictable_trips(2,:) = 10709125;
total_test = 10709125;


% plot mae
% ========================================
load NYC_evaluate_sample_month.mat
% ========================================

% tmp(1:3,:) = mae(7:9,:);
% tmp(4:9,:) = mae(1:6,:);
% mae = tmp;

figure();
hold on;
box on;
grid on;

xtic = [0.25, 0.5, 0.75, 1:2:11];
l = plot(xtic, mae, 'linewidth', 4, 'markersize', 12);
axis([0, 12, 140, 200]);
xlabel('Number of months used for training', 'fontsize', 24);
ylabel('MAE (second)', 'fontsize', 24);
set(gca, 'linewidth', 3, 'fontsize', 20, 'xscale', 'log', 'xtick',  [ 0.25, 0.5, 1, 3, 7, 11], ...
    'xticklabel', {'1 week', '2 weeks', '1', '3', '7', '11'});
set(l(1), 'marker', 'd');
set(l(2), 'marker', '^');
set(l(3), 'marker', 'v', 'linestyle', '--');
set(l(4), 'marker', 'o');
set(l(5), 'marker', 'x', 'linestyle', '--');
legend(METHOD, 'fontsize', 20, 'interpreter', 'latex', 'location', 'best');

fn = 'nyc-sample-month.eps';
print(gcf, fn, '-dpsc');
system(['epstopdf ', fn]);
delete(fn);


% plot coverage
figure();
hold on;
grid on;
box on;
[ax, l1, l2] = plotyy( xtic, num_predictable_trips, xtic, ...
    num_predictable_trips(1,:) / total_test);
set(l1(1), 'linewidth', 4, 'markersize', 14, 'marker', 'x');
set(l1(2), 'linewidth', 4, 'linestyle', '-.');
set(l2, 'linewidth', 4, 'markersize', 12, 'marker', 'o', 'linestyle', '--');
set(ax, 'fontsize', 20, 'xscale', 'log', 'linewidth', 3, 'xtick',  [ 0.25, 0.5, 1, 3, 7, 11], ...
    'xticklabel', {'1 week', '2 weeks', '1', '3', '7', '11'});
set(ax(2), 'ylim',  [0.92, 1], 'Position', [0.13 0.11 0.775-.08 0.815]);
xlabel('Number of months used for training', 'fontsize', 24);
ylabel(ax(1), 'Number of predictable trips', 'fontsize', 24);
ylabel(ax(2), 'Percentage of predicable trips', 'fontsize', 24);
legend({'Trips with neighbors', 'Total test', 'Percentage'}, ...
    'fontsize', 20, 'location', 'best');


fn = 'nyc-sample-month-cover.eps';
print(gcf, fn, '-dpsc');
system(['epstopdf ', fn]);
delete(fn);

