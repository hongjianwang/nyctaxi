function [ r2, r2lr, r2lr2, r2lr3, r2bing, MRE, MRElr, MRElr2, MRElr3, ...
        MREbing, MAE, MAElr, MAElr2, MAElr3, MAEbing ] = ...
        error_History_LR(nn_lower, nn_upper)


    % linear regression model
    data = importdata('../res/reg');
    l1 = data(:,1);
    td = data(:,2);
    p1 = polyfit(l1, td, 1);
    
    % Bing Map service privdes time / distance
    bing_driving = importdata('../res/bing.time');
    


    % column of trips
    % # neighbors, mean(T), var(T), mean(D), var(D), t, d, l1
    trips = importdata('../res/groupTrip4.res');

    nn = trips(:,1);
    
    if nargin >= 1
        trips = trips(nn>nn_lower,:);
        bing_driving = bing_driving(nn > nn_lower,:);
        nn = trips(:,1);
    end
    if nargin >= 2
        trips = trips(nn<=nn_upper,:);
        bing_driving = bing_driving(nn <= nn_upper, :);
        nn = trips(:,1);
    end
    size(trips)
    
    % Bing Map
    actualD = bing_driving(:,2);
    mapD = bing_driving(:,1);
    actualT = bing_driving(:,4);
    mapT = bing_driving(:,3);
    MREbing = sum(abs(actualT - mapT)) / sum(actualT);
    MAEbing = sum(abs(actualT - mapT)) / length(actualT);
    r2bing = r2error(mapT, actualT);
    
    
    
    % use historical data
    l1t = trips(:,8);
    mean_T = trips(:,2);
    t = trips(:, 6);
    MRE = sum(abs(mean_T - t)) / sum(t);
    MAE = sum(abs(mean_T - t)) / length(t);
    r2 = r2error( mean_T, t );
    
%     figure();
%     l = cdfplot(nn);
%     set(l, 'linewidth', 3);
%     set(gca, 'xscale', 'log');
%     xlabel('#similar trips');
%     ylabel('CDF');
    
%{     
     
    figure();
    scatter(mean_T, rsd);
%     axis([100, 650, 0.08, 1.1])
%     set(gca, 'xscale', 'log', 'yscale', 'log');
    xlabel('Average log(trip time)');
    ylabel('Relative standard deviation of log(time)');
    
    
    
    figure();
    scatter(nn, rsd);
%     axis([100, 650, 0.08, 1.1])
%     set(gca, 'xscale', 'log', 'yscale', 'log');
    xlabel('# similar trips');
    ylabel('Relative standard deviation of log(time)');
    
    
    
    
    nn_x = unique(nn);
    avg_rsd = zeros(size(nn_x));
    for i = 1:length(nn_x)
        avg_rsd(i) = mean(rsd(nn==nn_x(i)));
    end
    figure();
    plot(nn_x, avg_rsd, 'linewidth', 3);
    ylabel('Average relative standard deviation of log(time)');
    xlabel('# similar trips');
    
%}  
    yfit1 = polyval(p1, l1t);
    r2lr = r2error(yfit1, t);
    MRElr = sum(abs(yfit1-t)) / sum(t);
    MAElr = sum(abs(yfit1-t)) / length(t);
    
    p2 = polyfit(l1t, t, 1);
    yfit2 = polyval(p2, l1t);
    r2lr2 = r2error(yfit2, t);
    MRElr2 = sum(abs(yfit2-t)) / sum(t);
    MAElr2 = sum(abs(yfit2-t)) / length(t);
    
    p3 = polyfit(mapD, actualT, 1);
    yfit3 = polyval(p3, l1t);
    r2lr3 = r2error(yfit3, actualT);
    MRElr3 = sum(abs(yfit3-actualT)) / sum(actualT);
    MAElr3 = sum(abs(yfit3-actualT)) / length(actualT);
  

end