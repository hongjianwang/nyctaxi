%%

d = importdata('../java/region_pair_refence');

nanidx = find(~isnan(sum(d,2)));

d = d(nanidx, :);

ntaid = d(:,1);
ts = d(:,2:end);


besti = 0;
bestj = 0;
best_dif = -100;


% skip 99 NTA
for i = 1:size(d,1)
    n1 = ntaid(i);
    if bitshift(n1, -8) == 99 || bitand(n1, 255) == 99
        continue
    end
    
    for j = 218 % i+1:size(d,1)
        
        t1 = ts(i,:);
        t2 = ts(j,:);
        
        t1 = (t1 - mean(t1)) ./ var(t1);
        t2 = (t2 - mean(t2)) ./ var(t2);
        
        dif = t1 * t2' / norm(t1) / norm(t2);
        
        
%         dif = sum( (t1 - t2).^ 2);
        if dif > best_dif
            besti = i;
            bestj = j;
            best_dif = dif;
        end
    end
end


id1 = find(d(:,1)==6171);
id2 = find(d(:,1)==10253);


fprintf('%d, %d, %g\n', besti, bestj, best_dif);

n1 = ntaid(127);
fprintf('NTA: %d, %d, %d\n', bitshift(n1, -8),  bitand(n1, 255), n1);

n2 = ntaid(220);
fprintf('NTA: %d, %d, %d\n', bitshift(n2, -8),  bitand(n2, 255), n2);

n3 = ntaid(218);
fprintf('NTA: %d, %d, %d\n', bitshift(n3, -8),  bitand(n3, 255), n3);





figure();
hold on;
grid on;
box on;

% plot(ts(222, :));
% plot(ts(306, :), '-.', 'color', [0, 0.8, 0]);
plot(ts(id2, :), 'color', [0.7, 0, 0], 'linestyle', '--', 'linewidth', 3);
plot(ts(id1, :), 'color', [0, 0, 1], 'linewidth', 3);
% plot(ts(115, :), '--', 'color', [0.7, 0, 0], 'linewidth', 3);

pointsX = [84, 100];
pointsY1 = [8.545, 22.71]; 
pointsY2 = [7.009, 11.26];

set(gca, 'linewidth', 4, 'fontsize', 20, 'xlim', [0, 170], 'xtick', ...
    8:24:170, 'xticklabel', {'8 Sn', '8 M', '8 T', '8 W', '8 Th', '8 F', '8 S'});
plot(pointsX, pointsY1, 'd', 'markersize', 12, 'color', [0.7, 0, 0], 'linewidth', 4);
plot(pointsX, pointsY2, 'bo', 'markersize', 12, 'linewidth', 4);

%  annotation('textbox', [0.3, 0.8, 0.1, 0.1], 'string', '12:00 W - 7.01 mph vs 8.54 mph', 'fontsize', 20);
%  annotation('textbox', [0.3, 0.2, 0.1, 0.1], 'string', '4:00 Th - 11.26 mph vs 22.71 mph', 'fontsize', 20);

text(70, 6, '12:00 W', 'fontsize', 20);
text(90,24, '4:00 Th', 'fontsize', 20);
 
legend({'A-B', 'C-D'}, 'fontsize', 22, 'location' ,'northeast');
xlabel('Hour in week (hour)', 'fontsize', 24);
ylabel('Speed (mile/h)', 'fontsize', 24);


fn = 'nyc-nta-reference.eps';
print(gcf, fn, '-dpsc');
system(['epstopdf ', fn]);
delete(fn);

%% 

x = kmeans(ts, 7);

figure();
hold on;
for k = 1:7
    plot(mean(ts(x==k,:)), 'color', [0, 0.1 * k, 1-0.1*k]);
end

legend(k);


%% ALL 32767

figure();
hold on;
grid on;
box on;

plot(d(121,2:end), 'linewidth', 5);
set(gca, 'linewidth', 3, 'fontsize', 22, 'xlim', [0, 170], 'xtick', ...
    8:24:170, 'xticklabel', {'8am Sun', '8am Mon', '8am Tue', '8am Wed', '8am Thu', '8am Fri', '8am Sat'});

xlabel('Starting time of the trip', 'fontsize', 24);
ylabel('Average Speed (mile/h)', 'fontsize', 24);
set(gcf, 'PaperPosition', [0 1 16 4]);

fn = 'weekly-speed.eps';
print(gcf, fn, '-dpsc');
system(['epstopdf ', fn]);
delete(fn);


