% the matrix factorization

disp('add the MCNF function to the matlab path')
cd C:\Users\hxw186\Documents\MATLAB\MC-NMF
Run_Me_1st

% going back
cd C:\Users\hxw186\workspace\nycTaxi\matlab

A = importdata('../res/pair2m.res');
idx = A>0;
nr = zeros(4,2);

disp('method 1: nnmf')
[u,v] = nnmf(A, 5, 'algorithm', 'als');
Abar = u*v;
nr(1,1) = norm(A - Abar);
nr(1,2) = norm((A - Abar) .*idx);


disp('method 2: bcs_CF')
[u, v] = bcs_CF(A, 0, 0.5, 0.5, 5, 200, size(A));
Abar = u*v;
nr(2,1) = norm(A - Abar);
nr(2,2) = norm((A - Abar) .*idx);


disp('method 3: bcs_NG_CF')
[u, v] = bcs_NG_CF(A, 0, 0.5, 0.5, 5, 200, size(A));
Abar = u*v;
nr(3,1) = norm(A - Abar);
nr(3,2) = norm((A - Abar) .*idx);


disp('method 4: mc_nmf')
[u, v] = mc_nmf(A(A>0), A>0, 5, size(A,1), size(A,2), optimset('maxit', 200));
Abar = u*v;
nr(4,1) = norm(A - Abar);
nr(4,2) = norm((A - Abar) .*idx);


