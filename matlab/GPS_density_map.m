%% Plot the density map of GPS in New York City

% gpsNYC = importdata('../../../dataset/dist_l1/pickup_gps_20p');


[h, c] = hist3(gpsNYC, [100, 100]);   % put GPS into 1000-by-1000 bins
hn = h';
hn(hn==0) = NaN;

surf(c{1}, c{2}, hn, 'EdgeColor', 'none');
view(0, 90);
box on;

cmap = [
    128, 255, 255;
    191, 255, 128;
    255, 255, 0;
    255, 223, 0;
    255, 191, 0;
    255, 159, 0;
    255, 128, 0;
    255, 96, 0;
    255, 64, 0;
    255, 32, 0;
    255, 0, 0];
cmap = [cmap; (252:-2:146)' * [1, 0, 0]];
cmap = cmap ./ 255;

colormap(cmap);

set(gca, 'fontsize', 20, 'linewidth', 4, 'xlim', [-74.05, -73.85], ...
    'xtick', -74.05:0.05:-73.85);
xlabel('longitude', 'fontsize', 24);
ylabel('latitude', 'fontsize', 24);

fn = 'NYC-heatmap.eps';
print('-dpsc2', fn);
system(['epstopdf ', fn]);
delete(fn);



%% Plot the density map of GPS in Shanghai


% gps = importdata('../../../dataset/SH_gps_8p');


[h, c] = hist3(gps, [100, 100]);   % put GPS into 100-by-100 bins
hn = h';
hn(hn==0) = NaN;

figure();
surf(c{1}, c{2}, hn, 'EdgeColor', 'none');

cmap = [
    128, 255, 255;
    191, 255, 128;
    255, 255, 0;
    255, 223, 0;
    255, 191, 0;
    255, 159, 0;
    255, 128, 0;
    255, 96, 0;
    255, 64, 0;
    255, 32, 0;
    255, 0, 0];
cmap = [cmap; (252:-2:146)' * [1, 0, 0]];
cmap = cmap ./ 255;

colormap(cmap);

box on;
view(0, 90);
set(gca, 'fontsize', 20, 'linewidth', 4);
xlabel('longitude', 'fontsize', 24);
ylabel('latitude', 'fontsize', 24);

fn = 'SH-heatmap.eps';
print('-dpsc2', fn);
system(['epstopdf ', fn]);
delete(fn);
