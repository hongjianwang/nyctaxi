d = importdata('../java/neighborCnt');

figure();
hold on;
l1 = cdfplot(d(:,1));
set(l1, 'color' ,'red');

l2 = cdfplot(d(:,2));
set(l2, 'color', 'blue');


l3 = cdfplot(d(:,3));
set(l3, 'color', 'cyan');

l4 = cdfplot(d(:,4));
set(l4, 'color', 'yellow', 'linewidth', 3);

l5 = cdfplot(d(:,5));
set(l5, 'color', 'magenta', 'linewidth', 3);

l6 = cdfplot(d(:,6));
set(l6, 'color', 'black');

legend({'Direct', 'Order 1', 'Order 2', 'Order 3', 'Order 4', 'Order 5'});
set(gca, 'xscale', 'log');

title('CDF of # neighboring trips');
xlabel('Number of neighbors in log scale');
ylabel('CDF');