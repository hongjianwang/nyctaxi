%% coverage
a = [0.32, 0.85, 0.97, 0.99, 0.999, 1, 1, 1, 1];

figure();
hold on;
grid on;
box on;


plot(0:8, a, 'bd-', 'linewidth', 3, 'markersize', 12);
set(gca, 'linewidth', 3', 'fontsize', 20);
axis([-0.2, 8.2, 0, 1.1]);
xlabel('Neighbor threshold \tau', 'fontsize', 24);
ylabel('Portion of testing trips with neighbors', 'fontsize', 22);


fn = 'coverage.eps';
print(gcf, fn, '-dpsc');
system(['epstopdf ', fn]);
delete(fn);



%% error-n

METHOD = {'AVG', '$\texttt{TEMP}_{\texttt{rel}}$', ...
    '$\texttt{TEMP}_{\texttt{abs}}$', ...
    '$\texttt{TEMP}_{\texttt{rel}}+\texttt{R}$'};



a = [189, 186, 179.5, 177, 176.5, 177.5, 178.3, 178.9, 179.5;
    158, 152, 146, 144, 143.8, 144.3, 145, 147, 148;
    157.5, 151.3, 146.3, 144.5, 144.3, 144.9, 145.6, 147.4, 148.3;
    151, 146.5, 141, 138.5, 138.9, 140.2, 141, 143, 144];

figure();
hold on;
grid on;
box on;


plot(0:8, a(1,:), 'bd--', 'linewidth', 4, 'markersize', 12);
plot(0:8, a(2,:), 'co-', 'linewidth', 4, 'markersize', 12);
plot(0:8, a(3,:), 'rx--', 'linewidth', 4, 'markersize', 14);
plot(0:8, a(4,:), 'g+--', 'linewidth', 4, 'markersize', 14);
set(gca, 'linewidth', 3', 'fontsize', 20);
axis([0, 8, 130, 190]);
legend(METHOD, 'fontsize', 20, 'location', 'best', 'interpreter', 'latex');
xlabel('Neighbor threshold \tau', 'fontsize', 24);
ylabel('MAE (seconds)', 'fontsize', 24);


fn = 'error-n.eps';
print(gcf, fn, '-dpsc');
system(['epstopdf ', fn]);
delete(fn);