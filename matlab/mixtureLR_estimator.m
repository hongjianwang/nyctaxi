% after the training process of "linearFilter"
% we get the parameter for the regression model
%       y = w1 * x + w2 * 1 + error,
% where the error can be ignored.


[y1, x1, w1, t1, data] = linearFilter();

% !!! In this testing data, the L1 is not taken log
test = data(tst_idx,:); % this matrix match with ``test_part''
test = test(tst_filt_idx, :);

ybar = test_part(tst_filt_idx, 1);
ybar2 = test_part(tst_filt_idx, 2);
ybar3 = test_part(tst_filt_idx, 3);
ybar4 = test_part(tst_filt_idx, 4);
x = [log(test(:,2)), ones(size(test(:,2)))];
y = exp( x * w1);

gnd = test(:,3);
    





% neighbor without time
MAE1 = sum(abs(gnd-ybar)) / length(gnd);
MRE1 = sum(abs(gnd-ybar)) / sum(gnd);
RMSE1 = sqrt(sum((gnd-ybar).^2) / length(gnd));
fprintf('%d\t%g\t%g\t%g\t',size(gnd,1), MAE1, MRE1, RMSE1);


if useBingSet == 0
    % neighbor with time weight
    MAE2 = sum(abs(gnd-ybar2)) / length(gnd);
    MRE2 = sum(abs(gnd-ybar2)) / sum(gnd);
    RMSE2 = sqrt(sum((gnd-ybar2).^2) / length(gnd));

    fprintf('%g\t%g\t%g\t',MAE2, MRE2, RMSE2);
    
    MAE4 = sum(abs(gnd-ybar3)) / length(gnd);
    MRE4 = sum(abs(gnd-ybar3)) / sum(gnd);
    RMSE4 = sqrt(sum((gnd-ybar3).^2) / length(gnd));

    fprintf('%g\t%g\t%g\t',MAE4, MRE4, RMSE4);
    
    MAE5 = sum(abs(gnd-ybar4)) / length(gnd);
    MRE5 = sum(abs(gnd-ybar4)) / sum(gnd);
    RMSE5 = sqrt(sum((gnd-ybar4).^2) / length(gnd));

    fprintf('%g\t%g\t%g\t',MAE5, MRE5, RMSE5);
end

MAE3 = sum(abs(gnd-y)) / length(gnd);
MRE3 = sum(abs(gnd-y)) / sum(gnd);
RMSE3 = sqrt(sum((gnd-y).^2) / length(gnd));

fprintf('%g\t%g\t%g\n',MAE3, MRE3, RMSE3);



