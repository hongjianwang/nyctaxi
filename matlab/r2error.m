function r2 = r2error( y_pre, y_act  )
        s_res = sum( (y_act - y_pre).^2 );
        y_bar = mean(y_act);
        s_tot = sum( (y_act - y_bar).^2 );
        r2 = 1 - s_res / s_tot;
    end