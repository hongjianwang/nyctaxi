% evaluation various estimator on NYC dataset


%% evaluate on complete-trip-based method


method = {'AVG', 'TEMP-rela', 'TEMP-rela+R', 'TEMP-abs', 'TEMP-ARIMA', ...
    'TEMP-abs+R', 'TEMP-ARIMA+R', 'Dist/vref'};
% input data has 11 fileds
% 1. actual travel time
% 2. AVG
% 3. TEMP-rela
% 4. TEMP-rela+R
% 5. TEMP-abs
% 6. TEMP-ARIMA
% 7. TEMP-abs+R
% 8. TEMP-ARIMA+R
% 9. use $dist / vref$ instead of $time * vref$ 
% 10. neighbor count
% 11. actual distance
d = importdata('../../../dataset/7filter/2013-12.eval');
d = d(d(:,10) > 0,:);

% [~, idx] = ismember(c(:,4), d(:,7));
% d = d(idx, :);

gnd_all = d(:,1);

for i = 1:8
    est = d(:,i+1);
    mae = sum(abs(gnd_all-est)) / length(gnd_all);
    mre = sum(abs(gnd_all-est)) / sum(gnd_all);
    rmse = sqrt( sum((gnd_all-est).^2) / length(gnd_all) );
    medianE = median(abs(gnd_all-est));
    medianMRE = median(abs(gnd_all-est) ./ gnd_all);
    fprintf('%s\t%g\t%g\t%g\t%g\n', method{i}, mae, mre, medianE, medianMRE);
end

% nn = d(:,10);
% figure();
% l = cdfplot(nn);
% set(l, 'color', 'blue', 'linewidth', 3);
% set(gca, 'xscale', 'log');
% xlabel('Number of neighbors');
% ylabel('CDF');
% title(sprintf('CDF of %d trips', length(gnd)));


figure();
l1 = cdfplot(gnd_all);
set(l1, 'linewidth', 3);
hold on;
l2 = cdfplot(d(:,2));
l3 = cdfplot(d(:,5));
set(l2, 'color', 'red', 'linestyle', '--', 'linewidth', 3);
set(l3, 'color', [0, 0.8, 0], 'linewidth', 3, 'linestyle', '--');
set(gca, 'xscale', 'log', 'fontsize', 16);
legend({'Ground truth', 'AVG', 'RELA-abs'}, 'fontsize', 16, 'location', 'best');
xlabel('Trip travel time (s)', 'fontsize', 16, 'linewidth', 3);
ylabel('CDF', 'fontsize', 16);
title(sprintf('CDF of %d trips', length(gnd_all)));
title('');
% fn = 'cdf-biased.eps';
% print('-dpsc2', fn);
% system(['epstopdf ', fn]);
% delete(fn);



%{ 
% error w.r.t trip length
nd = [d, abs(gnd-medi) ./ gnd, abs(gnd-segment) ./ gnd, abs(gnd-relaP) ./ gnd, abs(gnd-absP) ./ gnd ];
[~,idx] = sort(nd(:,1));
nd = nd(idx, :);
figure();
xlabel('Trip time (seconds)', 'fontsize', 12);
ylabel('Relative Error', 'fontsize', 12);

subplot(2,2,1);
plot(nd(:,1), nd(:,7), 'linewidth', 1);
title('Median Estimator', 'fontsize', 12);
set(gca, 'ylim', [0, 15]);
subplot(2,2,2);
plot(nd(:,1), nd(:,8), 'linewidth', 1);
title('AVG Estimator', 'fontsize', 12);
set(gca, 'ylim', [0, 15]);
subplot(2,2,3);
plot(nd(:,1), nd(:,9), 'linewidth', 1);
title('TEMP-rela Estimator', 'fontsize', 12);
set(gca, 'ylim', [0, 15]);
subplot(2,2,4);
plot(nd(:,1), nd(:,10), 'linewidth', 1);
title('TEMP-abs Estimator', 'fontsize', 12);
set(gca, 'ylim', [0, 15]);
%}


%% linear regression estimator

% d = importdata('../../../dataset/logtime_logl1/2013-12.eval');
time = d(:,1);
dist = d(:,11);


p1 = polyfit(dist, time, 1);
fitT = polyval(p1, dist);

MAE = sum(abs(fitT - time)) / length(time);
MRE = sum(abs(fitT - time)) / sum(time);
medianE = median(abs(time-fitT));
medianMRE = median(abs(time-fitT) ./ time);
fprintf('LR\t%g\t%g\t%g\t%g\n', mae, mre, medianE, medianMRE);




%% Evaluate Bing Map Service


dtrips = importdata('../../../dataset/7filter/2013-12.9-11.eval.bing');
id1 = dtrips(:,12);

d = importdata('../test');
id2 = d(:,3);

[c, ia, ib] = intersect(id1, id2);

dtrips = dtrips(ia, :);
d = d(ib, :);


gnd_all = dtrips(:,1);

time = d(:,1);
timeTraffic = d(:,2);

for i = 1:8
    est = dtrips(:,i+1);
    mae = sum(abs(gnd_all-est)) / length(gnd_all);
    mre = sum(abs(gnd_all-est)) / sum(gnd_all);
    rmse = sqrt( sum((gnd_all-est).^2) / length(gnd_all) );
    medianE = median(abs(gnd_all-est));
    medianMRE = median(abs(gnd_all-est) ./ gnd_all);
    fprintf('%s\t%g\t%g\t%g\t%g\n', method{i}, mae, mre, medianE, medianMRE);
end


MAE_map = sum(abs(gnd_all - time)) / length(gnd_all);
MRE_map = sum(abs(gnd_all - time)) / sum(gnd_all);
medianE_map = median(abs(time-gnd_all));
medianRE_map = median(abs(time-gnd_all) ./ gnd_all);
fprintf('BING\t%g\t%g\t%g\t%g\n', MAE_map, MRE_map, medianE_map, medianRE_map);


MAE_map = sum(abs(gnd_all - timeTraffic)) / length(gnd_all);
MRE_map = sum(abs(gnd_all - timeTraffic)) / sum(gnd_all);
medianE_map = median(abs(timeTraffic-gnd_all));
medianRE_map = median(abs(timeTraffic-gnd_all) ./ gnd_all);
fprintf('BING-with traffic\t%g\t%g\t%g\t%g\n', MAE_map, MRE_map, medianE_map, medianRE_map);



%% Trip time/distance CDF
figure();
l1 = cdfplot(time / 60);
set(l1, 'linewidth', 6, 'linestyle', '--', 'color', [0, 0, 0.8]);
xlabel('Trip time (minute)', 'fontsize', 24);
ylabel('CDF', 'fontsize', 24);
set(gca, 'linewidth', 4, 'fontsize', 20, 'xlim', [0,120]);
legend({'travel time'}, 'fontsize', 24, 'location', 'southeast');
title('');
fn = 'NYC-cdf-time.eps';
print('-dpsc2', fn);
system(['epstopdf ', fn]);
delete(fn);


figure();
l2 = cdfplot(dist);
set(l2, 'linewidth', 6, 'linestyle', '--', 'color', [0,0.7,0]);
xlabel('Trip distance (mile)', 'fontsize', 24);
ylabel('CDF', 'fontsize', 24);
set(gca, 'linewidth', 4, 'fontsize', 20, 'xlim', [0,15]);
legend({'travel distance'}, 'fontsize', 24, 'location', 'southeast');
title('');
fn = 'NYC-cdf-dist.eps';
print('-dpsc2', fn);
system(['epstopdf ', fn]);
delete(fn);


