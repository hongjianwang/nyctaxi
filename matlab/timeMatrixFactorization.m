A = importdata('../res/pair2m.res');
% A = sparse(d);

nz_idx = find(A);
rdx = rand(size(nz_idx));


for k = 2:5
    res = zeros(5,10);
    for i = 1:10
        testD = A;
        testD(nz_idx(rdx>0.1)) = 0;

        trainD = A;
        trainD(nz_idx(rdx<0.1)) = 0;


        options = statset('display', 'off');

%         [W, H] = nnmf(trainD, k, 'algorithm', 'als', 'options', options); %, 'replicates', 10);
%         Abar = W * H;

%         [U, V] = bcs_NG_CF(trainD, 0, 0.5, 0.5, k, 200, size(A));
%         Abar = U*V;

%         [U, V] = bcs_CF(trainD, 0, 0, 0, k, 200, size(A));
%         Abar = U*V;

        opt = optimset('maxit', 200);
%         [U, V] = mc_nmf(trainD(trainD>0), find(trainD>0), k, size(trainD, 1), size(trainD,2), opt);
        [U, V] = mc_nmf(A(A>0), find(A>0), k, size(trainD, 1), size(trainD,2), opt);
        Abar = U*V;

        test_idx = find(testD);

        gndTruth = testD(test_idx);
        estimation = Abar(test_idx);

        err = gndTruth - estimation;
        res(1,i) = sqrt(mean(err.^2));  % RMES
        res(5,i) = sum(abs(err)) / sum(gndTruth); %MRE
        res(6,i) = sum(abs(err)) / numel(err); %MAE
        res(2,i) = norm(full(A-trainD));
        res(3,i) = norm(A-Abar);
        res(4,i) = norm(trainD-Abar);

    end
    dif = [k, mean(res(6,:)), mean(res(5,:)), mean(res(1,:)) ]%, ...
%         mean(res(2,:)), mean(res(3,:)) , mean(res(4,:))]
end


% figure()
% l = V';
% subplot(2,1,1);
% plot(l(:,1));
% title('Component 1');
% 
% subplot(2,1,2);
% plot(l(:,2));
% title('Component 2');

% subplot(3,1,3);
% plot(l(:,3));
% title('Component 3');


