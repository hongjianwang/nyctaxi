%% compare the relative reference and absolute reference

fid = fopen('../java/rela_vs_abs');

l = fgetl(fid);
rela = sscanf(l, '%f ');


cnt = 0;
l = fgetl(fid);
while ischar(l)
    abs = sscanf(l, '%f ');
    l = fgetl(fid);
    if length(abs) < 168
        continue
    else
        figure()
        hold on
        box on
        grid on
        
        plot(abs, 'r-', 'linewidth', 5)
        plot(rela, 'b--', 'linewidth', 5)
        legend({'Absolute speed reference', 'Relative speed reference'}, 'fontsize', 24, 'location', 'best');
        set(gca, 'linewidth', 3, 'fontsize', 22, 'xlim', [0, 170], 'xtick', ...
            8:24:170, 'xticklabel', {'8am Sun', '8am Mon', '8am Tue', '8am Wed', '8am Thu', '8am Fri', '8am Sat'});
        xlabel('Starting time of the trip', 'fontsize', 24);
        ylabel('Speed reference (mile/h)', 'fontsize', 24);
        set(gcf, 'PaperPosition', [0 1 16 4]);
        fn = sprintf('rela-abs%d.eps', cnt);
        print(gcf, fn, '-dpsc');
        system(['epstopdf ', fn]);
        delete(fn);
        cnt = cnt + 1;
    end
    
end


fclose(fid);