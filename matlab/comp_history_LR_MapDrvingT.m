r2 = zeros(1,5);
r2lr = zeros(1,5);
r2lr2 = zeros(1,5);
r2lr3 = zeros(1,5);
r2bing = zeros(1,5);

MRE = zeros(1,5);
MRElr = zeros(1,5);
MRElr2 = zeros(1,5);
MRElr3 = zeros(1,5);
MREbing = zeros(1,5);

MAE = zeros(1,5);
MAElr = zeros(1,5);
MAElr2 = zeros(1,5);
MAElr3 = zeros(1,5);
MAEbing = zeros(1,5);


r2w = zeros(1,5);
MREw = zeros(1,5);
MAEw = zeros(1,5);

nn_lowerbound = [0, 5, 10, 20, 50];
nn_upperbound = [5, 10, 20, 50, 600];
for i = 1:5
    [ r2(i), r2lr(i), r2lr2(i), r2lr3(i), r2bing(i), ...
        MRE(i), MRElr(i), MRElr2(i), MRElr3(i), MREbing(i), ...
        MAE(i), MAElr(i), MAElr2(i), MAElr3(i), MAEbing(i) ] ...
            = error_History_LR(nn_lowerbound(i));
    [r2w(i), MREw(i), MAEw(i)] = error_History(nn_lowerbound(i));
end

[ r2; r2lr; r2lr2; r2lr3; r2bing; MRE; MRElr; MRElr2; MRElr3; MREbing; ...
    MAE; MAElr; MAElr2; MAElr3; MAEbing ]

[r2; r2w; MRE; MREw; MAE; MAEw]

for i = 1:5
    [ r2(i), r2lr(i), r2lr2(i), r2lr3(i), r2bing(i), ...
        MRE(i), MRElr(i), MRElr2(i), MRElr3(i), MREbing(i), ...
        MAE(i), MAElr(i), MAElr2(i), MAElr3(i), MAEbing(i) ] ...
            = error_History_LR(nn_lowerbound(i), nn_upperbound(i));
    [r2w(i), MREw(i), MAEw(i)] = error_History(nn_lowerbound(i), nn_upperbound(i));
end

[ r2; r2lr; r2lr2; r2lr3; r2bing; MRE; MRElr; MRElr2; MRElr3; MREbing; ...
    MAE; MAElr; MAElr2; MAElr3; MAEbing ]

[r2; r2w; MRE; MREw; MAE; MAEw]

