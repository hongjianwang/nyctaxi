function [Y,X,w,t,p, data] = linearFilter( filterName )

    
    b = pi * exp(2);
    p = 0.5;
    sigma2 = 1;
	
    % format of data
    % time, L1 distance, distance, ID
	data = importdata('../../../dataset/sh-all-trips');
    % the L1 should larger than 0
    data = data(data(:,2)>0,:);
    
	train = data;
	%     Y = data(:,1);    % actual distance
    
    if (strcmp(filterName, 'logL1-logD'))
        Y = log(data(:,2)); % log L1 distance
        X = [log(data(:,3)), ones(size(data(:,3)))]; % log actual distance (!!! log here)
    elseif (strcmp( filterName, 'logT-logD'))
        Y = log(data(:,1)); % log time
        X = [log(data(:,3)), ones(size(data(:,3)))]; % log actual distance
	elseif (strcmp( filterName, 'logT-logL1'))
		Y = log(data(:,1)); % log time
		X = [log(data(:,2)), ones(size(data(:,2)))]; % log L1 distance
    end
	w = [1, 0]';
    
    t = zeros(size(Y));
   
    epsilon = 10;
    


    disp('start EM algorithm');
    cnt = 0;
    previousL = 0;
    
    err = (Y -  X * w);
    L = E_step();
    while ( abs(L - previousL) >= epsilon )
        previousL = L;
        cnt = cnt + 1;
        M_step();
        fprintf('%g\t%g\t%g\t%g\t%g\t%g\n', cnt, w(1), w(2), L, p);
        L = E_step();
    end
    
    disp('Write file');
    D = [tw, data];
    split = floor(length(Y) * p);
    [~, idx] = sort(t, 'descend');
    D = D(idx,:);
    D1 = D(D(:,1) < D(split,1),:);
    D2 = D(D(:,1) >= D(split,1), :);
    
    if (strcmp(filterName, 'logL1-logD'))
        fid = fopen('../../../dataset/sh-nonoutlier-logL1-logD', 'w');
        fid2 = fopen('../../../dataset/sh-outlier-logL1-logD', 'w');
    elseif (strcmp( filterName, 'logT-logD'))
        fid = fopen('../../../dataset/sh-nonoutlier-logT-logD', 'w');
        fid2 = fopen('../../../dataset/sh-outlier-logT-logD', 'w');
	elseif (strcmp( filterName, 'logT-logL1'))
        fid = fopen('../../../dataset/sh-nonoutlier-logT-logL1', 'w');
        fid2 = fopen('../../../dataset/sh-outlier-logT-logL1', 'w');
    end
    
       
    
    
    for j = 1:size(D1,1)
        fprintf(fid, '%g,%g,%g,%g,%d\n', D1(j,1), D1(j,2), D1(j,3), D1(j,4), D1(j,5));
    end
    fclose(fid);

    for j = 1:size(D2, 1)
        fprintf(fid2, '%g,%g,%g,%g,%d\n', D2(j,1), D2(j,2), D2(j,3), D2(j,4), D2(j,5));
    end
    fclose(fid2);
    
%     plotDifferentT();
    
    
    function L = E_step()
        
        % update t_i
        z = err.^2 / 2 / sigma2 + log(p / (1-p)) + 0.5 * log(b * sigma2/ pi / exp(2));
        t = 1 ./ (1 + exp(-z));  % which label to use for updating
        
        % calculate l_i
        tmp = ((1-p) / sqrt(2*pi*sigma2) .* exp( - err.^2 / 2 / sigma2)).^(1-t);
        tmp2 = ((p) .* sqrt(b/pi) / exp(1) / sqrt(2 * pi)).^t;
        l = tmp .* tmp2;
        
        L = -sum(log(l));
    end


    function M_step()
        % update p
        p = mean(t);
        
        % update sigma2
        sigma2 = sum((1-t) .* err.^2)/ (sum(1-t));
        
        % update b with 1/the median error of outlieres
        err_sorted = sort(err);
        i = recursive_median(err_sorted, 1, length(err), floor(length(err)/2));
        b = 1 / err_sorted(i);
        
        % update weight
        Xw = zeros(size(X));
        for i = 1:size(X,2)
            Xw(:,i) = X(:,i) .* sqrt(1-t);
        end
        
        Yw = sqrt(1-t) .* Y;
        
        A = Xw' * Xw;
        B = Xw' * Yw;
        w = lsqr(A,B, 0.001);
        
        % upate err
        err = (Y -  X * w);
    end


    function i = recursive_median( err, low, high, j )
%         fprintf('low %d, high %d, j %d\n', low, high, j);
        if high - low <= 1
            i = low;
        else
            if sum(err(1:j)) > sum(err) / 2
                i = recursive_median( err, low, j, floor((j+low)/2));
            elseif sum(err(1:j)) == sum(err) / 2
                i = j;
            else
                i = recursive_median( err, j, high, floor((j+high)/2));
            end
        end
    end

                
    
    function plotDifferentT()
        torg = train(:,4);
        h = figure();
        hold on;
        box on;
        l1 = cdfplot(torg);
        set(l1, 'linewidth', 3, 'color', 'r');
        l2 = cdfplot(t);
        set(l2, 'linewidth', 3, 'color', 'b');
        set(gca, 'xscale', 'log', 'yscale', 'log', 'linewidth', 3, 'fontsize', 16');
        legend({'Original membership Label', 'New membership assingment'}, ...
            'fontsize', 16', 'location', 'southeast');
        xlabel('t_i', 'fontsize', 18);
        ylabel('CDF', 'fontsize', 18);
        title('');
        saveas(h, 't2.png', 'png');
    end


end
