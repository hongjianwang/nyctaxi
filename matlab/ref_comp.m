% visualize temporal reference

d = importdata('../java/ref_comp');

figure();
subplot(2,2,1);
hold on;

lgd = {};
for i = 1:size(d,1)
    plot(d');
    lgd{i} = num2str(2+i);
end

legend(lgd);


subplot(2,2,2);
hold on;

plot(d(1,:), 'r');
plot(d(7,:), 'b');
plot(d(8,:), 'g-');
plot(d(14,:), 'k');
legend({'3', '9', '10', '16'});


subplot(2,2,3);
hold on;

plot(d(2,:), 'r');
plot(d(9,:), 'b');
title('Two different Mondays');
legend({'4', '11'});


subplot(2,2,4);
hold on;

plot(d(6,:), 'r');
plot(d(13,:), 'b');
title('Two different Fridays');
legend({'8', '15'});