%This DEMO is for eNetBCS formulation for Recommender System
% minimize_(U,V) ||Y-A(UV)||_2 + lambda_3||U||_F + lambda_1||V||_1 + lambda_2||V||_F
% Demo parametrs are set for 100K Movielens dataset and 5 fold cross
% validation; 2nd train-test pair tested

clc
clear all
close all;

% LOAD DATASET
load fold_5_dataset.mat;

%Assign Training Set, Test set, and baseline parameters
trainset=mt2;
testset=m2;
global_mean=g_mean2;
user_bias=bu_22;
item_bias=bi_22;

%Set Algorithm parameters value
lambda_3 = 1e3; 
lambda_1 = 1e-3;
lambda_2=1e-1;
rnk = 50;
max_iter = 45;
sizeX = size(trainset);

%Call function
[U, V]=bcs_CF(trainset, lambda_1, lambda_2, lambda_3, rnk, max_iter,sizeX);

%Combine with baseline estimates
X_interaction=U*V;
X_estimated=zeros(size(X_interaction));

for r=1:size(X_interaction,1)
             for c=1:size(X_interaction,2)               
                 X_estimated(r,c)=X_interaction(r,c)+user_bias(r,1)+item_bias(1,c)+global_mean;       
             end
end
 
 %Compute error
 Error=error_rate(testset,X_estimated);
 
 %Display Results
 disp('Mean Absolute error')
 disp(Error)
 
