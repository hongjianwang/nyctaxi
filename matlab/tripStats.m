bing_driving = importdata('../res/bing.time');
actualD = bing_driving(:,2);
mapD = bing_driving(:,1);
actualT = bing_driving(:,4);
mapT = bing_driving(:,3);

% histogram of distance on 34000 trips
figure()
hist(actualD, 10000);
xlabel('Actual Distance (mile)', 'fontsize', 18);
ylabel('Number of Trips', 'fontsize', 18);
set(gca, 'fontsize', 16);
axis([0, 2, 0, 3000]);
saveas(gcf, 'dist_test.jpeg');

c = 0;
for i = 0.2:0.1:2.5
    c = c + sum(actualD==i);
end

[c, length(actualD), c / length(actualD)]


% histogram of time on 34000 trips
figure()
hist(actualT, 10000);
xlabel('Actual Time (second)', 'fontsize', 18);
ylabel('Number of Trips', 'fontsize', 18);
set(gca, 'fontsize', 16);
axis([0, 1000, 0, 5000]);
saveas(gcf, 'time_test.jpeg');

c = 0;
for i = 60:60:2000
    c = c + sum(actualT==i);
end

[c, length(actualD), c / length(actualD)]


raw_mht = importdata('../../../dataset/dist_time');
dist = raw_mht(:,1);
time = raw_mht(:,2);

% histogram of distance on all Manhattan trips
figure()
hist(dist, 10000);
xlabel('Actual Distance (mile)', 'fontsize', 18);
ylabel('Number of Trips', 'fontsize', 18);
set(gca, 'fontsize', 16);
axis([0, 2, 0, 100000]);
saveas(gcf, 'dist_train.jpeg');

c = 0;
for i = 0.2:0.1:3.5
    c = c + sum(dist==i);
end

[c, length(dist), c / length(dist)]


% histogram of time on all Manhattan trips
figure()
hist(time, 10000);
xlabel('Actual Time (second)', 'fontsize', 18);
ylabel('Number of Trips', 'fontsize', 18);
set(gca, 'fontsize', 16);
axis([0, 1000, 0, 120000]);
saveas(gcf, 'time_train.jpeg');

c = 0;
for i = 60:60:8000
    c = c + sum(time==i);
end

[c, length(time), c / length(time)]



% # neighbors, mean(T), var(T), mean(D), var(D), t, d, l1
trips = importdata('../res/groupTrip4.res');

estT = trips(:,2);

