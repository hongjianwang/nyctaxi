d = importdata('../../../dataset/l1_t_neighbor');

ratio = d(:,2) ./ d(:,3);
d = [d, ratio];

figure();
cdfplot(d(:,1));
title('CDF of L1/t in testing');


figure();
cdfplot(ratio);
title('CDF of consistent neighbors');


figure();
scatter(d(:,3), d(:,4));
title('consistency v.s. #neighbors');