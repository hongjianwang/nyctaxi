function [r2, MRE, MAE] = error_History( nn_lower, nn_upper )

    % column of trips
    % # neighbors, mean(T), var(T), mean(D), var(D), t, d, l1
    trips = importdata('../res/groupTrip8.res');

    nn = trips(:,1);
    
    if nargin >= 1
        trips = trips(nn>nn_lower,:);
        nn = trips(:,1);
    end
    if nargin >= 2
        trips = trips(nn<=nn_upper,:);
    end
    size(trips)
    
    % use historical data
    mean_T = trips(:,2);
    t = trips(:, 6);
    MRE = sum(abs(mean_T - t)) / sum(t);
    MAE = sum(abs(mean_T - t)) / length(t);
    r2 = r2error( mean_T, t );
end