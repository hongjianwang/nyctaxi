%% initialization



method = {'AVG', 'TEMP-rela', 'TEMP-rela+R', 'TEMP-abs', 'TEMP-ARIMA', ...
    'TEMP-abs+R', 'TEMP-ARIMA+R', 'Dist/vref'};

mae = zeros(1, 8);
mre = zeros(1, 8);
medianE = zeros(1, 8);
medianRE = zeros(1, 8);



%% training with filter -- use November to evaluate December
fprintf('Evaluate December by November - with filter\n');
% input data has 11 fileds
% 1. actual travel time
% 2. AVG
% 3. TEMP-rela
% 4. TEMP-rela+R
% 5. TEMP-abs
% 6. TEMP-ARIMA
% 7. TEMP-abs+R
% 8. TEMP-ARIMA+R
% 9. use $dist / vref$ instead of $time * vref$ 
% 10. neighbor count
% 11. actual distance
d = importdata('../../../dataset/7filter/2013-12.11.eval');
d = d(d(:,10) > 0,:);

% [~, idx] = ismember(c(:,4), d(:,7));
% d = d(idx, :);

gnd_all = d(:,1);

for i = 1:8
    est = d(:,i+1);
    mae(i) = sum(abs(gnd_all-est)) / length(gnd_all);
    mre(i) = sum(abs(gnd_all-est)) / sum(gnd_all);
    medianE(i) = median(abs(gnd_all-est));
    medianRE(i) = median(abs(gnd_all-est) ./ gnd_all);
    fprintf('%s\t%g\t%g\t%g\t%g\n', method{i}, mae(i), mre(i), medianE(i), medianRE(i));
end


lr_d = importdata('../../../dataset/7filter/2013-11.LR.trip');
time = lr_d(:,2);
dist = lr_d(:,1);


p1 = polyfit(dist, time, 1);
fitT = polyval(p1, d(:,11));


mae(9) = sum(abs(fitT - gnd_all)) / length(gnd_all);
mre(9) = sum(abs(fitT - gnd_all)) / sum(gnd_all);
medianE(9) = median(abs(fitT-gnd_all));
medianRE(9) = median(abs(fitT-gnd_all) ./ gnd_all);
fprintf('LR\t%g\t%g\t%g\t%g\n', mae(9), mre(9), medianE(9), medianRE(9));



%% training without filter  -- use November to evaluate December

do = importdata('../../../dataset/raw/2013-12.11-11.eval');
do = do(do(:,10) > 0,:);

fprintf('Evaluate December by November - without filter\n');

gnd_all = do(:,1);

for i = 1:8
    est = do(:,i+1);
    mae(i) = sum(abs(gnd_all-est)) / length(gnd_all);
    mre(i) = sum(abs(gnd_all-est)) / sum(gnd_all);
    medianE(i) = median(abs(gnd_all-est));
    medianRE(i) = median(abs(gnd_all-est) ./ gnd_all);
    fprintf('%s\t%g\t%g\t%g\t%g\n', method{i}, mae(i), mre(i), medianE(i), medianRE(i));
end


% linear regression
% lr_d = importdata('../../../dataset/7filter/2013-11.LR.trip');
lr_do = importdata('../../../dataset/raw/2013-11.LR.trip-outlier');
% lr_d = [lr_d; lr_do];
time = lr_d(:,2);
dist = lr_d(:,1);


p1 = polyfit(dist, time, 1);
fitT = polyval(p1, do(:,11));


mae(9) = sum(abs(fitT - gnd_all)) / length(gnd_all);
mre(9) = sum(abs(fitT - gnd_all)) / sum(gnd_all);
medianE(9) = median(abs(fitT-gnd_all));
medianRE(9) = median(abs(fitT-gnd_all) ./ gnd_all);
fprintf('LR\t%g\t%g\t%g\t%g\n', mae(9), mre(9), medianE(9), medianRE(9));


%% testing without filter / training with filter -- use good November to evaluate December

do = importdata('../../../dataset/raw/2013-12.11-11.eval');
doo = importdata('../../../dataset/raw/2013-12.11-11.eval-outliers');

do = [do; doo];
do = do(do(:,10) > 0, :);

gnd_all = do(:,1);

for i = 1:8
    est = do(:,i+1);
    mae(i) = sum(abs(gnd_all-est)) / length(gnd_all);
    mre(i) = sum(abs(gnd_all-est)) / sum(gnd_all);
    medianE(i) = median(abs(gnd_all-est));
    medianRE(i) = median(abs(gnd_all-est) ./ gnd_all);
    fprintf('%s\t%g\t%g\t%g\t%g\n', method{i}, mae(i), mre(i), medianE(i), medianRE(i));
end


% linear regression
lr_d = importdata('../../../dataset/7filter/2013-11.LR.trip');
lr_do = importdata('../../../dataset/raw/2013-11.LR.trip-outlier');
% lr_d = [lr_d; lr_do];
time = lr_d(:,2);
dist = lr_d(:,1);


p1 = polyfit(dist, time, 1);
fitT = polyval(p1, do(:,11));


mae(9) = sum(abs(fitT - gnd_all)) / length(gnd_all);
mre(9) = sum(abs(fitT - gnd_all)) / sum(gnd_all);
medianE(9) = median(abs(fitT-gnd_all));
medianRE(9) = median(abs(fitT-gnd_all) ./ gnd_all);
fprintf('LR\t%g\t%g\t%g\t%g\n', mae(9), mre(9), medianE(9), medianRE(9));



