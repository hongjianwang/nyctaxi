%% load data


method = {'AVG', 'TEMP-rel', 'TEMP-rel+R', 'TEMP-abs', 'TEMP-ARIMA', ...
    'TEMP-abs+R', 'TEMP-ARIMA+R', 'Dist/vref'};
% input data has 11 fileds
% 1. actual travel time
% 2. AVG
% 3. TEMP-rela
% 4. TEMP-rela+R
% 5. TEMP-abs
% 6. TEMP-ARIMA
% 7. TEMP-abs+R
% 8. TEMP-ARIMA+R
% 9. use $dist / vref$ instead of $time * vref$ 
% 10. neighbor count
% 11. actual distance
d = importdata('../../../dataset/7filter/2013-12.eval');
d = d(d(:,10) > 0,:);
IDX = [2, 3, 4, 6, 8];
METHOD = {'AVG', '$\texttt{TEMP}_{\texttt{rel}}$', ...
    '$\texttt{TEMP}_{\texttt{rel}}+\texttt{R}$', ...
    '$\texttt{TEMP}_{\texttt{abs}}$', ...
    '$\texttt{TEMP}_{\texttt{abs}}+\texttt{R}$'};

% [~, idx] = ismember(c(:,4), d(:,7));
% d = d(idx, :);

gnd_all = d(:,1);


%% MAE w.r.t. trip actual distance

dist = d(:,11);

delta = (max(dist) - min(dist)) / 100;
range = min(dist):delta:max(dist);

mae = zeros(length(range)-1, length(IDX));
mre = zeros(length(range)-1, length(IDX));
medianAE = zeros(length(range)-1, length(IDX));
xlim_max = 0;

for j = 2:length(range)
    d_sub = d(d(:,11) < range(j), :);
    d_sub = d_sub( d_sub(:,11) >= range(j-1), :);
    
    if (size(d_sub, 1) > 1000)
        xlim_max = range(j);
        gnd_sub = d_sub(:,1);

        for i = 1:length(IDX)
            est = d_sub(:, IDX(i));
            mae(j-1, i) = sum(abs(gnd_sub-est)) / length(gnd_sub);
            mre(j-1, i) = sum(abs(gnd_sub-est)) / sum(gnd_sub);
            medianAE(j-1, i) = median( abs(gnd_sub-est) );
        end
    else
        mae(j-1,:) = NaN;
        mre(j-1,:) = NaN;
        medianAE(j-1,i) = NaN;
    end
end


% ========================================
load NYC_tripProperty_dist.mat
% ========================================


% plot MAE
figure();
l = plot(range(1:100), mae);
xlim([0, xlim_max]);
legend(METHOD, 'fontsize', 20, 'interpreter', 'latex', 'location', 'northwest');
box on;
grid on;
set(gca, 'linewidth', 3, 'fontsize', 18);
set(l, 'linewidth', 4);
set(l(3), 'linestyle', '--');
set(l(5), 'linestyle', '--');

xlabel('Trip actual distance (mile)', 'fontsize', 24);
ylabel('MAE (second)', 'fontsize', 24);


fn = 'nyc-mae-dist.eps';
print(gcf, fn, '-dpsc');
system(['epstopdf ', fn]);
delete(fn);


% plot MRE
figure();
l = plot(range(1:100), mre);
xlim([0, xlim_max]);
legend(METHOD, 'fontsize', 20, 'interpreter', 'latex', 'location', 'northeast');
box on;
grid on;
set(gca, 'linewidth', 3, 'fontsize', 18);
set(l, 'linewidth', 4);
set(l(3), 'linestyle', '--');
set(l(5), 'linestyle', '--');

xlabel('Trip actual distance (mile)', 'fontsize', 24);
ylabel('MRE (%)', 'fontsize', 24);


fn = 'nyc-mre-dist.eps';
print(gcf, fn, '-dpsc');
system(['epstopdf ', fn]);
delete(fn);

%% MAE w.r.t. trip travel time

time = d(:,1);

delta = (max(time) - min(time)) / 100;
range = min(time):delta:max(time);

mae = zeros(length(range)-1, length(IDX));
mre = zeros(length(range)-1, length(IDX));
xlim_max = 0;


for j = 2:length(range)
    d_sub = d(d(:,1) < range(j), :);
    d_sub = d_sub( d_sub(:,1) >= range(j-1), :);
    
    if size(d_sub, 1) > 1000
        gnd_sub = d_sub(:,1);
        xlim_max = range(j);
        
        for i = 1:length(IDX)
            est = d_sub(:, IDX(i));
            mae(j-1, i) = sum(abs(gnd_sub-est)) / length(gnd_sub);
            mre(j-1, i) = sum(abs(gnd_sub-est)) / sum(gnd_sub);
        end
    else
        mae(j-1,:) = NaN;
        mre(j-1,:) = NaN;
    end
end


% ========================================
load NYC_tripProperty_time.mat
% ========================================

% plot MAE
figure();
l = plot(range(1:100), mae);
xlim([0, xlim_max]);
legend(METHOD, 'fontsize', 20, 'interpreter', 'latex', 'location', 'northwest');
box on;
grid on;
set(gca, 'linewidth', 3, 'fontsize', 18);
set(l, 'linewidth', 4);
set(l(3), 'linestyle', '--');
set(l(5), 'linestyle', '--');

xlabel('Trip travel time (second)', 'fontsize', 24);
ylabel('MAE (second)', 'fontsize', 24);


fn = 'nyc-mae-time.eps';
print(gcf, fn, '-dpsc');
system(['epstopdf ', fn]);
delete(fn);


% plot MRE
figure();
l = plot(range(1:100), mre);
xlim([0, xlim_max]);
legend(METHOD, 'fontsize', 20, 'interpreter', 'latex', 'location', 'northeast');
box on;
grid on;
set(gca, 'linewidth', 3, 'fontsize', 18);
set(l, 'linewidth', 4);
set(l(3), 'linestyle', '--');
set(l(5), 'linestyle', '--');

xlabel('Trip travel time (second)', 'fontsize', 24);
ylabel('MRE (%)', 'fontsize', 24);


fn = 'nyc-mre-time.eps';
print(gcf, fn, '-dpsc');
system(['epstopdf ', fn]);
delete(fn);





%% MAE w.r.t. #neighbors for each trip

numNeigh = d(:,10);

delta = (max(numNeigh) - min(numNeigh)) / 100;
range = min(numNeigh):delta:max(numNeigh);

mae = zeros(length(range)-1, length(IDX));
mre = zeros(length(range)-1, length(IDX));
xlim_max = 0;

for j = 2:length(range)
    d_sub = d(d(:,10) < range(j), :);
    d_sub = d_sub( d_sub(:,10) >= range(j-1), :);
    
    if (size(d_sub, 1) > 25000)
        xlim_max = range(j);
        gnd_sub = d_sub(:,1);

        for i = 1:length(IDX)
            est = d_sub(:, IDX(i));
            mae(j-1, i) = sum(abs(gnd_sub-est)) / length(gnd_sub);
            mre(j-1, i) = sum(abs(gnd_sub-est)) / sum(gnd_sub);
        end
    else
        mae(j-1,:) = NaN;
        mre(j-1,:) = NaN;
    end
end

% plot mae
figure();
l = plot(range(1:100), mae);
xlim([0, xlim_max]);
legend(METHOD, 'fontsize', 20, 'interpreter', 'latex', 'location', 'northeast');
box on;
grid on;
set(gca, 'linewidth', 3, 'fontsize', 18);
set(l, 'linewidth', 4);
set(l(3), 'linestyle', '--');
set(l(5), 'linestyle', '--');

xlabel('Number of neighbors per trip', 'fontsize', 24);
ylabel('MAE (second)', 'fontsize', 24);


fn = 'nyc-mae-nneighb.eps';
print(gcf, fn, '-dpsc');
system(['epstopdf ', fn]);
delete(fn);


% ========================================
load NYC_tripProperty_numNeigh.mat
% ========================================

% plot mae
figure();
l = plot(range(1:100), mre);
xlim([0, xlim_max]);
legend(METHOD, 'fontsize', 20, 'interpreter', 'latex', 'location', 'southeast');
box on;
grid on;
set(gca, 'linewidth', 3, 'fontsize', 18);
set(l, 'linewidth', 4);
set(l(3), 'linestyle', '--');
set(l(5), 'linestyle', '--');

xlabel('Number of neighbors per trip', 'fontsize', 24);
ylabel('MRE (%)', 'fontsize', 24);


fn = 'nyc-mre-nneighb.eps';
print(gcf, fn, '-dpsc');
system(['epstopdf ', fn]);
delete(fn);


