d = importdata('../../../dataset/absRef-5m');


[~, idx] = sort(d(:,1));
d = d(idx,:);


train = d(1:24*30*8,3);
test = d(24*30*8+1:end, 3);

predictor = arima('ARLags', 1:4, 'MALags', 1:4, 'SARLags', 24*7, ...
    'SMALags', 24*7, 'Seasonality', 24*7);

fit = estimate(predictor, train);
[pr, ymse] = forecast(fit, length(test), 'Y0', train);

mae = sum( abs(pr - test)) / length(test);
mse = sum( (pr-test).^2 ) / length(test);

display( sprintf('mse\t%g\nmae\t%g\nYMSE%g\n', mse, mae, mean(ymse)) );