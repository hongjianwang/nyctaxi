from shapely.geometry import Polygon, Point, MultiPolygon
from flowMatch import Manhattan, Neighborhood
from time import strptime


class TweetFlowBasedManhattan( Manhattan ):
    
    @classmethod
    def addPoint( cls, picklat, picklon, droplat, droplon ):
        pickup = Point( picklon, picklat )
        dropoff = Point( droplon, droplat )
        src = None
        dst = None
        srcflag = False
        dstflag = False
        for n in cls.neighborhoods:
            if not srcflag and n != -1 and cls.neighborhoods[n].contains( pickup ):
                src = cls.neighborhoods[n]
                srcflag = True
            if not dstflag and n != -1 and cls.neighborhoods[n].contains( dropoff ):
                dst = cls.neighborhoods[n]
                dstflag = True
            if srcflag and dstflag:
                break
                
        if not srcflag:
            src = cls.oth
        if not dstflag:
            dst = cls.oth
        
        if cls.t_interval == 'day':
            src.outBoundInc( dst )

            
            
class TweetLocBasedManhattan( Manhattan ):

    @classmethod
    def addField_Neighborhoods( cls ):
        for n in cls.neighborhoods:
            cls.neighborhoods[n].tweets = [0 for i in range(7)]
    
    @classmethod
    def addPoint( cls, lat, lon, dweek):
        loc = Point( lon, lat )     # the Point format is lon, lat
        contain_flag = False
        tagetRegion = None
        for n in cls.neighborhoods:
            if n != -1 and cls.neighborhoods[n].contains( loc ):
                targetRegion = cls.neighborhoods[n]
                contain_flag = True
                break
        if not contain_flag:
            targetRegion = cls.oth
            
        targetRegion.tweets[dweek] += 1
            
                    
    @classmethod
    def getDensityMap( cls ):
        dmap = {id: cls.neighborhoods[id].tweets for id in cls.neighborhoods}
        return dmap
        
        
    
    @classmethod
    def createFiles( cls ):
        cls.files = {}
        for id in cls.neighborhoods:
            if id != -1:
                cls.files[id] = open( '../../dataset/NYC_Taxi', 'w' )
    
    
    @classmethod
    def splitFile( cls, line ):
        ls = line.strip().split('|')
        lat = float( ls[2] )
        lon = float( ls[3] )
        content = float( ls[4] )
        p = Point( lon, lat )
        for n in cls.neighborhoods:
            if n != -1 and cls.neighborhoods[n].contains( p ):
                cls.files[n].write( content + '\n' )
                
                
                
    @classmethod
    def closeFile( cls ):
        for file in cls.files.values():
            file.close()
            
            
            
            

def tweetsFlowBased_GraphConstruction():            
    neighborhoods, oth = TweetFlowBasedManhattan.getNeighborhoods(t_interval = 'day')
    
    cnt = 0
    
    with open('../../dataset/nyct') as fin:
        prev_uid = -1
        prev_lat = 0
        prev_lon = 0
        for l in fin:
            ls = l.strip().split('\t')
            lat = float( ls[2] )
            lon = float( ls[3] )
            uid = int( ls[0] )
            if uid == prev_uid:
                TweetBasedManhattan.addPoint( prev_lat, prev_lon, lat, lon)
            prev_uid = uid
            prev_lat = lat
            prev_lon = lon
            
            cnt += 1
            if cnt % 1000 == 0:
                print cnt
    

    ID, G = TweetFlowBasedManhattan.getTrafficGraph()
    with open('res/tweet.graph', 'w') as fout:
        fout.write(str(ID) + '\n')
        fout.write(str(G))
        
        
def tweetsLocBased_GraphConstruction():
    neighborhoods, oth = TweetLocBasedManhattan.getNeighborhoods(t_interval = 'day')
    TweetLocBasedManhattan.addField_Neighborhoods()
    cnt = 0
    
    with open('../../dataset/nyct') as fin:
        # head = [next(fin) for i in range(10000)]
        for l in fin:
            ls = l.strip().split('\t')
            lat = float( ls[2] )
            lon = float( ls[3] )
            datestr = ls[1][0:8]
            d = strptime( datestr, "%Y%m%d" ).tm_wday
            TweetLocBasedManhattan.addPoint( lat, lon, d )

            cnt += 1
            if cnt % 1000 == 0:
                print cnt
                
    dmap = TweetLocBasedManhattan.getDensityMap()
    with open('res/tweet.density', 'w') as fout:
        fout.write(str(dmap))
    
    return neighborhoods, oth
        
            
            

def tweets_Split_File():
    neighborhoods, oth = TweetLocBasedManhattan.getNeighborhoods(t_interval = 'day')
    cnt = 0
    
    TweetLocBasedManhattan.createFiles()
    

    with open('../../dataset/nyct-content') as fin:
        head = [next(fin) for i in range(10000)]
        for l in head:
            TweetLocBasedManhattan.splitFile( l )

            cnt += 1
            if cnt % 1000 == 0:
                print cnt
    
    TweetLocBasedManhattan.closeFile()
    
            
if __name__ == '__main__':
    n, o = tweetsLocBased_GraphConstruction()
    
    