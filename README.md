# New York City Data Exploration Project #


##The project [wiki](https://bitbucket.org/hongjianwang/nyctaxi/wiki/Home) is available now.##

### What data are used ? ###

* New York City taxi trips with only pickup and dropoff location
* New York City Neighborhood Tabulation Area information
* New York City neighborhood boundary defined by [Wikimapia](http://wikimapia.org).


### Fold Structure 


1. data --- contains intermediate files
2. res  --- contains result
3. *.py --- various scripts


### Fold Dependency


flowMatch.py

functions

+ define `Neighborhood` class
	1. create a neighborhood instance from xml node
	2. calculate the outbound flow
+ define  `Manhattan` class, contains all neighborhoods in Manhattan
	1. map one trip into corresponding region pairs.
	2. calculate the inbound flow
+ output the neighborhoods graph in a **dot** file

depends

* retrieveGeo.py

external library dependency

* Class `Polygon`, `Point`, `MultiPolygon` from `shapely` library


---------------------------------
	
clustering.py

functions

* cluster the neighborhoods by their time series
* aggregate the region profiles by cluster

depends 

* retrieveNTCProfile.py

external library dependency

* `array` from `numpy`
* `euclidean` from `scipy`
* `KMeans`, `metrics` from `sklearn`


---------------------------------	

minimumCut.py

functions

* minimum edge cut of the Manhattan neighborhoods traffic flow graph

depends

* flowMatch.py


--------------------------------

pruneGraph.py

functions

+ read in a dot file and filter the edges w.r.t the `weight` condition
+ generate the pruned graph as a pdf file

external software dependency

+ `dot` from `graphviz` software


--------------------------------

retrieveGeo.py

functions

+ split the trip data by day
+ crawl neighborhood ID in Manhattan from **wikimapia**
+ crawl neighborhood boundary from **wikimapia**
+ save neighborhoods' boundary as an xml file


----------------------------------


retrieveNTCProfile.py

functions

+ parse the census 'xlsx' file to get the attributes of each region
+ get the name of each field


external library dependency

+ `xlrd` library



---------------------------------


shpParser.py

functions
+ parse the arcGIS standard map shapefile (.shp)
+ retrieve the NYC Neighborhood Tabulation Area (NTA) boundary
+ save boundary as an xml file

external library dependency
+ `shapefile` --- to handle the shapefile
	
	
	
---------------------------------


tripStatistics.py

functions
+ statistics on trip distance, time, fare, and average speed