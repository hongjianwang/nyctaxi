"""
Retrieve the Manhattan neighborhoods boundary from Wikimapia online service

1. crawl all neighborhoods' ID and names
2. crawl boundary for each neighborhood
3. save them as xml

Hongjian Wang
"""



from time import strptime, strftime
from urllib import urlencode, urlopen
from xml.etree.ElementTree import Element, fromstring, tostring
from collections import deque



filePath = '..\\..\\dataset\\NYC_Taxi\\trip_Jan.csv'


def generateFileName(day, month = 1):
    return strftime('data/%b%d.csv', (2013, month, day, 0, 0, 0, 0, 0,0) )

def getOneDayTrips( name_fin, month, day ):
    
    name_fout = generateFileName(day, month)
    with open(name_fin) as f1, open(name_fout, 'w') as f2:
        for line in f1:
            lsp = line.split(',')
            pickupT = strptime( lsp[5], '%Y-%m-%d %H:%M:%S' )
            if pickupT.tm_mon == month and pickupT.tm_mday == day:
                f2.write(line)
                


def splitTripIntoDay( name_fin ):
    fouts = []
    for i in range(1, 32):
        name_fout = generateFileName(i)
        fouts.append( open(name_fout, 'w') )
        
    with open(name_fin) as fin:
        for line in fin:
            lsp = line.split(',')
            pickupT = strptime( lsp[5], '%Y-%m-%d %H:%M:%S' )
            fouts[pickupT.tm_mday - 1].write(line)

    for f in fouts:
        f.close()

                
def findNeighborhood():
    baseurl = 'http://api.wikimapia.org/?'
    paras = { 'key': 'B2362614-2CC84F7A-76999645-444CEE4F-6A4FD228-995B3EC0-638D6766-F92BBA99',
        'function': 'place.getbyarea',
        'lon_min': -74.111213,
        'lat_min': 40.528939,
        'lon_max': -73.659400,
        'lat_max': 40.930637,
        'count': 100,
        'page': 1,
        'category': 'neighborhood'
    }
    
    # dummy query to get number of results
    paras['count'] = 5
    urlfile = urlopen( baseurl + urlencode(paras) )
    
    with open('data/query.txt', 'w') as fout:
            fout.write(urlfile.geturl())
            
    page = urlfile.read()
    tree = fromstring( page )
    paras['count'] = 100
    
    totalN = int( tree.find('found').text )
    print 'We have {0} results found in total.'.format( totalN )
    neighborhoods = []
    
    for pageID in range(1, totalN / paras['count'] + 2):
        # get the right page (starts from 1)
        paras['page'] = pageID
        urlfile = urlopen( baseurl + urlencode(paras) )
        page = urlfile.read()
        tree = fromstring( page )
        
        places = list(tree.find('places'))
        for place in places:
            if place.find('location').find('city').text == 'New York City, New York':
                print place.find('id').text, place.find('title').text
                neighborhoods.append( int(place.find('id').text ) )
        
        
    
    return neighborhoods

class Region:
     
    key = [ 'B2362614-2CC84F7A-76999645-444CEE4F-6A4FD228-995B3EC0-638D6766-F92BBA99',
            'B2362614-E6BF3696-ED32D632-F110227B-5370B047-45589541-5A1F112C-4EC33169',
            'B2362614-616E7907-121ABE4F-6896D9F3-0D71511B-8399F75C-8FF572BC-8C244D81',
            'B2362614-C8C6AB9F-0796DEA9-14BC9F56-DA7F05FF-A6FF42FF-854CAD45-DC02004A'  ]
    keyCnt = 0
    baseurl = 'http://api.wikimapia.org/?function=place.getbyid&'
     
    def __init__(self, id):
        url_para = urlencode( {'key': Region.key[Region.keyCnt / 100], 'id': id } )
        Region.keyCnt += 1
        
        urlfile= urlopen( Region.baseurl + url_para )
        
        self.page = urlfile.read()
        tree = fromstring(self.page)
        self.id = id
        try:
            self.title = tree.find('title').text
            self.similarPlaces = tree.find('similarPlaces')
            
            polygon = tree.find('polygon')
            x, y = 0, 0
            self.boundary = []
            for a in polygon.iter():
                if a.tag == 'x':
                    x = float( a.text )
                if a.tag == 'y':
                    y = float( a.text )
                    self.boundary.append( (x, y) )
        except Exception:
            print self.id, tree
            
                
                
                
    def findSimilarLocID( self ):
        ids = []
        for t in self.similarPlaces.iter():
            if t.tag == 'id':
                ids.append( int(t.text) )
        
        return ids


        
    def __str__(self):
        return self.title.text
        
        
    def saveToFile( self, f ):
        try:
            root = Element('neighborhood')
            idNode = Element('id')
            idNode.text = str(self.id)
            root.append( idNode )
            titleNode = Element('title')
            titleNode.text = self.title
            root.append( titleNode )
            boundary = Element('polygon')
            root.append(boundary)
            for x, y in self.boundary:
                point = Element('point')
                lonNode = Element('lon')
                lonNode.text = str(x)
                latNode = Element('lat')
                latNode.text = str(y)
                point.append( lonNode )
                point.append( latNode )
                boundary.append(point)
            
            f.write( tostring(root, method='xml') )
        except Exception as e:
            print self.id, e

        


def main_use_similar():
    """Give three neighborhoods ID as a start.
    query each page.
    select the similarPlaces field from the xml file to extend to new neighborhoods"""
    fout = open('data/boundary.xml', 'w')
    fout.write("<?xml version='1.0' encoding='UTF-8'?>\n<neighborhoods>\n")
    
    # neighborhood ID: china town, 
    idQueue = deque([ 1335578, 22334544, 1714543])
    retrived_ids = []
    while len(idQueue) > 0:
        id = idQueue.popleft()
        
        if id not in retrived_ids:
            retrived_ids.append(id)
            
            region = Region(id)
            region.saveToFile( fout )
            print region.id, region.title
            children = region.findSimilarLocID()
            for c in children:
                if c not in idQueue:
                    idQueue.append( c )
                
    fout.write('</neighborhoods>\n')
    fout.close()

    
    
def main_save_neighborhood_ID():
    """Give a bounding box of the New York City.
    Query all neighborhood object from the city.
    Select those neighborhoods that belong to NYC."""
    nid = findNeighborhood()
    with open('data/neighborhoods.txt', 'w') as f:
        for n in nid:
            f.write('{0}\n'.format(n))
    return nid

    
def main_retrieve_neighborhoods_boundary():
    with open('data/neighborhoods.txt') as fin, open('data/boundary.xml', 'w') as fout:
        fout.write("<?xml version='1.0' encoding='UTF-8'?>\n<neighborhoods>\n")
        for line in fin:
            id = int (line)
            r = Region(id)
            r.saveToFile(fout)
            print r.id, r.title
        fout.write('</neighborhoods>\n')
    
    
    
if __name__ == '__main__':
    # splitTripIntoDay(filePath)
    # getOneDayTrips(filePath, 1, 1)
   # main_save_neighborhood_ID()
   main_retrieve_neighborhoods_boundary()
            
            
