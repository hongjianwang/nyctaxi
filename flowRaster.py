# -*- coding: utf-8 -*-
"""
Created on Mon Oct 27 18:00:20 2014

@author: hxw186
"""


from time import strptime, clock
#from multiprocessing.managers import BaseManager
from multiprocessing import Process, Queue
from retrieveGeo import generateFileName
import Queue as Q
import operator
import matplotlib.pyplot as plt
from sklearn.cluster import *




class Grid:
    """
    One Gird is the smallest location unit of study
    """
    
    def __init__( self, southwest, height, width ):
        self.southwest = southwest
        self.northeast = [southwest[0] + height, southwest[1] + width]
        self.pickup = {}
        self.dropoff = {}
        

    def pickupInc(self, dayOfYear, hourOfDay):
        hour_idx = dayOfYear * 24 + hourOfDay
        if hour_idx in self.pickup:
            self.pickup[hour_idx] += 1
        else:
            self.pickup[hour_idx] = 1
            
            
    def dropoffInc(self, dayOfYear, hourOfDay):
        hour_idx = dayOfYear * 24 + hourOfDay
        if hour_idx in self.dropoff:
            self.dropoff[hour_idx] += 1
        else:
            self.dropoff[hour_idx] = 1


    def mergeGrid( self, g ):
        self.pickup.update(g.pickup)
        self.dropoff.update(g.dropoff)
        
        
    def center( self ):
        lat = (self.southwest[0] + self.northeast[0]) / 2.0
        lon = (self.southwest[1] + self.northeast[1]) / 2.0
        return [lat, lon]
        
        
    def aggreateByHourIntoDay( self ):
        """
        Aggregate the traffic into day
        """
        pickH = [0 for i in range(24)]
        dropH = [0 for i in range(24)]
        for i in self.pickup:
            pickH[i%24] += self.pickup[i]
            
        for j in self.dropoff:
            dropH[j%24] += self.dropoff[j]
            
        self.pickH = pickH
        self.dropH = dropH
        pmin_idx, pmin_val = max( enumerate(self.pickH), key=operator.itemgetter(1) )
        dmin_idx, dmin_val = max( enumerate(self.dropH), key=operator.itemgetter(1) )
        return pmin_idx, pmin_val, dmin_idx, dmin_val


class Raster:
    """
    The Manhattan of NYC is represented as a Raster
    
    A Raster is consisted of many Grids      
    """
    SouthWest = [ 40.680120, -74.061088 ]       # latitude, longitude
    NorthEast = [ 40.879000, -73.777503 ]
    
    
    def __init__( self, H = 100, W = 100 ):
        self.Grids = []
        self.gridH = (Raster.NorthEast[0] - Raster.SouthWest[0] ) / H
        self.gridW = (Raster.NorthEast[1] - Raster.SouthWest[1] ) / W
        for i in range(H):
            rows = []
            for j in range(W):
                org = [ Raster.SouthWest[0] + i * self.gridH, Raster.SouthWest[1] + j * self.gridW ]
                rows.append( Grid(org, self.gridH, self.gridW))                
            self.Grids.append(rows)
            

    def addPoint( self, line ):
        ls = line.split(',')
        pickup = [ float(ls[11]), float(ls[10]) ]
        dropoff =[ float(ls[13]), float(ls[12]) ]
        
        gp = self.findGrid( pickup )
        if gp:
            pickupT = strptime( ls[5], '%Y-%m-%d %H:%M:%S' )
            dayOfYear = pickupT.tm_yday - 1     # range [0,365]
            hourOfDay = pickupT.tm_hour      # range [0,23]
            gp.pickupInc(dayOfYear, hourOfDay)
        
        gd = self.findGrid( dropoff )
        if gd:
            dropofT = strptime( ls[6], '%Y-%m-%d %H:%M:%S' )
            dayOfYear = dropofT.tm_yday - 1
            hourOfDay = dropofT.tm_hour
            gd.dropoffInc(dayOfYear, hourOfDay)



    def findGrid(self, coords):
        lat = coords[0]
        lon = coords[1]
        rowID = int( ( lat - Raster.SouthWest[0] ) / self.gridH )
        colID = int( ( lon - Raster.SouthWest[1] ) / self.gridW )
        res = None
        try:
            res = self.Grids[rowID][colID]
        except IndexError:
            pass
        return res
        
    
    def saveToFile( self ):
        with open('res/grids_pickup.txt', 'w') as fpickup, open('res/grids_dropoff.txt', 'w') as fdropoff:
            for rows in self.Grids:
                for cell in rows:
                    fpickup.write(str(cell.pickup) + '\n')
                    fdropoff.write(str(cell.dropoff) + '\n')
                    
    def readFromFile( self ):
        with open('res/grids_pickup.txt') as fpickup, open('res/grids_dropoff.txt') as fdropoff:
            for rows in self.Grids:
                for cell in rows:
                    lp = fpickup.readline()
                    cell.pickup = eval(lp)
                    ld = fdropoff.readline()
                    cell.dropoff = eval(ld)
                    
                    
    def peakTrafficHourDistribution( self ):
        PI = []
        P = []
        DI =[]
        D = []
        for rows in self.Grids:
            for cell in rows:
                pi, p, di, d = cell.aggreateByHourIntoDay()
                if p != 0:
                    PI.append(pi)
                    P.append(p)
                if d != 0:
                    DI.append(di)
                    D.append(d)
                
        
        plt.figure()
        plt.hist(PI, bins=24)
        plt.title('Max pickup hour Hist of {0} days'.format(len(PI)) )
        plt.show(block=False)
        
        plt.figure()
        plt.hist(DI, bins=24)
        plt.title('Max dropoff hour Hist of {0} days'.format(len(DI)) )
        plt.show(block=False)
                    
    
                    
    def kMeansCluster( self ):
        Xp = []
        Xd = []
        Pgrids = []
        Dgrids = []
        for rows in self.Grids:
            for cell in rows:
                pi, p, di, d = cell.aggreateByHourIntoDay()
                if p != 0:
                    cell.maxp = p
                    cell.maxp_idx = pi
                    Xp.append( cell.pickH )
                    Pgrids.append(cell)
                if d != 0:
                    cell.maxd = d
                    cell.maxd_idx = di
                    Xd.append( cell.dropH )
                    Dgrids.append(cell)
        
        km = KMeans(n_clusters = 5, random_state = 1 )
        km.fit(Xp)
        for i, l in enumerate( km.labels_ ):
            Pgrids[i].plabel = l
        
        km = KMeans(n_clusters = 5, random_state = 1 )
        km.fit(Xd)
        for i, l in enumerate( km.labels_ ):
            Dgrids[i].dlabel = l
            
        return Pgrids, Dgrids
        
          
        

#class RasterManager(BaseManager):
#    pass
#
#RasterManager.register('Raster', Raster, exposed = ['addPoint', 'findGrid'])


def flowMapping(q, fids):
    print 'Execute flowMapping with file IDs', fids
    raster = Raster(100,100)
    for i in fids:
        fname = generateFileName(i)
        print "Process file {0}".format(fname)
        with open(fname) as fin:
            cnt = 0
            for line in fin:
                raster.addPoint(line)
                cnt += 1
                if cnt % 100000 == 1:
                    print fname, cnt
    q.put(raster)
    print 'Subprocess with file IDs', fids, 'finished'



def mapFlowOnGrids_multiprocessing( NumPartition = 5 ):
    fids = range(1,32)
    chunkSize = len(fids) / NumPartition + 1
    chunks = [fids[x:x+chunkSize] for x in range(0, len(fids), chunkSize)]
    
    NumProc = len(chunks)
    procPools = []
    rasters = []
    q = Queue()
    for chunk in chunks:
        proc = Process(target=flowMapping, args=(q,chunk))        
        procPools.append(proc)
        proc.start()
        
    
    while len(rasters) < NumProc:
        try:
            r = q.get(timeout=1)
            if r:
                rasters.append(r)
                print 'get one element', len(rasters)
        except Q.Empty:
            pass
            
    print 'start join'
    for proc in procPools:
        print 'process with pid {0} joined'.format(proc.pid)
        proc.join()
        
    
    
    raster = rasters.pop(0)
    for i in range(100):
        for j in range(100):
            for r in rasters:
                raster.Grids[i][j].mergeGrid( r.Grids[i][j] )
                
    return raster

    

def flowMappingSingleProc(raster, fids):
    print 'Execute flowMapping with file IDs', fids
    for i in fids:
        fname = generateFileName(i)
        print "Process file {0}".format(fname)
        with open(fname) as fin:
            cnt = 0
            for line in fin:
                raster.addPoint(line)
                cnt += 1
                if cnt % 100000 == 1:
                    print fname, cnt



def clusterAnalysis( pgrids, dgrids ):
    MAX = [0 for i in range(5)]
    MAXIDX =[0 for i in range(5)]
    TRA = [0 for i in range(5)]
    cnt = [0.0 for i in range(5)]
    for cel in pgrids:
        l = cel.plabel
        MAX[l] += cel.maxp
        MAXIDX[l] += cel.maxp_idx
        TRA[l] += sum( cel.pickH )
        cnt[l] += 1
       
    color = ['Red', 'Green', 'Black', 'Blue', 'Yellow']
    print 'Pickup clusters'
    for i in range(5):
        MAX[i] /= cnt[i]
        MAXIDX[i] /= cnt[i]
        TRA[i] /= cnt[i]
        print color[i], '\t\t', MAX[i], '\t\t', MAXIDX[i], '\t\t', TRA[i]
    
    MAX = [0 for i in range(5)]
    MAXIDX =[0 for i in range(5)]
    TRA = [0 for i in range(5)]
    cnt = [0.0 for i in range(5)]
    for cel in dgrids:
        l = cel.dlabel
        MAX[l] += cel.maxd
        MAXIDX[l] += cel.maxd_idx
        TRA[l] += sum( cel.dropH )
        cnt[l] += 1
       
    print 'Dropoff clusters'
    for i in range(5):
        MAX[i] /= cnt[i]
        MAXIDX[i] /= cnt[i]
        TRA[i] /= cnt[i]
        print color[i], '\t\t', MAX[i], '\t\t', MAXIDX[i], '\t\t', TRA[i]
    
    



                    

if __name__ == '__main__':
    st = clock()
    ######## Using subprocesses to calculate the Raster ##########
#    mm = RasterManager()
#    mm.start()
#    syncedRaster = mm.Raster(100, 100)
#    raster = mapFlowOnGrids_multiprocessing(5)
    raster = Raster()
    raster.readFromFile()
#    raster.saveToFile()
    l1, l2 = raster.kMeansCluster()
    clusterAnalysis(l1, l2)
    

    
    
    ######## Using single process to calculate the Raster ##########
#    raster = Raster(100,100)
#    flowMappingSingleProc(raster, range(1,32))
    print "--- {0} seconds ---".format(clock() - st)
    
        