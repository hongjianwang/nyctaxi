import BaseHTTPServer
import SimpleHTTPServer
import sys
from urlparse import urlparse, parse_qs
from urllib import unquote
import json
from datetime import datetime

import os
from retrieveNTCProfile import Census
from flowRaster import Raster
from time import clock


class Handler ( SimpleHTTPServer.SimpleHTTPRequestHandler ):
    
    def do_GET(self):
        out = urlparse( self.path )
        print out
        if out.path == '/ntaxml':
            self.send_response(200)
            self.send_header("Content-type", "text/plain")
            self.end_headers()
            # get xml file
            with open('../data/NTA.xml') as fin:
                content = fin.read()
                self.wfile.write( content )
        elif out.path == '/nta':
            self.send_response(200)
            self.send_header("Content-type", "text/plain")
            self.end_headers()
            # get id
            id = out.query
            print id
            t = getNTAProfile( id )
            res = { 'age': t.avg_age, 'race': t.race, 'employ': t.employ, 'income': t.avg_income }
            self.wfile.write( json.dumps(res) )
        elif out.path == '/heatmap':
            self.send_response(200)
            self.send_header("Content-type", "text/plain")
            self.end_headers()
            # get hour_idx
            qs = parse_qs(out.query)
            print qs
            hour_idx = int(qs['hour'][0])
            trafficType = qs['type'][0]
            traffic = heatMapPoint(hour_idx, trafficType)
            self.wfile.write( json.dumps(traffic) )
        elif out.path == '/pickupgrids':
            self.send_response(200)
            self.send_header("Content-type", "text/plain")
            self.end_headers()
            # get pickup grids
            res = gridsClusters('pickup')
            self.wfile.write( json.dumps(res) )
        elif out.path == '/dropoffgrids':
            self.send_response(200)
            self.send_header("Content-type", "text/plain")
            self.end_headers()
            # get dropoff grids
            res = gridsClusters('dropoff')
            self.wfile.write( json.dumps(res) )
        else:
            f = self.send_head()
            if f:
                self.copyfile(f, self.wfile)
                f.close()
                

                
                
census = None
raster = None
pcls = None
dcls = None


def getNTAProfile( id ):
    t = census.regions[id]
    return t
    
    
def heatMapPoint( hour_idx, trafficType ):   
    traffic = []
    if trafficType == 'pickup':
        for rows in raster.Grids:
            for cell in rows:
                if hour_idx in cell.pickup:
                    p = cell.center()
                    p.append(cell.pickup[hour_idx])
                    traffic.append(p)
    elif trafficType == 'dropoff':
        for rows in raster.Grids:
            for cell in rows:
                if hour_idx in cell.dropoff:
                    p = cell.center()
                    p.append(cell.dropoff[hour_idx])
                    traffic.append(p)
    return traffic
       

def gridsClusters(type):
    res = []
    if type == 'pickup':
        print len(pcls)
        for c in pcls:
            tmp = [c.southwest, c.northeast, c.plabel]
            res.append(tmp)
    elif type == 'dropoff':
        print len(dcls)
        for c in dcls:
            tmp = [c.southwest, c.northeast, c.dlabel]
            res.append(tmp)
    
    return res
                
                
if __name__ == '__main__':
    if len(sys.argv) > 1:
        port = int(sys.argv[1])
    else:
        port = 8000
    server_address = ('0.0.0.0', port)
    Server = BaseHTTPServer.HTTPServer    
    
    httpd = Server( server_address, Handler )
    
    sa = httpd.socket.getsockname()
    
    
    t1 = clock()
    # get census data
    print "Initialize census data ..."
    census = Census()
    t2 = clock()
    print "--- {0} seconds ---".format(t2-t1)
    
    # get grids data
    print "Initialize grids data ..."
    raster = Raster()
    raster.readFromFile()
    pcls, dcls = raster.kMeansCluster()
    t3 = clock()
    print "--- {0} seconds ---".format(t3-t2)
    
    
    os.chdir('visualize')
    
    print "Serving HTTP on ", sa[0], "port", sa[1], datetime.now()
    print 'root path', os.getcwd()
    
    httpd.serve_forever()