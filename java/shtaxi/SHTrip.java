package shtaxi;

import java.io.*;
import java.util.*;


public class SHTrip {

	static public int cnt = 0;
	static String sdate = null;
	static String edate = null;

	
	int id;
	float duration;
	float distance;
	LinkedList<SHRecord> records;	// sequence of record in visiting order
	ArrayList<Edge> edges;			// sequence of edge in visiting order (this may differ from records)
	ArrayList<Double> travelTime;	// sequence of edge travel time in visiting order
	
	
	public SHTrip(int c, BufferedReader br) {
		id = cnt++;
		records = new LinkedList<>();
		try {
			for (int i = 0; i < c; i++) {
				String l = br.readLine();
				SHRecord r = new SHRecord(l);
				records.add(r);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		duration = records.getLast().time - records.getFirst().time;
		/*
		if (duration < 30) {
			System.out.println(records.size());
			for (SHRecord rec : records)
				System.out.printf("%s\t", rec.date);
			System.out.println("");
		}
		*/
		distance = getDistance();
	}
	
	
	/**
	 * Pass true to dateString to get the starting and ending date string and save into the class static field.
	 * @param c
	 * @param br
	 * @param dateString must be true to extract the starting and ending time
	 */
	public SHTrip(int c, BufferedReader br, boolean dateString) {
		id = cnt++;
		records = new LinkedList<>();
		try {
			for (int i = 0; i < c; i++) {
				String l = br.readLine();
				if (dateString && i == 0) {
					String[] ls = l.split(",");
					SHTrip.sdate = ls[4];
				} else if (dateString && i == c-1) {
					String[] ls = l.split(",");
					SHTrip.edate = ls[4];
				}
				SHRecord r = new SHRecord(l);
				records.add(r);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		duration = records.getLast().time - records.getFirst().time;
		/*
		if (duration < 30) {
			System.out.println(records.size());
			for (SHRecord rec : records)
				System.out.printf("%s\t", rec.date);
			System.out.println("");
		}
		*/
		distance = getDistance();
	}
	
	
	/**
	 * Trip distance calculated by L2 distance sum over segments 
	 * @return trip distance
	 */
	private float getDistance() {
		distance = 0;
		for (int i = 0; i < records.size() - 1; i++) {
			distance += records.get(i).distanceTo(records.get(i+1));
		}
		return distance;
	}
	
	private float L1distance() {
		SHRecord s = records.getFirst();
		SHRecord e = records.getLast();
		return (Math.abs(s.latitude - e.latitude) + Math.abs(s.longitude - e.longitude)) * 110.0f;
	}
	
	/**
	 * Model one trip 
	 * How many trips consecutive segments are covered.
	 * @param trp
	 * @return percentage of coverage
	 */
	public double segmentCoverage() {
		double res = 0;
		boolean connected = true;
		Edge prev = null;
		for (Edge e : edges) {
			if ( e.numGPS >= 2)
				res ++;
			if (prev != null) {
				if (!prev.connectedToEdge(e))
					connected = false;
			}
			prev = e;
		}
		if (connected)
			return res / this.edges.size();
		else
			return - res/ this.edges.size();
	}
	
	
	
	
	/**
	 * Decompose the trip into optimal sub-paths
	 * @return the optimal decomposition
	 */
	public LinkedList<Subpath> decomposeTrip() {	
		// all optimal decompositions for every first k segments
		ArrayList<LinkedList<Subpath>> subpaths = new ArrayList<>();
		for (int i = 0; i < edges.size(); i++) {
			subpaths.add(new LinkedList<Subpath>());
		}
		
		// the dynamic programming array for measurement
		double[] opt = new double[edges.size()];
		Arrays.fill(opt, Double.MAX_VALUE);
		for (int i = 0; i < edges.size(); i++) {
			for (int j = i; j >= 0; j--) {
				// if the subpath 0:j-1 does not exist, we should not try P' = j:i
				if (j > 1 && opt[j-1] == Double.MAX_VALUE)
					break;
				
				// construct the new sub-trip P' = j:i
				LinkedList<Edge> sub_traj = new LinkedList<>();
				for (int k = j; k <= i; k++)
					sub_traj.add(edges.get(k));
				Subpath sp = new Subpath(sub_traj);
				
				//calculate the measure on sp
				double measure_sp = sp.calculateVarianceMeasure();
				
				// the calculateVarianceMeasure function will initialize sp.trips
				// if P' has only the one trip traversing it
				// we should not rely on the variance (which must be 0)
				if (sp.trips.size() == 1 && i != edges.size() - 1)
					break;
	
				if (j == 0) {
					if (measure_sp < opt[i]) {
						opt[i] = measure_sp;
						subpaths.get(i).clear();
						subpaths.get(i).add(sp);
					}
				}
				else {
					if (measure_sp + opt[j-1] < opt[i]) {
						opt[i] = measure_sp + opt[j-1];
						subpaths.get(i).clear();
						subpaths.get(i).addAll(subpaths.get(j-1));
						subpaths.get(i).add(sp);
					}
				}
			}
		}
//		for (int i = 0; i < edges.size(); i++)
//			System.out.printf("%d:%g\t", subpaths.get(i).size(), opt[i]);
//		System.out.println(" ");
		return subpaths.get(edges.size()-1);
	}
	
	
	/**
	 * Sub-path-based travel time estimator
	 * @return estimated travel time
	 */
	public double subpath_based_estimation() {
		LinkedList<Subpath> res = this.decomposeTrip();
		if (res.size() == 0)
			return -1;
		double tres = 0;
		for (Subpath sp : res) {
			tres += sp.time;
		}
		return tres;
	}
	
	
	/**
	 * Segment-based travel time estimator
	 * @return estimated travel time
	 */
	public double segment_based_estimation() {
		double tres = 0;
		for (Edge e : edges) {
			tres += e.travelTime();
		}
		return tres;
	}
	

	
	public static void writeAllTripsForRegressionFilter( LinkedList<SHTrip> alltrips ) {
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter("../../../dataset/sh-all-trips"));
			for (SHTrip t : alltrips) {
				bw.write(String.format("%g,%g,%g,%d\n", t.duration, t.L1distance(), t.distance, t.id));
			}
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Get a list of IDs satisfying certain conditions. The ID is the last column of the trips.
	 * @param fname the file path of ids
	 * @return a hashset of IDs
	 */
	public static HashSet<Integer> getIDSet(String fname) {
		System.out.printf("Get IDs from %s\n", fname);
		LinkedList<Integer> ids = new LinkedList<>();
		try (BufferedReader fid = new BufferedReader(new FileReader(fname))) {
			String l = null;
			while ((l = fid.readLine()) != null) {
				String[] ls = l.split("[\t,]");
				int d = Integer.parseInt(ls[ls.length - 1]);
				ids.add(d);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		HashSet<Integer> idset = new HashSet<>(ids.size() * 2, 0.7f);
		idset.addAll(ids);
		return idset;
	}
	
	public static LinkedList<Integer> getIDList(String fname) {
		System.out.printf("Get ID list from %s\n", fname);
		LinkedList<Integer> ids = new LinkedList<>();
		try (BufferedReader fid = new BufferedReader(new FileReader(fname))) {
			String l = null;
			while ((l = fid.readLine()) != null) {
				String[] ls = l.split("[\t,]");
				int d = Integer.parseInt(ls[ls.length - 1]);
				ids.add(d);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ids;
	}
	
	
	/**
	 * Guarantee the trips ID by explicitly forcing the files are read in alphabetic order.
	 * @param finFolder
	 * @return file names in alphabetic order
	 */
	private static String[] guarantee_file_order(File finFolder) {
		String[] finNames = finFolder.list();
		Arrays.sort(finNames, new Comparator<String>() {
			@Override
			public int compare(String arg0, String arg1) {
				return arg0.compareTo(arg1);
			}
		});
		return finNames;
	}
	
	/**
	 * Retrieve all trips from the raw data (outliers included)
	 * @return
	 */
	public static HashMap<Integer, SHTrip> getAllTrips() {
		HashMap<Integer, SHTrip> res = new HashMap<>();
		try {
			System.out.println("Initializing all trips ...");
			long t1 = System.currentTimeMillis();
			File finFolder = new File("../../../dataset/SH_Taxi_byCar_trips/");
			String[] finNames = guarantee_file_order(finFolder);
			int fileCnt = 0;
			for (String finName : finNames) {
				BufferedReader fin = new BufferedReader(new FileReader("../../../dataset/SH_Taxi_byCar_trips/" + finName));
				String l = null;
				
				while ((l = fin.readLine()) != null) {
					int cnt = Integer.parseInt(l);
					SHTrip t = new SHTrip(cnt, fin);
					res.put(t.id, t);
				}
				fin.close();
				
				fileCnt ++;
				if (fileCnt % 200 == 0)
					System.out.println(fileCnt);
				
			}
			
			long t2 = System.currentTimeMillis();
			System.out.printf("Finished in %d seconds with %d trips.\n", (t2-t1)/1000, res.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}
	
	
	
	
	/**
	 * Retrieve all trips with their IDs contained in the constraint set.
	 * @param ids
	 * @return
	 */
	public static HashMap<Integer, SHTrip> getAllTrips(HashSet<Integer> ids) {
		HashMap<Integer, SHTrip> res = new HashMap<>();
		try {
			System.out.println("Initializing all trips ...");
			long t1 = System.currentTimeMillis();
			File finFolder = new File("../../../dataset/SH_Taxi_byCar_trips/");
			String[] finNames = guarantee_file_order(finFolder); 
			int fileCnt = 0;
			for (String finName : finNames) {
				BufferedReader fin = new BufferedReader(new FileReader("../../../dataset/SH_Taxi_byCar_trips/" + finName));
				String l = null;
				
				while ((l = fin.readLine()) != null) {
					int cnt = Integer.parseInt(l);
					SHTrip t = new SHTrip(cnt, fin);
					if (ids.contains(t.id))
						res.put(t.id, t);
					
				}
				fin.close();
				
				fileCnt ++;
				if (fileCnt % 200 == 0)
					System.out.println(fileCnt);
			}
			
			long t2 = System.currentTimeMillis();
			System.out.printf("Finished in %d seconds with %d trips.\n", (t2-t1)/1000, res.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}
	
	
	/**
	 * Output the trips in a consistent format with NYC dataset.
	 * 
	 * @param c number of GPS samples
	 * @param br the input file
	 * @param bw the output file
	 * @throws Exception IO exceptions during reading/parsing/writing
	 */
	private void reformatData(BufferedWriter bw) throws Exception {
		SHRecord s = records.getFirst();
		SHRecord e = records.getLast();
		bw.write(String.format("%g,%d,%d,%g,%g,%g,%s,%s,%g,%g,%g,%g,%d\n", s.distanceTo(e),
				s.speed, e.speed, this.L1distance(), duration, distance, 
				SHTrip.sdate, SHTrip.edate, s.longitude, s.latitude, e.longitude, e.latitude, id));
	}
	
	/**
	 * Filter out the outlier trips, and output the trip in another format (consistent with NYC data)
	 * 
	 * The format of output data is given in {@link #reformatData(BufferedWriter)}.
	 */
	public static void generateNYCformattedData() {
		try {
			// get non-outlier trips' ID
			HashSet<Integer> ids = getIDSet("../../../dataset/sh-all-trips-nonoutlier");
			
			BufferedWriter bw = new BufferedWriter(new FileWriter("../../../dataset/SH_taxi_trip"));
			
			System.out.println("Generate NYC-formatted trip data.");
			long t1 = System.currentTimeMillis();
			String folderPath = "../../../dataset/SH_Taxi_byCar_trips/";
			File folder = new File(folderPath);
			String[] finNames = guarantee_file_order(folder);
			int fileCnt = 0;
			for (String s : finNames) {
				BufferedReader br = new BufferedReader(new FileReader(folderPath + s));
				String l = null;
				
				while ( (l = br.readLine()) != null) {
					int cnt = Integer.parseInt(l);
					SHTrip t = new SHTrip(cnt, br, true);
					if (ids.contains(t.id))
						t.reformatData(bw);
				}
				
				br.close();
				
				// progress tracker
				fileCnt ++;
				if (fileCnt % 200 == 0)
					System.out.println(fileCnt);
			}
			
			bw.close();
			long t2 = System.currentTimeMillis();
			System.out.printf("Finished in %d seconds.", (t2-t1)/1000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Retrieve all non-outlier trips.
	 * 
	 * Combine function {@link #getIDSet(String)} and {@link #getAllTrips(HashSet)}.
	 * @return
	 */
	public static HashSet<Integer> getNonOutlierTrips() {
		HashSet<Integer> ids1 = getIDSet("../../../dataset/sh-nonoutlier-logT-logL1");
		HashSet<Integer> ids2 = getIDSet("../../../dataset/sh-nonoutlier-logT-logD");
		HashSet<Integer> ids3 = getIDSet("../../../dataset/sh-nonoutlier-logL1-logD");
		
		
		boolean f1 = ids1.retainAll(ids2);
		
		if (ids1.retainAll(ids3) || f1)		// notice to "or" f1 later, otherwise the short circuit effect kills you
			System.out.println("three sets intersect");
		else
			System.out.println("set 1 contains the other two sets");
		
		
		try (BufferedWriter bw = new BufferedWriter(new FileWriter("../../../dataset/sh-all-trips-nonoutlier"))) {
			for (int id : ids1)
				bw.write(String.format("%d\n", id));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ids1;
	}
	
	
	public static void putNonOutlierTripsIntoDB() {
		SHDB db = new SHDB();
		HashMap<Integer, SHTrip> res = getAllTrips(getIDSet("../../../dataset/sh-all-trips-nonoutlier"));
		long t1 = System.currentTimeMillis();
		System.out.println("Inserting trips into DB.");
		int cnt = 0;
		for (SHTrip t : res.values()) {
			db.insertTrip(t);
			if (cnt++ % 2000 == 0)
				System.out.println(cnt);
		}
		long t2 = System.currentTimeMillis();
		System.out.printf("DB insertion finished in %d seconds", (t2-t1)/1000);
	}

	
	
	public static void main(String[] args) {
//		LinkedList<SHTrip> res = getAllTrips();
//		writeAllTripsForRegressionFilter(res);
//		getNonOutlierTrips();
		generateNYCformattedData();
//		putNonOutlierTripsIntoDB();
	}
	
}
