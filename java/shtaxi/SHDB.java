package shtaxi;


import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Timestamp;


/**
 * Put the Shanghai taxi trips into a database, to ease the visualize process.
 * 
 * @author hxw186
 *
 */
public class SHDB {

	Connection connect;
	PreparedStatement pInsertTrip;
	PreparedStatement pInsertRecord;
	PreparedStatement pAddTripSrcDst;
	
	
	public SHDB() {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			connect = DriverManager.getConnection("jdbc:mysql://localhost/shtaxi?user=shtaxi&password=shtaxi");
			
			// disable auto commit to save time
			connect.setAutoCommit(false);
			
			pInsertTrip = connect.prepareStatement("insert into shtaxi.trip values (?, ?, null, null)");
			pInsertRecord = 
					connect.prepareStatement("insert into shtaxi.record values (default, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
			pAddTripSrcDst = 
					connect.prepareStatement("update shtaxi.trip set pickupRecID=?, dropoffRecID=? where id = ?");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	/**
	 * Insert one trip into the shtaxi.trip table.
	 * 
	 * Table format
	 * +----------------+-----------+-------------------------------+
	 * | id  			| int(11)	| primary key, not null			|
	 * | duration 		| float		|								|
	 * | pickupRecID	| int(11)	|								|
	 * | dropoffRecID	| int(11)	|								|
	 * +----------------+-----------+-------------------------------+
	 * @param t
	 * @return
	 */
	public boolean insertTrip(SHTrip t) {
		try {
			// insert the trip
			PreparedStatement prpstmt = pInsertTrip;
			prpstmt.setInt(1, t.id);
			prpstmt.setFloat(2, t.duration);
			prpstmt.executeUpdate();
			
			// insert the records
			int lastIdx = t.records.size() - 1;
			int pickupRecID = 0;
			int dropoffRecID = 0;
			
			int i = 0;
			for (SHRecord r : t.records) {
				int id = insertRecord(r, t.id);
				if (i == 0)
					pickupRecID = id;
				else if (i == lastIdx)
					dropoffRecID = id;
				i++;
			}
			
			// update the foreign keys on pickup and dropoff records
			prpstmt = pAddTripSrcDst;
			prpstmt.setInt(1, pickupRecID);
			prpstmt.setInt(2, dropoffRecID);
			prpstmt.setInt(3, t.id);
			prpstmt.executeUpdate();
			
			// commit once for one trip
			connect.commit();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return false;
		}
		return true;
	}
	
	
	
	/**
	 * Insert one record into the shtaxi.record table.
	 * 
	 * Table format
	 * +------------+-----------+-------------------------------+
	 * | id  		| int(11)	| primary key auto_increment 	|
	 * | latitude 	| float		|								|
	 * | longitude 	| float		|								|
	 * | time  		| datetime	|								|
	 * | tripid		| int(11)	| foreign key refer trip 		|
	 * +------------+-----------+-------------------------------+
	 * @param r	the record to be inserted
	 * @param tripID the ID of trip that this record belongs to
	 * @return
	 */
	private int insertRecord(SHRecord r, int tripID) {
		int newID = -1;
		try {
			PreparedStatement prpstmt = pInsertRecord;					
			prpstmt.setFloat(1, r.latitude);
			prpstmt.setFloat(2, r.longitude);
			prpstmt.setTimestamp(3, new Timestamp(r.time * 1000));	// Timestamp needs a time in millinseconds
			prpstmt.setInt(4, tripID);
			prpstmt.executeUpdate();
			ResultSet rs = prpstmt.getGeneratedKeys();
			rs.first();
			newID = rs.getInt(1);
			rs.last();
			if (rs.getRow() != 1)
				throw new Exception("Auto generated record ID is wrong");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return newID;
	}
	
	
	
	
	
}
