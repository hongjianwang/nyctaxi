package shtaxi;


import java.io.*;
import java.util.*;

import org.jsoup.*;
import org.jsoup.nodes.*;
import org.jsoup.parser.Parser;

 
/**
 * Baidu map API does not provide travel time estimation conditioned on traffic.
 * @author hxw186
 *
 */
public class BaiduMapQuery {
	static String[] aks;
	static int ak_idx = 0;
	static int query_cnt = 0;

	static String baseurl = "http://api.map.baidu.com/direction/v1";
	
	
	HashMap<String, String> params;
	
	static {
		try {
			BufferedReader br = new BufferedReader(new FileReader("baidu_api_keys"));
			LinkedList<String> ls = new LinkedList<>();
			String l = null;
			while ((l = br.readLine()) != null) {
				ls.add(l);
			}
			aks = ls.toArray(new String[ls.size()]);
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public BaiduMapQuery() {
		params = new HashMap<>();
		params.put("mode", "driving");
		params.put("origin_region", "上海");
		params.put("destination_region", "上海");
		params.put("output", "xml");
		params.put("coord_type", "wgs84");
		params.put("ak", aks[ak_idx]);
	}
	
	/**
	 * Parse the XML result to get distance and duration
	 * @param xml XML document
	 * @return duration in seconds, and distance in kilometers
	 * @throws Exception
	 */
	private double[] parseXMLforDurationDistance(Document xml) throws Exception {
		Element root = xml.select("DirectionDrivingResponse").first();
		Element status = xml.select("status").first();
		if (status.text().equals("5")) {	// key expired
			ak_idx ++;
			params.put("ak", aks[ak_idx]);
			throw new Exception("new key");
		} else if (status.text().equals("2")) {
			throw new Exception("wrong parameter");
		} else if (status.text().equals("401")) {
			throw new Exception("max concurrent connection reached");
		} else if (status.text().equals("0") && root != null) {
			Element tripRes = root.select("result").first().select("routes").first();
			Element duration = tripRes.select("duration").first();
			Element distance = tripRes.select("distance").first();
			return new double[] {Double.parseDouble(duration.text()), Double.parseDouble(distance.text())/1000.0};
		}
		System.out.println(xml.text());
		throw new Exception ("Query failed!");
	}
	
	
	/**
	 * Query one trip from Baidu Map Service
	 * @param source the starting point of the trip
	 * @param destination the ending point of the trip
	 * @return the duration in seconds, and the distance in kilometers
	 */
	public double[] queryTimeDistance(double[] source, double[] destination) {
		double[] res = null;
		params.put("origin", String.format("%g,%g", source[0], source[1]));		// lat, lon
		params.put("destination", String.format("%g,%g", destination[0], destination[1]));
		
		try {
			Connection connc = Jsoup.connect(baseurl).header("connection", "close").
					data(params).timeout(5000).parser(Parser.xmlParser());
			Document docum = connc.get();
			
			
			query_cnt ++;
			if (query_cnt % 100 == 1)
				System.out.printf("Query count : %d\n", query_cnt);
			
			res = parseXMLforDurationDistance(docum);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}
	
	
	
	public static void main(String[] args) {
		BaiduMapQuery bmq = new BaiduMapQuery();
		for (int i = 0; i < 6000; i++) {
			double[] s = bmq.queryTimeDistance(new double[]{31.219651, 121.403968}, new double[]{31.263095, 121.487739});
			System.out.printf("%d time: %g, distance: %g\n", i, s[0], s[1]);
		}
	}
}
