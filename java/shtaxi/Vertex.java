package shtaxi;

import java.io.*;
import java.util.*;

import esri.geometry.Point;


/**
 * Vertex in RoadNetwork
 * @author hxw186
 *
 */
public class Vertex implements Comparable<Vertex> {
	final static float epsilon = 0.0001f;	
	
	public int id;
	public float x;	// longitude
	public float y;	// latitude
	public HashSet<Vertex> neighbors;
	public HashSet<Edge> edges;
	
	public int numGPS;
	
	
	public Vertex() {
		x = 0;
		y = 0;
		id = 0;
		neighbors = new HashSet<Vertex>();
		edges = new HashSet<>();
		numGPS = 0;
	}
	
	
	public Vertex(float a, float b) {
		x = a;
		y = b;
		id = 0;
		neighbors = new HashSet<Vertex>();
		edges = new HashSet<>();
	}
	
	
	public Vertex(Point p, int id) {
		x = p.x;
		y = p.y;
		this.id = id;
		neighbors = new HashSet<Vertex>();
		edges = new HashSet<>();
		numGPS = 0;
	}
	
	
	public void addEdge(Edge d) {
		edges.add(d);
		if (d.v1.equals(this)) {
			neighbors.add(d.v2);
		} else {
			neighbors.add(d.v1);
		}
	}
	
	public void removeEdge(Edge d) {
		edges.remove(d);
		if (d.v1.equals(this))
			neighbors.remove(d.v2);
		else
			neighbors.remove(d.v1);
	}
	
	
	public float distanceTo(Vertex o) {
		float dx = o.x - this.x;
		float dy = o.y - this.y;
		return (float) (Math.sqrt(dx * dx + dy * dy));
	}
	
	
	public float distanceToKM(Vertex o) {
		float dx = (o.x - this.x) * 110;
		float dy = (o.y - this.y) * 110;
		return (float) (Math.sqrt(dx * dx + dy * dy));
	}
	
	
	public boolean vertexOnEdge(Edge e) {
		if (! this.equals(e.v1) && ! this.equals(e.v2) ) {
			float d1 = this.distanceTo(e.v1);
			float d2 = this.distanceTo(e.v2);
			if (d1 + d2 - e.length() < epsilon)
				return true;
			else
				return false;
		} else
			return false;
	}
	
	/**
	 * Save the current vertex into binary file
	 * 
	 * Fields: 
	 * 	ID, 
	 * 	x, 
	 * 	y
	 * @param dos output file
	 * @throws Exception
	 */
	public void writeBinary(DataOutputStream dos) throws Exception {
		dos.writeInt(this.id);
		dos.writeFloat(this.x);
		dos.writeFloat(this.y);
	}
	
	
	public void readBinary(DataInputStream dis) throws Exception {
		this.id = dis.readInt();
		this.x = dis.readFloat();
		this.y = dis.readFloat();
	}


	@Override
	public int compareTo(Vertex o) {
		return this.id - o.id;
	}
	

}
