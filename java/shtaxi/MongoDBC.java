package shtaxi;


import java.util.HashMap;
import java.util.LinkedList;

import com.mongodb.BulkWriteOperation;
import com.mongodb.MongoClient;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.BasicDBObject;


/**
 * Connect to the mongo DB and save the trips in MongoDB.
 * 
 * @author hxw186
 *
 */
public class MongoDBC {

	MongoClient mc;
	DB sht;		// shanghai taxi database
	DBCollection trips;		// we adopt embedded design to put records in each trips
	
	public MongoDBC() {
		try {
			mc = new MongoClient("localhost");
			DB sht = mc.getDB("SHTaxi");
			trips = sht.getCollection("trips");
		} catch (Exception e) {
			System.out.println("Mongo connection failed.");
			e.printStackTrace();
		}
		
	}
	
	
	public static BasicDBObject getTripDBObject(SHTrip t) {
		LinkedList<BasicDBObject> recordsDBdoc = new LinkedList<>();
		for (SHRecord r : t.records) {
			BasicDBObject recDBdoc = new BasicDBObject("time", r.time).append("speed", r.speed)
					.append("latitude", r.latitude).append("longitude", r.longitude);
			recordsDBdoc.add(recDBdoc);
		}
		BasicDBObject doc = new BasicDBObject("id", t.id).append("distance", t.distance)
				.append("duration", t.duration).append("recordsCnt", t.records.size())
				.append("records", recordsDBdoc);
		return doc;
	}
	
	
	public void insertTrip(BasicDBObject t) {
		trips.insert(t);
	}
	
	
//	public LinkedList<SHTrip> retrieveTrip(HashSet<Integer> idset) {
//		
//	}
	
	
	public static void main(String[] args) {
		MongoDBC mdbc = new MongoDBC();
		HashMap<Integer, SHTrip> trips = SHTrip.getAllTrips();
		
//		long t1 = System.currentTimeMillis();
//		for (SHTrip t : trips)
//			mdbc.insertTrip(getTripDBObject(t));
//		long t2 = System.currentTimeMillis();
//		System.out.printf(String.format("Insert finished in %d milliseconds", t2-t1));
		
		long t1 = System.currentTimeMillis();
		BulkWriteOperation bwk = mdbc.trips.initializeUnorderedBulkOperation();
		int cnt = 0;
		for (SHTrip t : trips.values()) {
			bwk.insert(getTripDBObject(t));
			if (cnt % 2000 == 0) {
				bwk.execute();
				// create a new bulk operator
				bwk = mdbc.trips.initializeUnorderedBulkOperation();
				System.out.println(cnt);
			}
			cnt ++;
		}
		bwk.execute();
		long t2 = System.currentTimeMillis();
		System.out.printf(String.format("Insert finished in %d milliseconds", t2-t1));
	}
	
}
