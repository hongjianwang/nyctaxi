package shtaxi;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.LinkedList;

import javax.swing.JFrame;
import javax.swing.JPanel;


enum VisualizeType {EDGE, TRIP, NONE};

public class VisualizeGraph extends JPanel {

	private static final long serialVersionUID = 907569761L;
	
	private LinkedList<Edge> edges;
	private LinkedList<SHTrip> trips;
	private float[] lowerLeft;
	private float[] upperRight;
	private int w, h;
	private float wf, hf;
	private VisualizeType vt;
	private Color TripColor;
	
	
	public VisualizeGraph() {
		edges = null;
		trips = null;
		lowerLeft = new float[]{180, 90};
		upperRight = new float[]{0, 0};
		findBoundingBox();
		vt = VisualizeType.NONE;
		TripColor = Color.BLUE;
	}
	
	
	public void visualizeEdges(LinkedList<Edge> es) {
		vt = VisualizeType.EDGE;
		edges = es;
		findBoundingBox();
	}
	
	
	
	public void visualizeTrips(LinkedList<SHTrip> tps) {
		vt = VisualizeType.TRIP;
		trips = tps;
		findBoundingBox();
	}
	
	
	/**
	 * Find the bounding box of the plot, which depends on the objects to visualize.
	 * 
	 * The vt with different VisualizeType will determine the functionality.
	 */
	public void findBoundingBox( ){
		if (vt == VisualizeType.EDGE) {
			for (Edge e : edges) {
				findBoundingBox(e);
			}
		} else if (vt == VisualizeType.TRIP) {
			for (SHTrip t : trips) {
				for (Edge e : t.edges)
					findBoundingBox(e);
				for (SHRecord r : t.records)
					findBoundingBox(r);
			}
		}
	}
	
	/**
	 * Update the bounding box by one Edge.
	 * @param e
	 */
	private void findBoundingBox(Edge e) {
		for (Vertex r : new Vertex[]{e.v1, e.v2}) {
			if (r.x < lowerLeft[0]) {
				lowerLeft[0] = r.x;
			} else if (r.x > upperRight[0]) {
				upperRight[0] = r.x;
			}
			if (r.y > upperRight[1]) {
				upperRight[1] = r.y;
			} else if (r.y < lowerLeft[1]) {
				lowerLeft[1] = r.y;
			}
		}
	}
	
	/**
	 * Update the bounding box by one Record.
	 * @param r
	 */
	private void findBoundingBox(SHRecord r) {
		if (r.longitude < lowerLeft[0])
			lowerLeft[0] = r.longitude;
		else if (r.longitude > upperRight[0])
			upperRight[0] = r.longitude;
		
		if (r.latitude > upperRight[1])
			upperRight[1] = r.latitude;
		else if (r.latitude < lowerLeft[1])
			lowerLeft[1] = r.latitude;
	}
	
	
	/**
	 * Automatically plot trips / road networks 
	 */
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		System.out.println("Start painting...");
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		w = getWidth();
		h = getHeight();
		wf = upperRight[0] - lowerLeft[0];
		hf = upperRight[1] - lowerLeft[1];
		
		// plot Edge
		if (vt == VisualizeType.EDGE) {
			for (Edge e : edges)
				plot(g2, e, Color.BLACK);
		} else if (vt == VisualizeType.TRIP) {
			int i = 0;
			for (SHTrip t : trips) {
				if (i == 0)
					TripColor = Color.GREEN;
				else
					TripColor = Color.BLUE;
				i++;
				
				plot(g2, t);
				for (Edge e : t.edges)
					plot(g2, e, Color.BLACK);
			}
		}
		
		System.out.println("Paint finished.");
		System.out.printf("%g, %g\n%g, %g\n", lowerLeft[0], lowerLeft[1], upperRight[0], upperRight[1]);
	}
	
	/**
	 * Plot one edge on the map.
	 * @param g
	 * @param e
	 */
	private void plot(Graphics2D g, Edge e, Color linkColor, Color vertexColor) {
		int[] res1 = coordinatesConversion(e.v1);
		int[] res2 = coordinatesConversion(e.v2);
		g.setPaint(linkColor);
		g.drawLine(res1[0], res1[1], res2[0], res2[1]);
		g.setPaint(vertexColor);
		g.drawOval(res1[0], res1[1], 2, 2);
		g.drawOval(res2[0], res2[1], 2, 2);
	}
	
	private void plot(Graphics2D g, Edge e, Color linkColor) {
		plot(g, e, linkColor, Color.RED);
	}
	
	/**
	 * Plot one GPS sample on the map.
	 * @param g
	 * @param r
	 */
	private int[] plot(Graphics2D g, SHRecord r) {
		int[] res = coordinatesConversion(new Vertex(r.longitude, r.latitude));
		g.setPaint(TripColor);
		g.drawOval(res[0] - 2, res[1] - 2, 4, 4);
		return res;
	}
	
	/**
	 * Plot one trip on the map.
	 * @param g
	 * @param t
	 */
	private void plot(Graphics2D g, SHTrip t) {
		SHRecord prev = null;
		for (SHRecord r : t.records) {
			int[] coords = plot(g, r);
			// plot the GPS transition link
			if (prev != null) {
				Vertex s = new Vertex(prev.longitude, prev.latitude);
				Vertex e = new Vertex(r.longitude, r.latitude);
				plot(g, new Edge(s, e), TripColor, TripColor);
			} else {	// on the first point
				g.drawString(String.format("tripID: %d", t.id), coords[0], coords[1]);
			}
			prev = r;
		}
	}
	
	private int[] coordinatesConversion(Vertex v) {
		int x = (int) ((v.x - lowerLeft[0]) / wf * w);
		int y = (int) ((upperRight[1] - v.y) / hf * h);
		return new int[]{x, y};
	}
	
	
	
	
	public static void plotRoadNetwork(LinkedList<Edge> es) {
		JFrame f = new JFrame();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		VisualizeGraph g = new VisualizeGraph();
		g.visualizeEdges(es);
		f.add(g);
		f.setSize((int)(753*1.5), (int)(578*1.5));
		f.setVisible(true);
	}
	
	
	
	public static void plotTripCaseStudy(LinkedList<SHTrip> ts) {
		JFrame f = new JFrame();
		f.setSize(740* 2, 578* 2);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setTitle(String.format("Trip ID: %d with %d neighbors", ts.getFirst().id, ts.size()-1));
		
		VisualizeGraph g = new VisualizeGraph();
		g.visualizeTrips(ts);
		f.add(g);
		f.setSize(1200,800);
		f.setVisible(true);
	}
}
