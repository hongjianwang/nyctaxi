package shtaxi;


import java.util.*;

/**
 * Assistant class used for the sub-trajectory based travel time estimator.
 * 
 * This class implement the method proposed in Wang, Zheng, Xue et al. (KDD'14).
 * 
 * It is a dynamic programming procedure to find the optimal partition of a Trip.
 * @author hxw186
 *
 */
public class Subpath {
	
	ArrayList<Edge> edges;		// all edges consist of this sub-path
	LinkedList<SHTrip> trips;	// trip traversing this sub-path
	double time;
	
	public Subpath(LinkedList<Edge> edges) {
		this.edges = new ArrayList<>();
		this.edges.addAll(edges);
		time = 0;
	}
	
	
	
	/**
	 * Extract all trips traverse the current sub-path
	 * @return the ID list of found trips
	 */
	private LinkedList<SHTrip> extractTrips() {
		trips = new LinkedList<>();
		int c = 0;
		for (Edge e : edges) {
			if (c == 0)
				trips.addAll(e.trips);
			else
				trips.retainAll(e.trips);
			c++;
		}
		return trips;
	}
	
	/**
	 * Calculate the travel time on current sub-path for given trip.
	 * @param t given trip
	 * @return the travel time on current sub-path
	 * @throws Exception 
	 */
	private double getTravelTime(SHTrip t) throws Exception {
		double time = 0;
		boolean MATCH = false;
		int i = 0;
		for (int j = 0; j < t.edges.size(); j++) {
			Edge e = t.edges.get(j);
			if (e == edges.get(i)) {
				MATCH = true;
				time += t.travelTime.get(j);
				i++;
				if (i == edges.size())
					break;
			} else {
				if (MATCH == true) {
					throw new Exception("Reverse sub-path.");	
				}
			}
		}
		return time;
	}
	
	
	/**
	 * Calculate the proposed measure for current sub-path.
	 * 
	 * The measure is :
	 * 		1/n_p * Variance(t_p_j)
	 * @return
	 */
	public double calculateVarianceMeasure() {
		// initialize the trips list first
		this.extractTrips();
		
		LinkedList<Double> times = new LinkedList<>();
		double sum = 0, travelT = 0;
		for (SHTrip t : trips) {
			try {	// if reverse order trips are found, we skip it
				travelT = getTravelTime(t);
			} catch (Exception e) {
				//System.out.println(e.getMessage());
				continue;
			}
			sum += travelT;
			times.add(travelT);
		}
		// calculate variance
		time = sum / times.size();
		sum = 0;
		for (double d : times) {
			sum += (time - d) * (time - d);
		}
		return sum / times.size();
	}

}
