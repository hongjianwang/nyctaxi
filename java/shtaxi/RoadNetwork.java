package shtaxi;

import esri.shp.*;
import esri.dbf.*;
import esri.geometry.*;
import gnu.trove.procedure.TIntProcedure;
import jsi.rtree.*;
import jsi.*;

import java.io.*;
import java.util.*;


public class RoadNetwork {
	static final String shpFile = "../../../dataset/sh_map/highway_line.shp";
	static int EXISTid = -1;
	
	RTree si;
	ArrayList<Vertex> vertices;
	LinkedList<Edge> edges;
	
	
	/**
	 * Generate the roadnetwork from the Shapefile.
	 * 
	 * Since the input data is not edge graph, I need to map some vertices to edges on my own.
	 */
	public RoadNetwork() {
		si = new RTree();
		si.init(null);
		vertices = new ArrayList<>();
		edges = new LinkedList<>();
		int totalPoints = 0, streetCnt = 0;
		
		try {
			FileInputStream fin = new FileInputStream("../../../dataset/sh_map/highway_line.shp");
			ShpReader shpr = new ShpReader(new DataInputStream(new BufferedInputStream(fin)));
			
			Polyline pl = new Polyline();
			
			while(shpr.hasMore()) {
				shpr.queryPolylineZ(pl);
				totalPoints += pl.numPoints;
				streetCnt ++;
				
				// get all streets (vertices and links) from shapefile
				for (LinkedList<Point> paths : pl.paths) {
					Vertex prevV = null;
					for (final Point p : paths) {
						EXISTid = -1;
//						System.out.printf("%f\t%f\n", p.x, p.y);
						si.nearest(new FPoint(p.x, p.y), new TIntProcedure() {
							public boolean execute(int i) {
								Vertex v = vertices.get(i);
								if (v.x == p.x && v.y == p.y)
									EXISTid = i;
								return true;
							}
						}, 0.01f);
						Vertex cur = null;
						// if the current vertex does not exist, we create and add it to the spatial index
						if (EXISTid == -1) {
							final int c = vertices.size();
							si.add(new Rectangle(p.x, p.y, p.x, p.y), c);
							cur = new Vertex(p, c);
							vertices.add(cur);
						} else {
							cur = vertices.get(EXISTid);
						}
						
						// add new edge
						if (prevV != null) {
							// due to the unreliable precision of float, two points (very close) could be mis-judged as one
							if (prevV.id == cur.id)
								System.err.printf("Wrong Edge %d %d\n", prevV.id, cur.id);
							else {
								Edge ne = new Edge(prevV, cur);
								edges.add(ne);
							}
						}
						prevV = cur;				
					}
				}
			}
			
			fin.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.printf("# street: %d\n", streetCnt);
		System.out.printf("# points: %d\n", totalPoints);
		System.out.printf("# vertices: %d\n", vertices.size());
		System.out.printf("# segments: %d\n", edges.size());
		numConnectedComponent();
		
		
		System.out.println("Match vertices on edges.");
		for (Vertex v : vertices) {
			for (Edge e : edges) {
				if (v.vertexOnEdge(e)) {
					e.addPassingVertex(v, v.distanceTo(e.v1));
				}
			}
			if (v.id % 5000 == 0)
				System.out.println(v.id);
		}
		
		System.out.println("Update all edges and vertices.");
		LinkedList<Edge> newEdges = new LinkedList<>();
		for (Edge e : edges) {
			LinkedList<Edge> es = e.updateEdge();
			if (!es.isEmpty()) {
				newEdges.addAll(es);
			} else
				newEdges.add(e);
		}
		edges.clear();
		edges = newEdges;
		
		System.out.println("RoadNetwork successfully built.");
		System.out.printf("# street: %d\n", streetCnt);
		System.out.printf("# points: %d\n", totalPoints);
		System.out.printf("# vertices: %d\n", vertices.size());
		System.out.printf("# segments: %d\n", edges.size());
		numConnectedComponent();
	}
	
	
	/**
	 * Initialize the road network from binary file
	 * @param fromFile
	 */
	public RoadNetwork(boolean fromFile) {
		if (fromFile) {
			si = new RTree();
			si.init(null);
			vertices = new ArrayList<>();
			edges = new LinkedList<>();
			
			this.readRoadNetwork();
			
			this.readEdgeProperties();
			
			// build index
			for (Vertex v : vertices) {
				final int vid = v.id;
				si.add(new Rectangle(v.x, v.y, v.x, v.y), vid);
			}
			
			System.out.println("RoadNetwork successfully built from binary file.");
			System.out.printf("# vertices: %d\n", vertices.size());
			System.out.printf("# segments: %d\n", edges.size());
			System.out.printf("# RTree size: %d\n", si.size());
		}
	}
	
	/**
	 * Write the graph as a binary file.
	 */
	public void writeRoadNetwork() {
		try (DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("../../../dataset/sh_map/road.map"))) ) {
			// write number of vertices
			dos.writeInt(vertices.size());
			for (Vertex v : vertices)
				v.writeBinary(dos);
			
			// write number of edges
			dos.writeInt(edges.size());
			for (Edge e : edges)
				e.writeBinary(dos);
			
			System.err.println(Edge.wrongEdge);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Initialize the roadnetwork from binary file (edge graph)
	 */
	private void readRoadNetwork() {
		try (DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream("../../../dataset/sh_map/road.map"))) ) {
			vertices.clear();
			edges.clear();
			// read in all vertices
			int cnt = dis.readInt();
			for (int i = 0; i < cnt; i++) {
				Vertex v = new Vertex();
				v.readBinary(dis);
				vertices.add(v);
			}
			
			// read in all edges
			cnt = dis.readInt();
			for (int i = 0; i < cnt; i++) {
				Edge e = new Edge();
				e.readBinary(vertices, dis);
				edges.add(e);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Match the GPS record to a specific road segment.
	 * @param r the GPS record to be matched
	 * @return matched road segment
	 */
	private Edge matchRoadSegment(SHRecord r) {
		final LinkedList<Vertex> res = new LinkedList<Vertex>();
		si.nearestN(new FPoint(r.longitude, r.latitude), new TIntProcedure() {
			public boolean execute(int i) {
				res.add(vertices.get(i));
				return true;
			}
		}, 3, 0.01f);
		if (res.isEmpty())
			return null;
		else {
			float minD = Float.MAX_VALUE;
			Edge minE = null;
			int cntEdge = 0;
			for (Vertex v1 : res) {
				for (Edge e : v1.edges) {
					cntEdge ++;
					float d = e.distancePointToEdge(r);
					if (d < minD) {
						minD = d;
						minE = e;
					}
				}
			}
			if (minE == null)
				System.out.println(cntEdge);
			else {
				minE.numGPS ++;
				minE.speedSum += r.speed;
			}
			return minE;
		}
	}
	
	
	/**
	 * Map each record of given trip onto one road segment.
	 * @param trp
	 */
	private void mapRecordToEdge(SHTrip trp) {
		ArrayList<Edge> es = new ArrayList<>();
		ArrayList<Double> travelTimes = new ArrayList<>();
		for (SHRecord r : trp.records) {
			Edge e = this.matchRoadSegment(r);
			if (e != null && !es.contains(e)) {
				es.add(e);
				e.trips.add(trp);
				travelTimes.add( (double) (e.lengthKM() / r.speed * 3600.0) );
			}
		}
		trp.edges = es;
		trp.travelTime = travelTimes;
	}
	
	
	private int numConnectedComponent() {
		boolean[] visited = new boolean[si.size()];
		// pre-order search -- number of connected components
		LinkedList<Vertex> queue = new LinkedList<>();
		int numCC = 0;
		for (int i = 0; i < vertices.size(); i++) {
			if (visited[i] == false) {
				queue.add(vertices.get(i));
				visited[i] = true;
				numCC++;
				
				while (!queue.isEmpty()) {
					Vertex cur = queue.removeFirst();
					for (Vertex vn : cur.neighbors) {
						if (vn.id == cur.id)
							System.err.printf("edge %d %d\n", cur.id, vn.id);
						if (! visited[vn.id] ) {
							queue.add(vn);
							visited[vn.id] = true;
						}
					}
				}
			}
		}
		System.out.printf("# Connected components %d\n", numCC);
		return numCC;
	}
	
	
	/**
	 * Test the Shapefile parser
	 * @deprecated the reader class works fine
	 */
	public static void test_esri_reader() {
		try {
			FileInputStream fin = new FileInputStream("../../../dataset/sh_map/highway_line.shp");
			ShpReader shpr = new ShpReader(new DataInputStream(new BufferedInputStream(fin)));
			Polyline pl = new Polyline();
			while (shpr.hasMore()) {
				shpr.queryPolylineZ(pl);
				System.out.printf("NumParts: %d\tNumPoints %d\tPolyline %d - %d\n", shpr.numParts, shpr.numPoints, pl.paths.size(), pl.numPoints);
			}
			fin.close();
			
			FileInputStream fin2 = new FileInputStream("../../../dataset/sh_map/highway_line.dbf");
			DBFReader dbf = new DBFReader(new DataInputStream(new BufferedInputStream(fin2)));
			Map<String, Object> map = new HashMap<>();
			while (dbf.readRecordAsMap(map) != null) {
				System.out.println(map);
			}
			fin2.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Generate the binary file of graph
	 * @deprecated the file is generated already, no need to run this again.
	 */
	public static void buildBinaryRoadNetworkFile() {
		long t1 = System.currentTimeMillis();
		RoadNetwork rn = new RoadNetwork();
		long t2 = System.currentTimeMillis();
		System.out.printf("Time elapsed %d seconds", (t2-t1)/1000);
		rn.writeRoadNetwork();
		long t3 = System.currentTimeMillis();
		System.out.printf("Write binary file in %d seconds", (t3-t2)/1000);
	}
	
	
	
	/**
	 * Use raw GPS data calculate the number of GPS on each road segment.
	 * @deprecated run this once to get intermediate file
	 */
	public static void calculateEdgeTravelTime() {
		final RoadNetwork rn = new RoadNetwork(true);
		try {
			File finFolder = new File("../../../dataset/SH_Taxi_byCar/");
			
			long t1 = System.currentTimeMillis();
			
			int totalGPS = 0;
			for (String fin: finFolder.list()) {
				BufferedReader br = new BufferedReader(new FileReader("../../../dataset/SH_Taxi_byCar/" + fin));
				String l = null;
				
				while ((l = br.readLine()) != null) {
					totalGPS ++;
					SHRecord record = new SHRecord(l);
					rn.matchRoadSegment(record);
				}
				
				br.close();
			}
			
			long t2 = System.currentTimeMillis();
			System.out.printf("Process %d trips in %d seconds.\n", totalGPS, (t2-t1)/1000);

			int cntZero = 0;
			for (Vertex p : rn.vertices)
				if (p.numGPS == 0)
					cntZero ++;
			System.out.printf("Nocover vertices: %d\n", cntZero);
			
			cntZero = 0;
			BufferedWriter bw = new BufferedWriter(new FileWriter("edge-cover-stat"));
			for (Edge e : rn.edges) {
				bw.write(String.format("%d,%g\n", e.numGPS, e.speedSum));
				if (e.numGPS == 0)
					cntZero ++;					
			}
			bw.close();
			System.out.printf("Nocover edges: %d\n", cntZero);
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	/**
	 * Read in the edge travel time and number of GPS records
	 */
	private void readEdgeProperties() {
		try (BufferedReader fin = new BufferedReader(new FileReader("edge-cover-stat")) ) {
			for (Edge e : edges) {
				String[] l = fin.readLine().split(",");
				e.numGPS = Integer.parseInt(l[0]);
				e.speedSum = Double.parseDouble(l[1]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Initiate a given set of trips. Map the trip records on road network edges.
	 * @param rn
	 * @return
	 */
	private static HashMap<Integer, SHTrip> initiateTripAndMapEdge(RoadNetwork rn, HashSet<Integer> idset) {
		HashMap<Integer, SHTrip> alltrips;
		long t1 = System.currentTimeMillis();
		if (idset != null)
			alltrips = SHTrip.getAllTrips(idset);
		else
			alltrips = SHTrip.getAllTrips();
		long t2 = System.currentTimeMillis();
		
		// map edges with trips
		System.out.println("Start mapping edges.");
		int c = 0;
		for (SHTrip t : alltrips.values()) {
			rn.mapRecordToEdge(t);
			if (c++ % 50000 == 0)
				System.out.println(c);
		}
		long t3 = System.currentTimeMillis();
		System.out.printf("In total, %d edges are mapped.\nRetrieve traing set %d seconds, map to edge %d seconds, total %d seconds.\n",
				alltrips.size(), (t2-t1)/1000, (t3-t2)/1000, (t3-t1)/1000);
		
		return alltrips;
	}
	
	
	/**
	 * Initiate all non-outlier trips.
	 * @param rn
	 * @return
	 */
	private static HashMap<Integer, SHTrip> initiateTripAndMapEdge(RoadNetwork rn) {
		HashSet<Integer> ids = SHTrip.getIDSet("../../../dataset/sh-all-trips-nonoutlier");
		return initiateTripAndMapEdge(rn, ids);
	}
	
	
	
//	private static LinkedList<SHTrip> initialTripFromDBAndMapEdge(RoadNetwork rn, HashSet<Integer> idset) {
//		LinkedList<SHTrip> alltrips;
//		
//		return alltrips;
//	}
	
	
	/**
	 * Evaluate the segment-based trip travel time estimator
	 */
	public static void evaluate() {
		final RoadNetwork rn = new RoadNetwork(true);
//		HashSet<Integer> testids = SHTrip.getIDSet("../../../dataset/sh-all-trips-nonoutlier");
		LinkedList<Integer> testids = SHTrip.getIDList("../../../dataset/SH_taxi_eval-sampled");
		HashMap<Integer, SHTrip> alltrips = initiateTripAndMapEdge(rn);
		
		try {
			long t1 = System.currentTimeMillis();
			int totalTrip = 0, continuousTrip = 0, predictableTrip = 0;
			BufferedWriter bw = new BufferedWriter(new FileWriter("trip-cover-rate"));
			BufferedWriter bw2 = new BufferedWriter(new FileWriter("prediction"));
			
//			LinkedList<Edge> tobePlot = new LinkedList<>();	
			
			System.out.println(testids.size());
			for (int id : testids) {
				if (alltrips.containsKey(id)) {
					SHTrip t = alltrips.get(id);
					double tcv = t.segmentCoverage();
					totalTrip ++;
					bw.write(String.format("%g\n", tcv));
					if (tcv > 0)
						continuousTrip ++;
					if (tcv == 1.0) {
						double t_segment = t.segment_based_estimation();
						double t_subpath = t.subpath_based_estimation();
						if (t_segment > 0 && t_subpath > 0)
						{
							bw2.write(String.format("%g\t%g\t%g\t%d\n", t.duration, t_segment,
								t_subpath, t.id));
							predictableTrip ++;
						}
//						else
//							System.out.printf("Trip id %d\t gnd %g\tsegment %g\tsubpath %g\n", t.id, t.duration, t_segment, t_subpath);
	//					if (t.edges != null)
	//						tobePlot.addAll(t.edges);
					}
					
					if (totalTrip % 50000 == 0)
						System.out.println(totalTrip);
				}
			}
			
			System.out.printf("# trips %d\n# connected trips %d\n# predictable trips %d\n", totalTrip, continuousTrip, predictableTrip);
			long t2 = System.currentTimeMillis();
			System.out.printf("Estimation finished in %d seconds", (t2-t1)/1000);
			bw.close();
			bw2.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	
	public static void visualizeCases() {
		LinkedList<LinkedList<Integer>> caseTrip = new LinkedList<>();
		HashSet<Integer> tripIDset = new HashSet<>();
		
		try (BufferedReader br = new BufferedReader(new FileReader("cases-neighbor-list"))) {
			String l = null;
			while ((l = br.readLine()) != null) {
				LinkedList<Integer> tps = new LinkedList<>();
				String[] ls = l.split("\t");
				for (String lsid : ls) {
					int id = Integer.parseInt(lsid);
					tps.add(id);
					tripIDset.add(id);
				}
				
				caseTrip.add(tps);				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// plot trip t_i
		getTvalue(tripIDset);

		final RoadNetwork rn = new RoadNetwork(true);
		HashMap<Integer, SHTrip> alltrips = initiateTripAndMapEdge(rn, tripIDset);
		
		HashMap<Integer, SHTrip> tripMap = new HashMap<>();
		for (SHTrip t : alltrips.values())
			tripMap.put(t.id, t);
		
		// visualize in java
//		for (LinkedList<Integer> caseT : caseTrip) {
//			LinkedList<SHTrip> trps = new LinkedList<>();
//			for (int id : caseT) {
//				trps.add(tripMap.get(id));
//			}
//			VisualizeGraph.plotTripCaseStudy(trps);
//		}
		
		// visualize in JSON object
		try (BufferedWriter bw = new BufferedWriter(new FileWriter("../visualize/cases.js"))) {
			bw.write("var cases = [");	// JSON starts
			int caseCnt = 0;
			for (LinkedList<Integer> caseT : caseTrip) {
				if (caseCnt == 0)
					bw.write("[");	// case starts
				else
					bw.write(",[");	// case starts
				caseCnt ++;
				
				int tpCnt = 0;
				for (int id : caseT) {
					if (tpCnt == 0)
						bw.write("[");	// trip starts
					else
						bw.write(",[");	// trip starts
					tpCnt ++;
					
					SHTrip tp = tripMap.get(id);
					int i = 0;
					for (SHRecord r : tp.records) {
						if (i == 0)
							bw.write(String.format("[%g, %g]", r.latitude, r.longitude));
						else
							bw.write(String.format(",[%g, %g]", r.latitude, r.longitude));
						i++;
					}
					bw.write("]");	// trip ends
				}
				bw.write("]");	// case ends
			}
			bw.write("];");	// JSON ends
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void getTvalue(HashSet<Integer> tripIDset) {
		HashMap<Integer, double[]> ti = new HashMap<>();
		try (BufferedReader br1 = new BufferedReader(new FileReader("../../../dataset/sh-nonoutlier-logL1-logD"));
				BufferedReader br2 = new BufferedReader(new FileReader("../../../dataset/sh-nonoutlier-logT-logD"));
				BufferedReader br3 = new BufferedReader(new FileReader("../../../dataset/sh-nonoutlier-logT-logL1"))) {
			String l = null;
			while ((l = br1.readLine()) != null) {
				String[] ls = l.split(",");
				int id = Integer.parseInt(ls[4]);
				if (tripIDset.contains(id)) {
					ti.put(id, new double[3]);
					ti.get(id)[0] = Double.parseDouble(ls[0]);
				}
			}
			
			while ((l = br2.readLine()) != null) {
				String[] ls = l.split(",");
				int id = Integer.parseInt(ls[4]);
				if (tripIDset.contains(id)) {
					if (ti.containsKey(id))
						ti.get(id)[1] = Double.parseDouble(ls[0]);
				}
			}
			
			while ((l = br3.readLine()) != null) {
				String[] ls = l.split(",");
				int id = Integer.parseInt(ls[4]);
				if (tripIDset.contains(id)) {
					if (ti.containsKey(id))
						ti.get(id)[2] = Double.parseDouble(ls[0]);
				}
			}
			
			for (int i : ti.keySet()) {
				System.out.printf(String.format("%d\t%g\t%g\t%g\n", i, ti.get(i)[0], ti.get(i)[1], ti.get(i)[2]));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void visualizeRoadNetwork() {
		final RoadNetwork rn = new RoadNetwork(true);
		LinkedList<Edge> plot_edges = new LinkedList<>();
		
		try (BufferedWriter bw = new BufferedWriter(new FileWriter("../../../dataset/SH_gps"))) {
			File finFolder = new File("../../../dataset/SH_Taxi_byCar/");
			for (String fname : finFolder.list()) {
				BufferedReader fin = new BufferedReader(new FileReader("../../../dataset/SH_Taxi_byCar/" + fname));
				String l = null;
				
				while ((l = fin.readLine()) != null) {
					SHRecord r = new SHRecord(l);
					bw.write(String.format("%g,%g\n", r.longitude, r.latitude));
					rn.matchRoadSegment(r);
				}
				fin.close();
			}
			
			BufferedWriter fout = new BufferedWriter(new FileWriter("edge-sample"));
			for (Edge e : rn.edges) {
				fout.write(String.format("%d\n", e.numGPS));
				if (e.numGPS > 0)
					plot_edges.add(e);
			}
			fout.close();
			VisualizeGraph.plotRoadNetwork(rn.edges);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] argv) {
//		buildBinaryRoadNetworkFile();
//		calculateEdgeTravelTime();
		evaluate();
//		visualizeCases();
//		RoadNetwork rn = new RoadNetwork(true);
//		VisualizeGraph.plotRoadNetwork(rn.edges);
	}
}
