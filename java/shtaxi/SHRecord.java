package shtaxi;

import java.text.SimpleDateFormat;
import java.util.Date;

public class SHRecord {

	static final String timeFormat = "yyyy-MM-dd HH:mm:ss";
	static final SimpleDateFormat sdf = new SimpleDateFormat(timeFormat);
	
	
	float longitude;
	float latitude;
	short speed;
	long time;
	
	
	public SHRecord(String l) {
		String[] ls = l.split(",");
		longitude = Float.parseFloat(ls[0]);
		latitude = Float.parseFloat(ls[1]);
		speed = Short.parseShort(ls[2]);
		if (speed < 5)
			speed = 5;
		try {
			Date d = sdf.parse(ls[4]);
			time = d.getTime() / 1000;	// in seconds
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(l);
		}
	}
	
	
	public SHRecord(double[] coords) {
		longitude = (float) coords[1];
		latitude = (float) coords[0];
	}
	
	
	public float distanceTo(Vertex o) {
		float dx = o.x - this.longitude;
		float dy = o.y - this.latitude;
		return (float) (Math.sqrt(dx * dx + dy * dy));
	}
	
	
	/**
	 * Return the actual distance in unit (km)
	 * @param o the other record
	 * @return distance in km
	 */
	public float distanceTo(SHRecord o) {
		float d_lon = (longitude - o.longitude) * 110;
		float d_lat = (latitude - o.latitude) * 110;
		return (float) (Math.sqrt(d_lon * d_lon + d_lat * d_lat));
	}
	
	
}
