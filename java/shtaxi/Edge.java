package shtaxi;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.*;

import esri.geometry.Point;


public class Edge {
	
	public static int wrongEdge = 0;
	
	Vertex v1;
	Vertex v2;
	
	LinkedList<Vertex> passingVertices;
	LinkedList<Float> distanceToS;
	
	public int numGPS;
	public double speedSum;
	
	public LinkedList<SHTrip> trips;
	
	public Edge() {
		v1 = null;
		v2 = null;
		numGPS = 0;
		trips = new LinkedList<>();
	}
	
	public Edge(Vertex s, Vertex e) {
		v1 = s;
		v2 = e;
		passingVertices = new LinkedList<Vertex>();
		distanceToS = new LinkedList<Float>();
		s.addEdge(this);
		e.addEdge(this);
		trips = new LinkedList<>();
	}
	
	/**
	 * add passing vertex to current edge
	 * @param p vertex
	 * @param dist the distance from vertex p to the first end point of current edge
	 */
	public void addPassingVertex(Vertex p, float dist) {
		passingVertices.add(p);
		distanceToS.add(dist);
	}
	
	public float length() {
		return v1.distanceTo(v2);
	}
	
	public float lengthKM() {
		return v1.distanceToKM(v2);
	}
	
	
	
	/**
	 * Split the current edge
	 */
	public LinkedList<Edge> updateEdge() {
		LinkedList<Edge> res = new LinkedList<>();
		if (!distanceToS.isEmpty()) {
			// sort and get the index
			int[] idx = new int[distanceToS.size()];
			float[] main = new float[distanceToS.size()];
			for (int i = 0; i < distanceToS.size(); i++) {
				main[i] = distanceToS.get(i);
				idx[i] = i;
			}
			quicksort(main, idx);
			Vertex prev = v1;
			prev.removeEdge(this);
			Vertex e = v2;
			e.removeEdge(this);
			for (int i = 0; i < idx.length; i++) {
				Vertex cur = passingVertices.get(idx[i]);
				Edge ne = new Edge(prev, cur);
				res.add(ne);
				if (i == idx.length - 1) {
					ne = new Edge(cur, e);
					res.add(ne);
				}
				prev = cur;
			}
		}
		return res;
	}
	
	
	//// =========  helper function quicksort family ================== 
	private void quicksort(float[] main, int[] idx) {
		quicksort(main, idx, 0, idx.length - 1);
	}
	
	private void quicksort(float[] a, int[] idx, int left, int right) {
		if (right <= left)
			return;
		int i = partition(a, idx, left, right);
		quicksort(a, idx, left, i-1);
		quicksort(a, idx, i, right);
	}
	
	private int partition(float[] a , int[] idx, int left, int right) {
		int i = left - 1;
		int j = right;
		while (true) {
			while (i < right && a[++i] <= a[right])
				;
			while (a[right] < a[--j])
				if (j == left)
					break;
			if (i >= j)
				break;
			swap(a, idx, i, j);
		}
		swap(a, idx, i, right);
		return i;
	}
	
	private void swap(float[] a, int[] idx, int i, int j) {
		float s = a[i];
		a[i] = a[j];
		a[j] = s;
		int k = idx[i];
		idx[i] = idx[j];
		idx[j] = k;
	}
	
	//// =========  helper function quicksort family ends ==================
	
	
	
	/**
	 * Write the current edge to a binary file.
	 * 
	 * @param dos
	 * @throws Exception
	 */
	public void writeBinary(DataOutputStream dos) throws Exception {
		dos.writeInt(v1.id);
		dos.writeInt(v2.id);

		if (v1.id == v2.id) {
//			System.err.printf("Wrong Edge %d %d\n", v1.id, v2.id);
			wrongEdge++;
		}
	}
	
	
	public void readBinary(ArrayList<Vertex> vertics, DataInputStream dis) throws Exception {
		int s = dis.readInt();
		int e = dis.readInt();
		v1 = vertics.get(s);
		v2 = vertics.get(e);
		v1.addEdge(this);
		v2.addEdge(this);
	}
	
	
	
	/**
	 * Minimum distance from a point to the current line segment
	 * @param r the point represented by a GPS record
	 * @return the distance between r and current line segment, which is represented by edge
	 */
	public float distancePointToEdge(SHRecord r) {
		// square of length
		float l2 = (v1.x - v2.x) * (v1.x - v2.x) + (v1.y - v2.y) * (v1.y - v2.y);
		// dot( p - s, e - s) / l2
		float t = ((r.longitude - v1.x) * (v2.x - v1.x) + (r.latitude - v1.y) * (v2.y - v1.y)) / l2;
		if (t < 0.0f)
			return r.distanceTo(v1);
		else if (t > 1.0f)
			return r.distanceTo(v2);
		float x = v1.x + t * (v2.x - v1.x);
		float y = v1.y + t * (v2.y - v1.y);
		return r.distanceTo(new Vertex(x, y));		
	}
	
	
	
	public boolean connectedToEdge(Edge o) {
		return this.v1.edges.contains(o) || this.v2.edges.contains(o);
	}
	
	
	public double travelTime() {
		double speed = speedSum / numGPS;
		if (speed != 0) {
			return lengthKM() / speed * 3600;
		} else {
			return -1;
		}
	}
	
	
	
	public static void test_quicksort() {
		Edge e = new Edge();
		float[] main = new float[] { 3.1f, 1.3f, 5.3f, 3.9f, 5.3f};
		int[] idx = new int[] {0, 1, 2, 3, 4 };
		e.quicksort(main, idx);
		for (int i = 0; i < idx.length; i++)
			System.out.printf("%f, %d\n", main[i], idx[i]);
	}
	
	
	public static void test_addPassingVertex() {
		LinkedList<Edge> edges = new LinkedList<>();
		LinkedList<Vertex> vertices = new LinkedList<>();
		Vertex v1 = new Vertex(new Point(1, 1), 0);
		Vertex v2 = new Vertex(new Point(3, 3), 1);
		Vertex v3 = new Vertex(new Point(2, 2), 2);
		Vertex v4 = new Vertex(new Point(2, 1), 3);
		vertices.add(v1);
		vertices.add(v2);
		vertices.add(v3);
		vertices.add(v4);
		edges.add(new Edge(v1, v2));
		edges.add(new Edge(v3, v4));

		System.out.printf("# segments: %d\n", edges.size());
		
		System.out.println("Match vertices on edges.");
		for (Vertex v : vertices) {
			for (Edge e : edges) {
				if (v.vertexOnEdge(e)) {
					System.out.printf("%d is on %d-%d\n", v.id, e.v1.id, e.v2.id);
					e.addPassingVertex(v, v.distanceTo(e.v1));
				}
			}
		}
		
		System.out.println("Update all edges and vertices.");
		LinkedList<Edge> newEdges = new LinkedList<>();
		for (Edge e : edges) {
			LinkedList<Edge> es = e.updateEdge();
			System.out.printf("update %d-%d size %d\n", e.v1.id, e.v2.id, es.size());
			if (!es.isEmpty()) {
				newEdges.addAll(es);
			} else
				newEdges.add(e);
		}
		edges.clear();
		edges = newEdges;

		System.out.printf("# segments: %d\n", edges.size());
	}
	
	public static void main(String[] args) {
//		test_quicksort();
		test_addPassingVertex();
	}
	
}
