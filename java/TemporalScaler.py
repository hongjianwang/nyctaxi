import matplotlib.pyplot as plt
import numpy as np
import sys
import subprocess
import random


def individualCases(fn):
    cnt = 0
    with open('./{0}'.format(fn) ,'r') as fin, open('./cases.log', 'w') as fout:

        for line in fin:
            Ts = line.split(";")
            scaledTs = Ts[0]
            ls = scaledTs.split(",")

            gnd = float(ls[0])
            nvs = [float(e) for e in ls[1:]]
            m = np.mean(nvs)
            sd = np.std(nvs)

            if np.abs(m - gnd) <= sd:
                cnt += 1
                if np.abs(m-gnd) <= 0.3 * sd and sd <= 0.1 * m:
                    print gnd, m, sd
                    plt.figure()
                    plt.plot(nvs)
                    plt.axhline(y=gnd)
                    fout.write(line)

    plt.show()
    print cnt
    subprocess.call(['wc', '-l', fn])



def groupCase(fn):

    with open('./{0}'.format(fn), 'r') as fin, open('groupCase.log', 'w') as fout:
        for line in fin:
            Ts = line.split(";")
            ls = Ts[0].split(",")

            gnd = float(ls[0])
            nvs = [float(e) for e in ls[1:]]

            m = np.mean(nvs)
            sd = np.std(nvs)

            #if np.abs(m - gnd) <= 0.1 * sd:
            if np.abs(m-gnd) <= sd and random.random() <= 0.01:
                fout.write("{0}, {1}\n".format(gnd, np.mean(nvs)))




if __name__ == '__main__':
    print sys.argv
    fn = sys.argv[1]
    flag = subprocess.check_output(['head', '-n', '1', fn])
    print flag
    if flag.strip() == 'neighborhoods':
        print 'Cut out the first 17 lines'
        with open(fn+"bk", 'wb') as fout:
            subprocess.call('tail -n +17 {0}'.format(fn).split(" "), stdout=fout)
        subprocess.call(['mv', fn+"bk", fn])


    # find cases where the s_i * t_i are concentrated and correct
    if sys.argv[2] == 'individual':
        print 'Find consistency of scaling in individual trip'
        individualCases(fn)
    elif sys.argv[2] == 'group':
        print 'Find consistency of scaling in a group of trips'
        groupCase(fn)
