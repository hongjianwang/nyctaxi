%{
Update: 10/29/2015
Hongjian


This script load the traffic flow from Lincoln Park and Madison Square
Garden.

Then apply the Basis Pursit to find a sparse approximation of the noise
component

Dependency:
    where is the "lincoln-MSG-out-in" from? which program generates this
    file?
%}

cd /home/hxw186/Documents/asp/
addpath(pwd)
as_setup


cd /home/hxw186/workspace/NYCtaxi/java/
a = importdata('lincoln-MSG-out-in');
f = a(2,:)';



z = zeros(size(f));

curGoal = 1000;
prvGoal = 2000;
cnt = 0;

op.loglevel = 0;
opts = as_setparms(op);
lambda = 600;   % 300 for Lincoln, 600 for MSG

while abs(prvGoal - curGoal) / prvGoal > 1e-7
    cnt = cnt + 1;
    prvGoal = curGoal;
    
    % update u
    fbar = f - z;
    u = zeros(24 *7 , 1);
    ucnt = zeros(24 * 7, 1);
    
    for i = 1:length(f)
        idx = mod(i, 168) + 1;
        u(idx) = u(idx) + fbar(i);
        ucnt(idx) = ucnt(idx) + 1;
    end
    u = u ./ ucnt;
    
    figure();
    subplot(3,1,1);
    plot(u);
    title('Periodic component');
    
    % update B
    B = f;
    for i = 168:168:length(f)
        if mod(i, 168) == 0
            B(i-167:i) = B(i-167:i) - u;
        else
            B(i-167:i) = B(i-167:i) - u(1:mod(i,168));
        end
    end
    
    subplot(3,1,2);
    plot(B);
    title('Actual - periodic component');
    [z, inform] = as_bpdn( eye(length(f)), B, lambda, opts );
    
    
    subplot(3,1,3);
    plot(z);
    title('Sparse component');

    curGoal = norm(B - z)^2 + lambda * norm(z, 1);
    fprintf('%d %g %g\n', cnt, prvGoal, curGoal);
end



%% find day from z

[zbar, zi] = sort(z, 'descend');
for i = 1:length(z)
    if zbar(i) ~= 0
        dv = datevec(zi(i) / 24 + datenum('2013-1-1 0:0:0'));
        dv(4) = mod(zi(i), 24);
        fprintf( '%s %d\n', datestr(dv), zbar(i) );
    end
end


