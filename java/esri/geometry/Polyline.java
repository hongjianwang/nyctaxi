package esri.geometry;

import java.util.LinkedList;



public class Polyline {

	public LinkedList<LinkedList<Point>> paths;
	public int numPoints;
	
	public Polyline() {
		paths = new LinkedList<>();
		numPoints = 0;
	}
	
	
	public void setEmpty() {
		paths.clear();
		numPoints = 0;
	}

	
	public void startPath(double a, double b) {
		Point p = new Point(a, b);
		paths.add(new LinkedList<Point>());
		paths.getLast().add(p);
		numPoints ++;
	}
	
	public void lineTo(double a, double b) {
		Point p = new Point(a, b);
		paths.getLast().add(p);
		numPoints ++;
	}
}
