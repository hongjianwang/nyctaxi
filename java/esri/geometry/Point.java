package esri.geometry;



/**
 * Point datatype in ESRI.
 * 
 * @author hxw186
 *
 */
public class Point {

	public float x;
	public float y;
	
	public Point() {
		x = 0;
		y = 0;
	}
	
	public Point(double a, double b) {
		x = (float) a;
		y = (float) b;
	}
	
	public void setX(double a) {
		x = (float) a;
	}
	
	public void setY(double b) {
		y = (float) b;
	}

}
