package nyctaxi;

import java.io.*;
import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

import shtaxi.SHTrip;


class Grid {
	public static final int nTimeSlots = Trip.nTimeSlots;
	public static int count = 0;
	
	double[] southwest;
	double[] northeast;
	int rowID;
	int colID;
	LinkedList<Trip> pickup;	// number of HashSet is determined by number of time slots

	
	public Grid( int rowID, int colID, double height, double width ) {
		count += 1;
		this.rowID = rowID;
		this.colID = colID;
		southwest = new double[]{ Raster.SouthWest[0] + rowID * height, Raster.SouthWest[1] + colID * width };
		northeast = new double[]{ southwest[0] + height, southwest[1] + width};
		pickup = new LinkedList<Trip>();
	}
}

enum REFTYPE {SPEED, DURATION};

public class Raster {

	public static final int nTimeSlots = Grid.nTimeSlots;
	public static final String rawTripInput = "../../../dataset/logtime_logl1/2013-01-someweek.trip";
	public static final String trnIdxOutput = "../../../dataset/logtime_logl1/training_10m_idx";
	public static final String tstIdxOutput = "../../../dataset/logtime_logl1/testing_10m_idx";
	public static final String evalOutput = "../../../dataset/logtime_logl1/ntaTS-eval";
	
	public static double[] SouthWest = { 40.680120, -74.061088 }; // latitude, longitude
	public static double[] NorthEast = { 40.879000, -73.777503 };
	
	public static final int EXTRACTOR_THREAD_NUM = 8;
	public static final int GRID_LEVEL = 1;
	
	public static REFTYPE Ref = REFTYPE.SPEED;
	
	
	/**
	 * ARIMA parameters
	 * 
	 * The key is NTA pairs, the value is the coefficients.
	 */
	public static HashMap<Short, double[]> ARIMA_paras = new HashMap<>();

	public static ArrayList<NTA> ntas = null;
	
	public static LinkedList<Thread> threadpool = null;
	
	static Random rnd = new Random();

	
	

	/**
	 * the time reference for different cases (src/dst)
	 * 
	 * The overall time reference is indexed by key ALL (= short.MAX_VALUE).
	 * 
	 * The src/dst NTA pair time reference is indexed by key Trip.ntaID,
	 * which is a short with high byte for srcID and low byte for dst ID,
	 * e.g. 3345 for (13,17), 3345 = 13 * 256 + 17
	 */
	public static HashMap<Short, TimeReference> tr = new HashMap<>();
	public static final short ALL = Short.MAX_VALUE;
	
	
	
	/**
	 * Configuration for input/output path.
	 */
	public static String ntaXML;
	public static String rawFile;
	public static String tripByMonthFolder;
	
	public static String testMonth;
	public static String[] trainMonths;
	
	static {
		try (BufferedReader br = new BufferedReader(new FileReader("path.config"))) {
			String l = null;
			while ((l = br.readLine()) != null) {
				String id = l.trim();
				if (id.equals("rawFile"))
					rawFile = br.readLine().trim();
				else if (id.equals("tripByMonthFolder"))
					tripByMonthFolder = br.readLine().trim();
				else if (id.equals("testMonth"))
					testMonth = br.readLine().trim();
				else if (id.equals("trainMonths")) {
					trainMonths = br.readLine().trim().split("[,\t]");
				} else if (id.equals("ntaXML")) {
					ntaXML = br.readLine().trim();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		ntas = NTA.getAllNeighborhoods();
		// initialize the global speed temporal reference
		tr.put(ALL, new TimeReference());
	}
	
	
	/**
	 * A two-level index, row first column second
	 */
	HashMap<Integer, HashMap<Integer, Grid>> raster;
	

	double gridH;
	double gridW;
	int maxRow;
	int maxCol;
	
	
	public Raster( int H, int W ) {
		raster = new HashMap<>();
		
		maxRow = H;
		maxCol = W;
		
		gridH = ( NorthEast[0] - SouthWest[0] ) / H;
		gridW = ( NorthEast[1] - SouthWest[1] ) / W;
		
		if (!tr.containsKey(ALL))
			tr.put(ALL, new TimeReference());
		Trip.cnt = 0;
	}
	
	
	/**
	 * Get all n-order neighbors of current grid.
	 * @param g current grid
	 * @param n the distance threshold for neighbors
	 * @return all neighbors (including current grid)
	 */
	HashSet<Grid> getGridNeighbors( Grid g, int n ) {
		HashSet<Grid> ns = new HashSet<>();
		int ri = g.rowID;
		int ci = g.colID;
		
		for (int i = ri - n; i <= ri + n; i++) {
			for (int j = ci - n; j <= ci + n; j++) {
				if ((Math.abs(i - ri) + Math.abs(j-ci)) <= n && raster.containsKey(i) && raster.get(i).containsKey(j)) {
					ns.add(raster.get(i).get(j));
				}
			}
		}
//		System.out.println(ns.size());
		return ns;
		
	}
	
	/**
	 * update the speed distribution over different time slots
	 * 
	 * this distribution is later used as a reference for weighting.
	 * @param t -- one Trip
	 */
	private void updateSpeedReference( Trip t ) {
		tr.get(ALL).updateReferenceByOneTrip(t);
		
		// update the reference by NTA pairs
		if (t.ntaID != Trip.EMPTY_NTAID) {
			if (tr.containsKey(t.ntaID)) {
				tr.get(t.ntaID).updateReferenceByOneTrip(t);
			} else {
				tr.put(t.ntaID, new TimeReference());
				tr.get(t.ntaID).updateReferenceByOneTrip(t);
			}
		}
	}
	
	/**
	 * Return the grid given it's row and column.
	 * If the grid does not exist, then build it first.
	 * @param rowID -- row, the 0 is at the bottom
	 * @param colID -- column, the 0 is on the left
	 * @return the grid
	 */
	private Grid getGrid( int rowID, int colID ) {
		if (raster.containsKey(rowID)) {
			if (raster.get(rowID).containsKey(colID))
				return raster.get(rowID).get(colID);
			else {
				Grid g = new Grid(rowID, colID, gridH, gridW);
				raster.get(rowID).put(colID, g);
				return g;
			}
		} else {
			Grid g = new Grid(rowID, colID, gridH, gridW);
			
			raster.put(rowID, new HashMap<Integer, Grid>());
			raster.get(rowID).put(colID, g);
			return g;
		}
	}
	
	
	private Trip addPoint( Trip t ) {
		updateSpeedReference(t);
		indexTrip(t);
		return t;
	}
	
	/**
	 * Helper function -- build bi-direction index for trips and grids
	 * @param t
	 */
	private void indexTrip( Trip t ) {
		// pickup index
		int[] p = mapCoordinate(t.pickup);
		Grid g = getGrid(p[0], p[1]);
		g.pickup.add(t);
		t.pikGrid = g;
		
		// drop-off index
		int[] d = mapCoordinate(t.dropoff);
		g = getGrid(d[0], d[1]);
		t.drpGrid = g;		
	}
	
	/**
	 * Add a trip, only when its two end grids are created already.
	 * @param l the raw string representation of the trip
	 * @return If the trip is added, return that trip, otherwise null.
	 */
	@SuppressWarnings("unused")
	private Trip conditioned_addPoint( String l ) {
		Trip t = new Trip(l);
		updateSpeedReference( t );
		
		int[] p = mapCoordinate(t.pickup);
		int[] d = mapCoordinate(t.dropoff);
		if ( tripGridExist(p[0], p[1], d[0], d[1]) ) {
			indexTrip(t);
			return t;
		} else {
			return null;
		}
	}
	
	/**
	 * Build the trip->grid index only.
	 * 
	 * For testing trips, we need to index their pickup/dropoff grids, while we don't want to add them
	 * into the Grids.
	 * @param t
	 */
	void reverseIndexTrip( Trip t ) {
//		updateSpeedReference(t);
		
		// pickup index
		int[] p = mapCoordinate(t.pickup);
		Grid g = getGrid(p[0], p[1]);
		t.pikGrid = g;
		
		// drop-off index
		int[] d = mapCoordinate(t.dropoff);
		g = getGrid(d[0], d[1]);
		t.drpGrid = g;				
	}
	
	/**
	 * Helper function -- map coordinate into grid (represented by row, column)
	 * @param coords GPS coordinate (latitude, longitude)
	 * @return the grid row and column
	 */
	private int[] mapCoordinate( double[] coords ) {
		int row = (int) ( (coords[0] - SouthWest[0]) / gridH );
		int col = (int) ( (coords[1] - SouthWest[1]) / gridW );
		return new int[] {row, col};
	}
	
	/**
	 * Helper function -- judge whether the two grids exist
	 * @param r1
	 * @param c1
	 * @param r2
	 * @param c2
	 * @return	true, if the Grid pairs exist; false, otherwise
	 */
	private boolean tripGridExist( int r1, int c1, int r2, int c2 ) {
		boolean f1 = raster.containsKey(r1) && raster.get(r1).containsKey(c1);
		boolean f2 = raster.containsKey(r2) && raster.get(r2).containsKey(c2);
		return f1 && f2;
	}
	
	
	/**
	 * count the taxi flow in/out of a specific rectangular region.
	 */
	public static void targetAreaTrafficVolume() {
		double[] sw = {40.750110, -73.995231};
		double[] ne = {40.751345, -73.990531};
		int[] goingIn = new int[24 * 7];
		int[] goingOut = new int[24 * 7];
		int cnt = 0;
		try ( BufferedReader br = new BufferedReader( new FileReader(rawTripInput)) ) {
			String l = null;
			while (( l = br.readLine()) != null) {
				Trip t = new Trip(l);
				if (t.pickup[0] >= sw[0] && t.pickup[0] <= ne[0] && 
						t.pickup[1] >= sw[1] && t.pickup[1] <= ne[1]) {
					goingOut[t.htwIdx] ++;
					
					if (t.htwIdx == 19) {
						cnt ++;
					}
				}
				
				if (t.dropoff[0] >= sw[0] && t.dropoff[0] <= ne[0] && 
						t.dropoff[1] >= sw[1] && t.dropoff[1] <= ne[1]) {
					goingIn[t.htwIdx] ++;
				}
				
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(cnt);
		
		for (int i : goingIn) {
			System.out.printf("%d\t", i);
		}
		System.out.print("\n");
		
		for (int j : goingOut) {
			System.out.printf("%d\t", j);
		}
	}
	
	
	
	/**
	 * Small scale evaluation of complate-trip based method
	 * 
	 * This function works everything out, only in a small scale.
	 * 
	 * 1). sample trips - identify testing and training trips
	 * 2). find neighbors
	 * 3). make predictions - write out results
	 * 
	 * This function is suitable for testing a subset of trips.
	 * 
	 * For complete NYC dataset, we use intermediate files and run the 
	 * experiments step by step, due to the large size of data and limited
	 * memory size available.
	 * 
	 * For large scale evaluations, refer to
	 * 		{@link #extractNeighborByMonth()}
	 * 		{@link #evaluateByMonth()}
	 * 
	 */
	public static void smallScaleEvaluation( ) {
		LinkedList<Trip> Ttest = new LinkedList<>();
		int TrainCnt = 0;
		int MAX_ORDER = 8;
		
		Raster r = new Raster(500, 500);
		rnd.setSeed(0);
		
		try (BufferedReader br = new BufferedReader( new FileReader(rawTripInput));
				BufferedWriter ftid = new BufferedWriter( new FileWriter(trnIdxOutput));
				BufferedWriter ftid2 = new BufferedWriter( new FileWriter(tstIdxOutput));
				) {
			
			ArrayList<BufferedWriter> bw = new ArrayList<>();
			
			for (int n = 6; n<=MAX_ORDER; n++) {
				bw.add( new BufferedWriter( new FileWriter(evalOutput+ String.format("_%d", n))) );
			}
			
			System.out.println("process testing trip and create training");
			int cnt = 0;
			String l = null;
			long t1 = System.currentTimeMillis();
			// randomly sample testing trip, and write the training trip ID into a file
			while ((l = br.readLine()) != null) {
				cnt += 1;
				/* random sample 10% -- which is not reasonable in time series settings
				if (rg.nextFloat() < 0.1) {
					Trip t = new Trip(l);
					r.reverseIndexTrip(t);
					Ttest.add(t);
				} else {
					r.addPoint(t);
					Ttrain.add(t);
					ftid.write(Integer.toString(cnt) + "\n");
				}
				*/
				
				Trip t = new Trip(l, ntas);
				
				if (t.htyIdx <= 6575) {	// 6575 hour index w.r.t year is 9/30 23:00:00
					r.addPoint(t);
					TrainCnt += 1;
					ftid.write(Integer.toString(cnt) + "\n");
				} else if (t.htyIdx > 6575 && t.htyIdx <= 7294){	// 7294 hour index w.r.t year is 10/31 23:00:00
					r.reverseIndexTrip(t);
					Ttest.add(t);
					ftid2.write(Integer.toString(cnt) + "\n");
				}

				if (Trip.cnt % 100000 == 0)
					System.out.println(String.format("%d\t%d", Trip.cnt, TrainCnt));
			}
			long t2 = System.currentTimeMillis();
			System.out.printf("Finished in %d seconds, Grid size %d.\n", (t2-t1)/1000, Grid.count);
		
			
			
			cnt = 0;
			/**
			 * Write out the speed reference
			 
			// make reference to the global speed reference
			HashMap<Integer, Integer> absRefCnt = tr.get("all").absRefCnt;
			HashMap<Integer, Double> absSpeedRef = tr.get("all").absSpeedRef;
			for (int c : absRefCnt.keySet()) {
				cnt += absRefCnt.get(c);
				fAbsRef.write(String.format("%d %d %g\n", c, absRefCnt.get(c), absSpeedRef.get(c) / absRefCnt.get(c)));
			}
			*/
			System.out.printf("There are %d time reference for %d trips\n", tr.get("all").absSpeedRef.size(), cnt);
			
			
			// prediction
			cnt = 0;
			System.out.println("make prediction");
			for (Trip t : Ttest) {
				
				for (int n = 6; n <= MAX_ORDER; n++ ) {
					// n-th order neighbors
					HashSet<Grid> src = r.getGridNeighbors(t.pikGrid, n);
					HashSet<Grid> dst = r.getGridNeighbors(t.drpGrid, n);
					HashSet<Trip> ts2 = new HashSet<>();
					for (Grid g : src) {
						for (Trip tr : g.pickup) {
							if ( dst.contains(tr.drpGrid)) {
								ts2.add(tr);
							}							
						}
					}
					
					// print trip prediction
					if (ts2.size() > 0)
						bw.get(n-6).write(String.format("%g\t%g\t%g\t%g\t%d\n", t.estimateT(ts2, "avg"), t.estimateT(ts2, "relaweight"), 
								t.estimateT(ts2, "arima2-absweight"), t.estimateT(ts2, "localspeed-relaweight"), ts2.size()));
					else
						bw.get(n-6).write("-1\t-1\t-1\t-1\t0\n");
				}
				
				// print the error with ID
//				if (ts2.size() > 0) {
//					bw.write(String.format("\n%g\t%g\t%g\t%d\t%d\t%g\t%g\t%g\t%g\t%g\n", t.estimateT(ts, "moreNeigh-absweight"), 
//							t.estimateT(ts, "weight"), t.estimateT(ts, "moreNeigh-spascale-absweight"), ts.size(),
//							t.hourIdx, t.t, t.pickup[0], t.pickup[1], t.dropoff[0], t.dropoff[1] ));
//					for (Trip tt : ts)
//						bw.write(String.format("%g\t%d\t%g\n", tt.t, tt.hourIdx, tt.v));
//				}
				
				// test L1/t consistency
//				int tmp = 0;
//				bw2.write(String.format("%g\t", t.l1/t.t));
//				for (Trip tn : ts2) {
//					double vref = Raster.absSpeedRef.get(t.htyIdx) / Raster.absRefCnt.get(t.htyIdx);
//					double vn = Raster.absSpeedRef.get(tn.htyIdx) / Raster.absRefCnt.get(tn.htyIdx);
//					double scaltn = tn.t * vn / vref; 
//					if ((tn.l1 - t.l1) * (scaltn - t.t) > 0)
//						tmp++;
//					bw2.write(String.format("%g\t", tn.l1/tn.t));
//				}
//				bw2.write("\n");
//				bw.write(String.format("%g\t%d\t%d\n", t.l1 / t.t, tmp, ts2.size()));
				
				
				if (cnt++ % 50000 == 0)
					System.out.printf("#testing processed: %d.\n", cnt);
			}
			long t3 = System.currentTimeMillis();
			System.out.printf("Finished in %d seconds.\n", (t3-t2)/1000);
			System.out.printf("#testing processed: %d.\n", cnt);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	/**
	 * Count how many neighboring trips we have, when we use different level of grids.
	 * 
	 * {@Trip.nTimeSlots} should be set to 1 for this function.
	 * 
	 * It takes quite long (1 hour) to run this function.
	 * 
	 * Conclusion, when grid order = 2, we cover 95% of all the testing trips with neighbors.
	 */
	public static void countNumNeighbors() {
		LinkedList<Trip> Ttrain = new LinkedList<>();
		LinkedList<Trip> Ttest = new LinkedList<>();
		
		String fin = "../../../dataset/SH_taxi_trip";
		String fout = "neighborCnt";
		
//		Raster r = new Raster(500, 500);
		// Shanghai Raster
		Raster.NorthEast = new double[] { 31.4330, 121.933 };
		Raster.SouthWest = new double[] { 30.8973, 121.018 };
		Raster r = new Raster(1200, 2000);
		
		
		rnd.setSeed(0);

		try (BufferedReader br = new BufferedReader( new FileReader(fin));
				BufferedWriter bw = new BufferedWriter( new FileWriter(fout))) {
			System.out.println("process testing trip and create grids");
			int cnt = 0;
			String l = null;
	
			// randomly sample testing trip, and write the training trip ID into a file
			while ((l = br.readLine()) != null) {
				cnt += 1;
				Trip t = new Trip(l);
				if (rnd.nextFloat() < 0.1) {
					r.reverseIndexTrip(t);
					Ttest.add(t);
				} else {
					r.addPoint(t);
					Ttrain.add(t);
				}

				if (Trip.cnt % 50000 == 0)
					System.out.println(String.format("%d\t%d", Trip.cnt, Ttrain.size()));
			}
			System.out.println(Grid.count);
			
			
			// find neighbors and count
			cnt = 0;
			System.out.println("find direct neighbors");
			for (Trip t : Ttest) {
				// find direct neighboring grid
				Grid gsrc = t.pikGrid;
				Grid gdst = t.drpGrid;
				
				int[] cnt_n = new int[6];
				for (Trip tr : gsrc.pickup) {
					if (tr.drpGrid == gdst)
						cnt_n[0] += 1;
				}
				
				
				for (int n = 0; n < 6; n++) {
					HashSet<Grid> src = r.getGridNeighbors(t.pikGrid, n);
					HashSet<Grid> dst = r.getGridNeighbors(t.drpGrid, n);
					for (Grid gs : src) {
						for (Trip tr : gs.pickup) {
							if (dst.contains(tr.drpGrid)) {
								cnt_n[n] += 1;
							}
						}
					}
				}
				
				for (int c : cnt_n)
					bw.write(String.format("%d\t", c));
				bw.write("\n");
				
				if (cnt++ % 50000 == 0)
					System.out.println(cnt);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	/**
	 * Find the neighborhood (NTA) id for each trip and save them in separate *.nta file
	 * 
	 * This function builds the NTA from xml file first. Then it goes through all the 
	 * trips, calculates and saves their src/dst neighborhood IDs.
	 * 
	 * @deprecated This function is merged with {@link SampleTrip#sampleAndCompressTrip()}.
	 */
	public static void identifyTripNTA() {
		String[] months = {"2013-06", "2013-07", "2013-08", "2013-09","2013-10"};
		
		for (int i = 0; i < months.length; i++) {
			try (BufferedReader br = new BufferedReader(new FileReader(tripByMonthFolder + months[i] + ".trip"));
					BufferedWriter bw = new BufferedWriter(new FileWriter(tripByMonthFolder + months[i] + ".nta"))) {
			
				String l = null;
				int cnt = 0;
				System.out.printf("Generate NTA for month %s\n", months[i]);
				long t1 = System.currentTimeMillis();
				while ((l=br.readLine()) != null) {
					Trip t = new Trip(l);
					bw.write(t.NTAbelonging(ntas) + "\n");
					
					if (cnt++ % 1000000 == 0)
						System.out.println(cnt);
				}
				long t2 = System.currentTimeMillis();
				System.out.printf("Finished in %d seconds\n", (t2-t1)/1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Calculate neighbor by months
	 * 
	 * We use the last month (December) as testing set. The rest of 11 months are training set.
	 * @param trainM the index of training month from 0 to 10
	 * 
	 * @Warning This multi-threaded function run 20 hours.
	 * This function generate the evaluation intermediate file. After this we should apply {@link #evaluateByMonth()}.
	 */
	public static void extractNeighborByMonth( int trainM ) {
		// testMonth, trainMonths are initiated from file
		String[] trainMs;
		if (trainM == 11) // all
			trainMs = trainMonths;
		else if (trainM < 0 || trainM > 10) {
			System.err.printf("Training month does not exist.\n", trainM);
			return;
		} else {
			trainMs = new String[] {trainMonths[trainM]};
		}
		
		for (int i = 0; i < trainMs.length; i++) {
			try (DataInputStream ftrain = new DataInputStream(new BufferedInputStream( new FileInputStream(tripByMonthFolder + trainMs[i] + ".btrip")));
					DataOutputStream fout = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(tripByMonthFolder + trainMs[i] + ".bneibors")));
					) {
				
				Raster r = new Raster(500, 500);
				
				// training
				long t1 = System.currentTimeMillis();
				System.out.println("Train with " + trainMs[i] + " trips");
				boolean eof = false;
				while ( !eof ) {
					try {
						Trip t = new Trip(ftrain);
						r.addPoint(t);
						
						if (Trip.cnt % 1000000 == 0)
							System.out.println(String.format("# training trips: %d", Trip.cnt));
					} catch (IOException e) {
						eof = true;
					}
				}
				long t2 = System.currentTimeMillis();
				Runtime rt = Runtime.getRuntime();
				long usedMB = (rt.totalMemory() - rt.freeMemory()) / 1024 / 1024;
				System.out.printf("Construct %d training trips in %d seconds, memory usage %dMB.\n", Trip.cnt, (t2-t1)/1000, usedMB);
				
				// testing
				int cnt = 0;
				
				DataInputStream ftest = new DataInputStream( new BufferedInputStream(new FileInputStream(tripByMonthFolder + testMonth + ".btrip")));
				
				// reset concurrent signal
				TripExtractor.NumClosedExtractor.set(0);
				
				// create concurrent testing trip consumers
				ArrayList<Thread> threads = new ArrayList<>();
				
				for (int k = 0; k < EXTRACTOR_THREAD_NUM; k++) {
					TripExtractor tp = new TripExtractor(k, r);
					threads.add(tp);
					tp.start();
				}
				TripFinalizer tf = new TripFinalizer(fout);
				threads.add(tf);
				tf.start();
				
				// producer feeding
				eof = false;
				while( !eof ) {
					try {
						Trip t = new Trip(ftest);
						BuildTripTask triptask = new BuildTripTask(cnt, t);
						
						TripExtractor.bq.put(triptask);
						cnt ++;
						
						if (cnt % 1000000 == 0) {	// 1 million
							System.out.println(cnt);
						}
					} catch (IOException e) {
						eof = true;
					}
				}
				
				TripExtractor.bq.put(new BuildTripTask(-1, null));
				
				// wait consumer to join
				for (Thread tp: threads)
					tp.join();
				
				ftest.close();
					
				long t3= System.currentTimeMillis();
				System.out.printf("prediction finished in %d seconds\n", (t3-t2)/1000);
				
				r = null;
				Raster.tr.clear();
				System.gc();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	
	/**
	 * This function evaluate the testing trips
	 * 
	 * @param trainM number of months we want for training, taking value 1-11.
	 * If trainM = -1, -3, we take the days of November. 
	 * If trainM = 1, we take November. 
	 * If trainM = 11, we take January to November. 
	 * 
	 * Dependency:
	 * {@link #retrieveSpeedReference()}.
	 * {@link #retrieve_ARIMA_parameter()}.
	 */
	public static void evaluateByMonth( int trainM, double sampleRate ) {
		
		String[] trainMs;
		String Fextention = "";

		if (sampleRate < 1)
			Fextention += String.format(".s%d", (int) (sampleRate * 100));
		
		if (trainM < -30 || trainM > 11) {
			System.err.printf("Training month %d does not exist.\n", trainM);
			return;
		} else if (trainM < 0) {	// use half month
			trainMs = new String[]{ trainMonths[10] };
			Fextention += String.format(".11d%d", -trainM);
		} else {
			trainMs = trainMonths;
			Fextention += String.format(".m%d", trainMs.length);
		}
		if (Ref == REFTYPE.SPEED)
			Fextention += ".eval";
		else if (Ref == REFTYPE.DURATION)
			Fextention += ".teval";
		
		try {
			int cnt = 0;
			DataInputStream test = new DataInputStream(new BufferedInputStream(
					new FileInputStream(tripByMonthFolder + testMonth + ".btrip")));
			BufferedWriter tbw = new BufferedWriter(new FileWriter(
					tripByMonthFolder + testMonth + Fextention));
			ArrayList<DataInputStream> trains = new ArrayList<>();
			System.out.printf("Training with the following %d month(s)\n", trainMs.length);
			for (int n = 0; n < trainMs.length; n++) {
				trains.add(new DataInputStream(new DataInputStream(new BufferedInputStream( 
						new FileInputStream(tripByMonthFolder + trainMs[n] + ".bneibors")))));
				System.out.println(trainMs[n]);
			}
			
			// reset the Thread
			SimpleTripEvaluator.counter.set(0);
			SimpleTripEvaluator.NumClosedEvaluator.set(0);
			
			threadpool = new LinkedList<Thread>();
			for (int i = 0; i < EXTRACTOR_THREAD_NUM; i++) {
				SimpleTripEvaluator ste = new SimpleTripEvaluator(i);
				threadpool.add(ste);
				ste.start();
			}
			SimpleTripEvaluationWriter stew = new SimpleTripEvaluationWriter(tbw);
			stew.start();
			threadpool.add(stew);
			
			long t1 = System.currentTimeMillis();
			boolean EOF = false;
			int nn = 0;
			while ( !EOF ) {
				try {
					SimpleTrip trip = null;
					if (Raster.Ref == REFTYPE.SPEED)
						trip = new SimpleTrip(test);
					else if (Raster.Ref == REFTYPE.DURATION)
						trip = new SimpleTripUseDurationAsReference(test);
					
					LinkedList<NeededNeighborInfo> neighborInfos = new LinkedList<>();
					
					for (DataInputStream bw : trains) {
						int numNeighbors = bw.readShort();
						for (int i = 0; i < numNeighbors; i++) {
							NeededNeighborInfo ni = new NeededNeighborInfo(bw);
							if (trainM > 0 )
								if (sampleRate > 1 || rnd.nextFloat() < sampleRate)
									neighborInfos.add(ni);
							// ================ start ===============
							// specifically handle weeks of November
							if (trainM < 0 && ni.htyIdx > 8040 + trainM * 24) {
								neighborInfos.add(ni);
								nn++;
							}
							// ================ end =================
						}
					}
					
					if (!neighborInfos.isEmpty()) {
						EvaluatorTask tsk = new EvaluatorTask(trip, neighborInfos);
						SimpleTripEvaluator.lbq.put(tsk);
						cnt++;
					}
					
					if (cnt % 1000000 == 0)
						System.out.println(cnt);
					
					if (cnt > 1_000_000)
						break;
				} catch (IOException e) {
					EOF = true;
				}
			}
			System.err.printf("Total neighbors added %d\n", nn);
			Runtime rt = Runtime.getRuntime();
			long useMB = (rt.freeMemory() - rt.totalMemory()) / 1024 / 1024;
			
			// end the threads
			SimpleTripEvaluator.lbq.put(new EvaluatorTask(null, null));
			
			for (Thread t : threadpool)
				t.join();
			long t2 = System.currentTimeMillis();
			System.out.printf("evaluateByMonth() finished in %d milliseconds, use %dMB memory.\n", (t2-t1), useMB);
			
			test.close();
			tbw.close();
			for (DataInputStream br : trains)
				br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Use 100% training trips in each month
	 * @param trainM
	 */
	public static void evaluateByMonth(int trainM) {
		evaluateByMonth(trainM, 10);
	}
	
	
	/**
	 * Generate the speed reference (relative and absolute) for each pair of regions.
	 * 
	 * The reverse of function {@link #retrieveSpeedReference()}.
	 */
	public static void generateSpeedReference(double sampleRate) {
//		String[] months = {"2013-01", "2013-02", "2013-03", "2013-04", "2013-05", "2013-06", "2013-07", "2013-08", "2013-09","2013-10", "2013-11", "2013-12"};
		String[] months = new String[1 + trainMonths.length];
		System.arraycopy(trainMonths, 0, months, 0, trainMonths.length);
		months[trainMonths.length] = testMonth;
		
		String Fextention = "";
		if (sampleRate < 1)
			Fextention += String.format(".s%d", (int) (sampleRate * 100));
		if (Ref == REFTYPE.SPEED)
			Fextention += ".ref";
		else if (Ref == REFTYPE.DURATION)
			Fextention += ".tref";
		
		for (int n = 0; n < months.length; n++) {
			try (DataInputStream br = new DataInputStream(new BufferedInputStream(new FileInputStream(tripByMonthFolder + months[n] + ".btrip")));
					BufferedWriter bw = new BufferedWriter(new FileWriter(tripByMonthFolder + months[n] + Fextention)); ) {

				Raster r = new Raster(500, 500);
				
				System.out.println(months[n]);
				
				boolean eof = false;
				long t1 = System.currentTimeMillis();
				while ( !eof ) {
					try {
						Trip t = new Trip(br);
						r.updateSpeedReference(t);
						if (Trip.cnt % 2000000 == 0)
							System.out.println(Trip.cnt);
					} catch (IOException e) {
						eof = true;
					}
				}
				long t2 = System.currentTimeMillis();				

				// write out reference
				for (short key : Raster.tr.keySet()) {
					tr.get(key).writeToFile(key, bw);
				}

				r = null;
				System.gc();
				Runtime rt = Runtime.getRuntime();
				long usedMB = (rt.totalMemory() - rt.freeMemory()) / 1024 / 1024;				
				Raster.tr.clear();
				System.out.printf("Construct %d training trips in %d seconds, memory usage %dMB.\n", Trip.cnt, (t2-t1)/1000, usedMB);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void generateSpeedReference() {
		generateSpeedReference(10);
	}
	
	
	/**
	 * Retrieve the various speed references from disk, and reconstruct the time reference classes. It also retrieve the ARIMA parameters.
	 * 
	 * The reverse of function {@link #generateSpeedReference()}.
	 * 
	 * This function is required by {@link #evaluateByMonth()}.
	 */
	public static void retrieveSpeedReference(double sampleRate) {
//		String[] months = {"2013-01", "2013-02", "2013-03", "2013-04", "2013-05", "2013-06", "2013-07", "2013-08", "2013-09","2013-10", "2013-11", "2013-12"};
		String[] months = new String[1 + trainMonths.length];
		System.arraycopy(trainMonths, 0, months, 0, trainMonths.length);
		months[trainMonths.length] = testMonth;
		
		String Fextention = "";
		if (sampleRate < 1)
			Fextention += String.format(".s%d", (int)(sampleRate * 100));
		if (Ref == REFTYPE.SPEED)
			Fextention += ".ref";
		else if (Ref == REFTYPE.DURATION)
			Fextention += ".tref";
		
		// empty the old reference if there is one
		Raster.tr.clear();
		// =========================================
		for (int n = 0; n < months.length; n++) {
			System.out.printf("Extract refernce from month %s\n", months[n]);
			try (BufferedReader br = new BufferedReader(new FileReader(tripByMonthFolder + months[n] + Fextention))) {
				String l = null;
				long t1 = System.currentTimeMillis();
				while((l = br.readLine()) != null) {
					String[] relaRef = l.split("\t");	//	0: key, after count,Sum,variance
					short key = Short.parseShort(relaRef[0]);
					if(!tr.containsKey(key)) {
						tr.put(key, new TimeReference());
					}
					TimeReference ref = tr.get(key);
					// update the relative reference
					for (int i = 0; i < Trip.nTimeSlots; i++) {
						String[] pair = relaRef[i+1].split(",");
						ref.relaRefCnt[i] = Integer.parseInt(pair[0]);
						ref.relaRefSum[i] = Double.parseDouble(pair[1]);						
					}
					// update the absolute reference
					l = br.readLine();
					String[] absRef = l.split("\t");
					short key2 = Short.parseShort(absRef[0]);
					if (key2 == key) {
						ref = tr.get(key2);
					} else {
						throw new Exception("NTA keys do not match.");
					}
					for (int i = 1; i < absRef.length; i++) {
						String[] triple = absRef[i].split("[,:]");
						ref.absRefCnt.put(Short.parseShort(triple[0]), Integer.parseInt(triple[1]));
						ref.absSpeedRef.put(Short.parseShort(triple[0]), Double.parseDouble(triple[2]));
					}
					ref.initialize_statisticsFields();	// get ENOUGH_TRIPS, total_trip
				}
				long t2 = System.currentTimeMillis();
				Runtime rt = Runtime.getRuntime();
				long usedMB = (rt.totalMemory() - rt.freeMemory()) / 1024 / 1024;
				System.gc();
				System.out.printf("Retrieve reference finished in %d milliseconds, memory usage %dMB.\n", (t2-t1), usedMB);
				
				System.out.println(tr.get(ALL).absSpeedRef.size());
				System.out.println(tr.get(ALL).absRefCnt.size());
				System.out.println(tr.get(ALL).relaRefSum.length);
				System.out.println(tr.get(ALL).relaRefCnt.length);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Use 100% training trips in each month.
	 */
	public static void retrieveSpeedReference() {
		retrieveSpeedReference(10);
	}
	
	
	
	/**
	 * Write the speed reference in a text file.
	 * 
	 * @param goal
	 * 		- "compareRegion" extract the relative speed reference of all regions
	 * 		- "rela_vs_abs" extract the relative and absolute global reference 
	 * Dependency:
	 * 		Raster.tr needs to be created first.
	 */
	public static void writeReadableSpeedReference(String goal) {
		retrieveSpeedReference();
		if (goal.equals("compareRegion")) {
			try (BufferedWriter bw = new BufferedWriter(new FileWriter("region_pair_refence"))) {
				for (short k : tr.keySet()) {
					bw.write(Short.toString(k));
					int[] refCnt = tr.get(k).relaRefCnt;
					double[] refSum = tr.get(k).relaRefSum;
					
					for (int i = 0 ; i < refCnt.length; i++) {
						bw.write(String.format(",%g", refSum[i] / refCnt[i]));
					}
					bw.write("\n");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (goal.equals("rela_vs_abs")) {
			try (BufferedWriter bw = new BufferedWriter(new FileWriter("rela_vs_abs"))) {
				int[] relaCnt = tr.get(Raster.ALL).relaRefCnt;
				double[] relaSum = tr.get(Raster.ALL).relaRefSum;
				for (int i = 0; i < relaCnt.length; i++) {
					bw.write(String.format("%g ", relaSum[i] / relaCnt[i]));
				}
				bw.write("\n");
				
				HashMap<Short, Integer> absCnt = tr.get(Raster.ALL).absRefCnt;
				HashMap<Short, Double> absSum = tr.get(Raster.ALL).absSpeedRef;
				short s = Collections.min(absCnt.keySet());
				short e = Collections.max(absCnt.keySet());
				for (short i = s; i <= e; i++) {
					if (SimpleTrip.get_hourIdx(i) == 0)
						bw.write("\n");
					bw.write(String.format("%g ", absSum.get(i) / absCnt.get(i)));
				}
				bw.write("\n");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	
	/**
	 * Get number of trips per day
	 */
	public static void tripPerDay() {
		retrieveSpeedReference();
		
		int[] tripN = new int[365];
		HashMap<Short, Integer> cnts = tr.get(ALL).absRefCnt;
		for (short htyidx : cnts.keySet()) {
			tripN[htyidx / 24] += cnts.get(htyidx);
		}
		
		try (BufferedWriter bw = new BufferedWriter(new FileWriter("tripPerDay"))) {
			for (int e : tripN) {
				bw.write(String.format("%d\n", e));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Calculate the significance of each assumption.
	 * 
	 * Dependency: {@link #retrieveSpeedReference()} 
	 */
	public static void assumption_significance_test() {
		String testMonths = "2013-12";
//		String[] months = {"2013-01", "2013-02", "2013-03", "2013-04", "2013-05", "2013-06", "2013-07", "2013-08", "2013-09","2013-10", "2013-11"};
		String[] months = {"2013-11"};
		
		
		double err1 = 0, gnd_sum1 = 0, err2 = 0, gnd_sum2 = 0, err3 = 0, gnd_sum3 = 0;
		double err4 = 0, gnd_sum0 = 0, gnd_sum4 = 0;
		
		try {
			DataInputStream test = new DataInputStream(new BufferedInputStream(new FileInputStream(tripByMonthFolder + testMonths + ".btrip")));
			ArrayList<DataInputStream> trains = new ArrayList<>();
			for (int n = 0; n < months.length; n++) {
				trains.add(new DataInputStream(new BufferedInputStream(new FileInputStream(tripByMonthFolder + months[n] + ".bneibors"))));
			}
			
			System.out.println("Test file is " + tripByMonthFolder + testMonths + ".btrip");
			
			int cnt = 0;
			boolean EOF = false;
			while (! EOF) {
				try {
					SimpleTrip trip = new SimpleTrip(test);
					LinkedList<NeededNeighborInfo> neighs = new LinkedList<>();
					
					for (DataInputStream bw : trains) {
						int numNeigh = bw.readShort();
						for( int i = 0; i < numNeigh; i++) {
							NeededNeighborInfo ni = new NeededNeighborInfo(bw);
							neighs.add(ni);
						}
					}
					
					if (trip.t > 240) {
					LinkedList<double[]> stat_list = trip.generate_pairVariables_for_assumption(neighs);
					for (double[] oc : stat_list) {
						err1 += Math.abs(oc[0] - oc[1]);	// assumption 2: ratio of speed
						err4 += Math.abs(oc[0] - oc[1]) / oc[1];
						gnd_sum1 += oc[1];
						gnd_sum0 += 1;
						err2 += Math.abs(oc[2] - oc[3]);	// assumption 1: distance
						gnd_sum2 += oc[2];
						gnd_sum4 += oc[3];
						err3 += Math.abs(oc[4] - oc[5]);
						gnd_sum3 += oc[4];
					}
					
					if (cnt ++ % 10000 == 1)
						System.out.println(cnt);
					
					
					if (cnt == 1_000_000)
						break;
					}
				} catch (EOFException e) {
					EOF = true;
				}
			}
			
			if (Raster.Ref == REFTYPE.SPEED) {
				System.out.printf("MRE %g, %g for assumption 2 -- ratio of travel speed\n", err1 / gnd_sum1, err4 / gnd_sum0);
				System.out.printf("MRE %g, %g for assumption 1 -- distance\n", err2 / gnd_sum2, err2 / gnd_sum4);
				System.out.printf("MRE %g for assumption 4 -- vref as v\n", err3 / gnd_sum3);
			}
			else if (Raster.Ref == REFTYPE.DURATION) {
				System.out.printf("MRE %g for assumption 1 -- ratio of travel time\n", err1 / gnd_sum1);
				System.out.printf("MRE %g for assumption 2 -- distance\n", err2 / gnd_sum2);
				System.out.printf("MRE %g for assumption 5 -- travel time\n", err3 / gnd_sum3);
			}
			
			test.close();
			for (DataInputStream br : trains)
				br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Learn the ARIMA parameter for the speed reference on each region.
	 * 
	 * Dependency:
	 * Run {@link #retrieveSpeedReference()} first to build the TimeReferences.
	 * 
	 * @deprecated the results file is appended to "arima_parameters". If this function needs to be run again, 
	 * remember to empty that file first.
	 * 
	 * The output file is used by {@link #retrieve_ARIMA_parameter()}.
	 */
	public static void learn_ARIMA_parameter() {
		int cnt = 0, n_reliable_nta = 0, n_covered_trip = 0, n_total_trip = 0;
		long t1 = System.currentTimeMillis();
		for (short id : tr.keySet()) {
			System.out.printf("Generate time series for region pair %d.\n", id);
			int[] res = tr.get(id).generateRegionBasedAbsRef_learnARIMA(id);
			if ( id != Short.MAX_VALUE ) {
				System.out.printf("%s -- %d\n", id, tr.get(id).absRefCnt.size());
				cnt ++;
				n_reliable_nta += res[0];
				n_total_trip += res[1];
				if (res[0] == 1) {
					n_covered_trip += res[1];
				}
			}
		}
		long t2 = System.currentTimeMillis();
		Runtime rt = Runtime.getRuntime();
		long usedMB = (rt.totalMemory() - rt.freeMemory()) / 1024 / 1024;
		System.out.printf("Learn ARIMA parameter finished in %d seconds, with memory usage: %dMB.\n", (t2-t1)/1000, usedMB);
		System.out.printf("We have %d NTAs, with reliable reference, out of %d NTAs.\n", n_reliable_nta, cnt);
		System.out.printf("In thoes reliable NTAs, %d trips, out of %d, are covered.\n", n_covered_trip, n_total_trip);
	}
	
	
	/**
	 * Retrieve the ARIMA parameters from related file.
	 * 
	 * The ARIMA parameters are generated from {@link #learn_ARIMA_parameter()}.
	 */
	public static void retrieve_ARIMA_parameter() {
		System.out.println("retrieve_ARIMA_parameter.");
		try (BufferedReader br = new BufferedReader(new FileReader(Raster.tripByMonthFolder + 
				"ARIMA/arima_parameters"))) {
			String l = null;
			while ((l = br.readLine()) != null) {
				String[] ls = l.split("\t");
				double[] ars = new double[] { Double.parseDouble(ls[1]), Double.parseDouble(ls[2]) };
				ARIMA_paras.put(Short.parseShort(ls[0]), ars);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void LR_train() {
		String[] months = {"2013-01", "2013-02", "2013-03", "2013-04", "2013-05", "2013-06", "2013-07", "2013-08", "2013-09","2013-10", "2013-11"};
		
		try {
			

			for (int i = 0; i < months.length; i++) {
				BufferedReader br = new BufferedReader(new FileReader(tripByMonthFolder + months[i] + ".trip"));
				BufferedWriter bw = new BufferedWriter(new FileWriter(tripByMonthFolder + months[i] + ".LR"));
				
				String l = null;
				while ((l = br.readLine()) != null) {
					Trip trp = new Trip(l);
					bw.write(String.format("%g,%g\n", trp.t, trp.dist));
				}
				br.close();
				System.out.printf("%s finished\n", months[i]);
				bw.close();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	///========================================================================
	/// 			Evaluation our method on Shanghai Taxi Dataset
	///========================================================================
	/**
	 * This function specifically implemented for the Shanghai Taxi trips.
	 * 
	 * I use the function {@link SHTrip#generateNYCformattedData()} to reformat the data.
	 * Then the {@link Trip} class can be used directly on the SH dataset. 
	 */
	public static void evaluate_SH_taxi() {
		LinkedList<Trip> Ttest = new LinkedList<>();
		int trainCnt = 0;
		int MAX_ORDER = 3;
		
		// extreme cases
		List<Integer> lst = new ArrayList<>(); //Arrays.asList(1670551, 894593, 1831044, 239535, 1006426, 633122, 605654, 206978);
		HashSet<Integer> targs = new HashSet<>(lst);
		
		// Shanghai Raster
		Raster.NorthEast = new double[] { 31.4330, 121.933 };
		Raster.SouthWest = new double[] { 30.8973, 121.018 };
		Raster r = new Raster(1200, 2000);
		try (BufferedReader br = new BufferedReader(new FileReader("../../../dataset/SH_taxi_trip"));
				BufferedWriter bw = new BufferedWriter(new FileWriter("../../../dataset/SH_taxi_eval"));
				BufferedWriter refw = new BufferedWriter(new FileWriter("reference-all"))) {
			
			boolean RANDOM_SAMPLE_TEST_SET = false;
			double test_proportion_low = 0.1;
			double test_proportion_high = 0.1;
			final int cutoffthreshold = 160;
			HashSet<Integer> tstIDs = null;
			if (RANDOM_SAMPLE_TEST_SET)
				System.out.printf(String.format("Randomly generate testing set with probability %g\n", test_proportion_low));
			else {
				System.out.println("Testing set is given in file.");
				tstIDs = SHTrip.getIDSet("prediction-bias");
			}
			
			System.out.println("Process testing trips ...");
			String l = null;
			
			
			while ((l = br.readLine()) != null) {
				
				Trip t = new Trip(l);
				// random sample testing set
				if (RANDOM_SAMPLE_TEST_SET) {
					if ((t.t > cutoffthreshold && rnd.nextDouble() < test_proportion_low) ||
							(t.t <= cutoffthreshold && rnd.nextDouble() < test_proportion_high)) {
						Ttest.add(t);
						r.reverseIndexTrip(t);
					} else {
						r.addPoint(t);
						trainCnt += 1;
					}
				} else {	// fixed testing set
					if (tstIDs.contains(t.id)){
						double sp = sample_probability(t);
						if (rnd.nextDouble() < sp) {
							Ttest.add(t);
							r.reverseIndexTrip(t);
						}
					} else {
						r.addPoint(t);
						trainCnt += 1;
					}
				}
				
				if (Trip.cnt % 500_000 == 0)
					System.out.printf("%d\t%d\n", Trip.cnt, trainCnt);
			}
			
			
			System.out.println("Make prediction");
			CaseStudy cs = new CaseStudy();
			for (Trip t : Ttest) {
				HashSet<Grid> src = r.getGridNeighbors(t.pikGrid, MAX_ORDER);
				HashSet<Grid> dst = r.getGridNeighbors(t.drpGrid, MAX_ORDER);
				
				// search similar trips
				HashSet<Trip> ts2 = new HashSet<>();
				for (Grid g : src) {
					for (Trip tr : g.pickup) {
						if (dst.contains(tr.drpGrid)) {
							ts2.add(tr);
						}
					}
				}
				
				if (ts2.size() > 0) {
					bw.write(String.format("%g\t%g\t%g\t%g\t%g\t%d\t%d\n", t.t, t.estimateT(ts2, "avg"), 
							t.estimateT(ts2, "median"),
							t.estimateT(ts2, "relaweight"), t.estimateT(ts2, "absweight"), ts2.size(), t.id));
				}
				

				// print detailed neighbors for selected testing trips
				if (targs.contains(t.id)) {
					cs.addCase(t, ts2);
					cs.getNeighborsInDetail();
					cs.saveNeighborIDsToFile();
					System.out.printf(String.format("%g\t%g\t%g\t%g\t%g\t%d\t%d\n", t.t, t.estimateT(ts2, "avg"), 
							t.estimateT(ts2, "median"),
							t.estimateT(ts2, "relaweight"), t.estimateT(ts2, "absweight"), ts2.size(), t.id));
					System.out.println("\n");
				}
			}
			
			cs.closeFile();
			tr.get(ALL).writeToFile(ALL, refw);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	
	
	/**
	 * Assistant function for {@link #evaluate_SH_taxi()}.
	 * 
	 * This function gives the sampling probability for converting one testing set
	 * to another, in order to keep the same distribution.
	 * @param t
	 * @return
	 */
	public static double sample_probability(Trip t) {
		double p = 0;
		int minute = (int) (t.t / 60);
		double[] prob_distri = new double[] {0.0041601,
				0.014635 ,
				0.054316 ,
				0.15187   ,
				0.3924     ,
				0.84828   ,
				1.5251     ,
				3.2747     ,
				4.4875     ,
				9.1312     ,
				9.8872     ,
				17.982     ,
				52.886     ,
				88.956     ,
				75.803      };
		
		if (minute >= prob_distri.length)
			p = prob_distri[prob_distri.length-1];
		else
			p = prob_distri[minute];
		return p;
	}
	
	
	/**
	 * verify the consistency of temporal scaling factor
	 * @param NL_THRESHOLD lower bound of number of neighbors
	 * @param NH_THRESHOLD upper bound of number of neighbors
	 */
	public static void verify_temporalScalingFactor(int NL_THRESHOLD, int NH_THRESHOLD) {
		retrieveSpeedReference();
		try {
			DataInputStream test = new DataInputStream(new BufferedInputStream(
					new FileInputStream(tripByMonthFolder + testMonth + ".btrip")));
			DataInputStream train = new DataInputStream(new BufferedInputStream(
					new FileInputStream(tripByMonthFolder + trainMonths[0] + ".bneibors")));
			
			boolean EOF = false;
			
			HashMap<Short, Double> s1 = tr.get(Raster.ALL).absSpeedRef;
			HashMap<Short, Integer> s2 = tr.get(Raster.ALL).absRefCnt;
			System.out.format("s1: %d, s2: %d\n", s1.size(), s2.size());
			
			try {
				while (!EOF) {
					SimpleTrip q = new SimpleTrip(test);
					double refq = s1.get(q.htyIdx) / s2.get(q.htyIdx);
					int nn = train.readShort();
					boolean SELECT = nn >= NL_THRESHOLD && nn < NH_THRESHOLD;
					if (SELECT)
						System.out.print(q.t);
					ArrayList<Float> rawT = new ArrayList<>();
					for (int i = 0; i < nn; i++) {
						NeededNeighborInfo nni = new NeededNeighborInfo(train);
						if (SELECT) {
							if (!s1.containsKey(nni.htyIdx)) {
								System.err.format("nn: %d, idx: %d\n", nn, nni.htyIdx);
								continue;
							}
							rawT.add(nni.time);
							double refi = s1.get(nni.htyIdx) / s2.get(nni.htyIdx);
							System.out.format(",%g", nni.time * refi / refq);
						}
					}
					if (SELECT) {
						System.out.print(";");
						for (int i = 0; i < rawT.size(); i++) {
							if (i == 0)
								System.out.format("%g", rawT.get(i));
							else
								System.out.format(",%g", rawT.get(i));
						}
						System.out.print("\n");
					}
				}
			} catch (IOException e) {
				EOF = true;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Verify 
	 * @param NL_THRESHOLD
	 * @param NH_THRESHOLD
	 */
	public static void verify_constantSpeedRefRatio(int NL_THRESHOLD, int NH_THRESHOLD) {
		try {
			retrieveSpeedReference();
			DataInputStream test = new DataInputStream(new BufferedInputStream(
					new FileInputStream(tripByMonthFolder + testMonth + ".btrip")));
			DataInputStream train = new DataInputStream(new BufferedInputStream(
					new FileInputStream(tripByMonthFolder + trainMonths[0] + ".bneibors")));
			
			boolean EOF = false;
			
			HashMap<Short, Double> s1 = tr.get(Raster.ALL).absSpeedRef;
			HashMap<Short, Integer> s2 = tr.get(Raster.ALL).absRefCnt;
			System.out.format("s1: %d, s2: %d\n", s1.size(), s2.size());
			
			try {
				while (!EOF) {
					SimpleTrip q = new SimpleTrip(test);
					HashMap<Short, Double> s3 = tr.get(q.ntaID).absSpeedRef;
					HashMap<Short, Integer> s4 = tr.get(q.ntaID).absRefCnt;
					
					double refq = s1.get(q.htyIdx) / s2.get(q.htyIdx);
					double vq = q.dist / q.t;
					double refRq = 0;
					if (s3.containsKey(q.htyIdx))
						refRq = s3.get(q.htyIdx) / s4.get(q.htyIdx);
					else
						refRq = refq;
					
					int nn = train.readShort();
					boolean SELECT = vq >= 5.0 / 3600 && vq <= 30.0 / 3600 && nn >= NL_THRESHOLD && nn < NH_THRESHOLD;
					for (int i = 0; i < nn; i++) {
						NeededNeighborInfo nni = new NeededNeighborInfo(train);
						double vi = nni.distance / nni.time;
						double refi = s1.get(nni.htyIdx) / s2.get(nni.htyIdx);
						double vr = vi / vq;
						double Vr = refi / refq;
						double qr = q.t / nni.time;
					
						double refRi = 0;
						if (s3.containsKey(nni.htyIdx))
							refRi = s3.get(nni.htyIdx) / s4.get(nni.htyIdx);
						else
							refRi = refi;
						double VRr = refRi / refRq;
						
						if (SELECT && vi <= 30.0 / 3600 && vi >= 5.0 / 3600 && vr <= 3 && qr <= 3 && VRr <= 3 && rnd.nextDouble() <= 0.00003) {
							System.out.format("%g,%g,%g,%g,%g\n", qr, vr, 
									Vr, q.dist / nni.distance, VRr );
						}
					}
				}
			} catch (IOException e) {
				EOF = true;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	///========================================================================
	///			Main function and various helper class
	///========================================================================
	
	/**
	 * The main function entrance
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length < 1) {
			System.out.printf("Possible arguments: smallScale | extractNeighbor | evaluate | "
					+ "generateReference"
					+ "| significanceTest | reference | evaluateSH | targetAreaTrafficVolume"
					+ "tripPerDay | temporalScaler | refRatio");
			return;
		}
		String cmd = args[0];
		System.out.println(cmd);
		
		if (cmd.equals("smallScale")) {
			countNumNeighbors();
			smallScaleEvaluation();
		} else if (cmd.equals("extractNeighbor"))
			extractNeighborByMonth( Integer.parseInt(args[1]) );	// don't run this again, because it takes 20 hours
		else if (cmd.equals("evaluate")) {
			for (double s = 0.01; s < 0.1; s += 0.02) {
				generateSpeedReference( 1.1 );
				retrieveSpeedReference();
				learn_ARIMA_parameter();
				retrieve_ARIMA_parameter();
				evaluateByMonth( 11, 1.1 );
			}
		} else if (cmd.equals("generateReference")) 
			generateSpeedReference();
		else if (cmd.equals("significanceTest")) {
			assumption_significance_test();
		} else if (cmd.equals("reference")) {
//			LR_train();
			writeReadableSpeedReference(args[1]);
		} else if (cmd.equals("evaluateSH")) {
			// evaluate on the Shanghai taxi data
			evaluate_SH_taxi();
		} else if (cmd.equals("targetAreaTrafficVolume"))
			targetAreaTrafficVolume();
		else if (cmd.equals("tripPerDay"))
			tripPerDay();
		else if (cmd.equals("temporalScaler"))
			verify_temporalScalingFactor(Integer.parseInt(args[1]), Integer.parseInt(args[2]));
		else if (cmd.equals("refRatio"))
			verify_constantSpeedRefRatio(Integer.parseInt(args[1]), Integer.parseInt(args[2]));
		else
			System.out.println("Wrong command!");
	}
}



///==================================================================
///			Multi-thread for extractNeighborByMonth() function 
///==================================================================

/**
 * Thread class that extract the neighbors of given trips.
 * 
 * Input - a shared buffer from the Reader Thread
 * Output - a shared buffer to the Writer Thread
 * @author Hongjian
 *
 */
class TripExtractor extends Thread {
	public static AtomicInteger counter = new AtomicInteger(0);
	public static LinkedBlockingQueue<BuildTripTask> bq = new LinkedBlockingQueue<>(3000);
	public static final ReentrantLock lock = new ReentrantLock();
	public static AtomicInteger NumClosedExtractor = new AtomicInteger(0);
	
	private int id;
	private Raster r;

	
	public TripExtractor( int i, Raster r ) {
		this.id = i;
		this.r = r;
	}
	
	public void run() {
		while (true) {
			BuildTripTask task = null;
			try {
				task = bq.take();
				
				// poison pillar
				if (task.cnt == -1 && task.t == null)
					break;
				
				lock.lock();
				Trip trip = task.t;
				r.reverseIndexTrip(trip);
				lock.unlock();
				
				// find its similar trips
				HashSet<Grid> src = r.getGridNeighbors(trip.pikGrid, Raster.GRID_LEVEL);
				HashSet<Grid> dst = r.getGridNeighbors(trip.drpGrid, Raster.GRID_LEVEL);
				HashSet<Trip> ts = new HashSet<>();
				for (Grid g : src) {
					for (Trip tr : g.pickup) {
						if ( dst.contains(tr.drpGrid)) {
							ts.add(tr);
						}				
					}
				}
				
				LinkedList<NeededNeighborInfo> neighborInfos = new LinkedList<>();
				for (Trip tn: ts)
					neighborInfos.add(new NeededNeighborInfo(tn));
				
				// feed to the finalizer
				WriteNeighborTask ntsk = new WriteNeighborTask(task.cnt, neighborInfos);
				TripFinalizer.opbuffer.put(ntsk);
				
				counter.incrementAndGet();
				if (counter.get() % 500000 == 0)
					System.out.printf("Thread ID: %d -- # testing trips: %d -- input buffer size %d\n", this.id, counter.get(), bq.size());

			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.printf("Thread %d closed, process %d trips in total.\n", this.id, counter.get());
		try {
			if (NumClosedExtractor.incrementAndGet() == Raster.EXTRACTOR_THREAD_NUM)
				TripFinalizer.opbuffer.put(new WriteNeighborTask(-1, null));
			else
				bq.put(new BuildTripTask(-1, null));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}



/**
 * Assistant class used by {@link TripExtractor}.
 * @author hxw186
 *
 * The <em>cnt</em> tracks the Trip creating order.
 * When {@link TripFinalizer} outputs the list of neighbors, 
 * the same order is guaranteed. 
 */
class BuildTripTask implements Comparable<BuildTripTask>{
	public int cnt;
	public Trip t;
	
	public BuildTripTask(int c, Trip t) {
		this.cnt = c;
		this.t = t;
	}

	
	@Override
	public int compareTo(BuildTripTask o) {
		return this.cnt - o.cnt;
	}
}



/**
 * Writer thread that writes the list of neighbors to file.
 * @author hxw186
 *
 */
class TripFinalizer extends Thread {
	public static LinkedBlockingQueue<WriteNeighborTask> opbuffer = new LinkedBlockingQueue<>(3000);
	

	private DataOutputStream bw;
	private int counter;
	private PriorityQueue<WriteNeighborTask> pq;
	
	public TripFinalizer(DataOutputStream bw) {
		this.bw = bw;
		counter = 0;
		pq = new PriorityQueue<>();
	}
	
	/**
	 * Write out the first trip in the Queue. It is guaranteed to be the next to output.
	 */
	private void writeHeadTrip() {
		WriteNeighborTask curtsk = pq.remove();
		try {
			bw.writeShort(curtsk.neighInfos.size());
			for (NeededNeighborInfo ni : curtsk.neighInfos)
				ni.writeToBinaryFile(bw);
			counter ++;
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (counter % 500000 == 0)
			System.out.printf("Write # testing trips: %d -- output buffer size %d\n", counter, pq.size());
	}
	
	
	public void run() {
		while (true) { // the producer is not done or the opbuffer is not empty

			WriteNeighborTask tsk;
			try {
				tsk = opbuffer.take();
				// Poison pillar
				if (tsk.cnt == -1 && tsk.neighInfos == null)
					break;
				
				pq.add(tsk);
				
				if (!pq.isEmpty() && pq.peek().cnt == counter) {
					writeHeadTrip();
				}
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		}
		
		// finish off what's left
		while (!pq.isEmpty()) {
			if (pq.peek().cnt == counter) {
				writeHeadTrip();
			}
		}
		System.out.printf("Writing thread closed, write %d trips in total.\n", counter);
	}
}


/**
 * Assistant class used by {@link TripFinalizer}.
 * @author hxw186
 *
 */
class WriteNeighborTask implements Comparable<WriteNeighborTask> {
	public int cnt;
	public LinkedList<NeededNeighborInfo> neighInfos;
	
	public WriteNeighborTask(int c, LinkedList<NeededNeighborInfo> neighInfos) {
		this.cnt = c;
		this.neighInfos = neighInfos;
	}
	
	
	@Override
	public int compareTo(WriteNeighborTask o) {
		return this.cnt - o.cnt;
	}
}






///==================================================================
///			Multi-thread for evaluateByMonth() function 
///==================================================================

class EvaluatorTask {
	SimpleTrip trip;
	LinkedList<NeededNeighborInfo> neighbors;
	
	public EvaluatorTask(SimpleTrip t, LinkedList<NeededNeighborInfo> ns) {
		trip = t;
		neighbors = ns;
	}
}

class SimpleTripEvaluator extends Thread {
	
	public static LinkedBlockingQueue<EvaluatorTask> lbq = new LinkedBlockingQueue<>(3000);
	public static AtomicInteger counter = new AtomicInteger(0);
	public static AtomicInteger NumClosedEvaluator = new AtomicInteger(0);
	
//	public static HashSet<Integer> testIds = SHTrip.getIDSet("../test");
	
	private int id;
	
	public SimpleTripEvaluator(int i) {
		id = i;
		System.out.printf("Thread SimpleTripEvaluator %d starts\n", id);
	}
	
	public void run() {
		while ( true ) {
			EvaluatorTask t;
			try {
				t = lbq.take();
				// poison pillar -- stopping the current thread
				if (t.trip == null && t.neighbors == null)
					break;
				
//				if (testIds.contains(t.trip.id))
					// skip the not-testable trips
					//SimpleTripEvaluationWriter.outbuffer.put(String.format("%g\t-2\t-2\t-2\t-2\t-2\t-2\t-2\t-2\t0\t%g\t%d\n", t.trip.t, t.trip.dist, t.trip.id));
				{
					try {
						double[] prd = t.trip.estimateT(t.neighbors);
						// output
						if (prd[8] > 0)
							SimpleTripEvaluationWriter.outbuffer.put(String.format("%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%g\t%d\t%g\t%d\n", t.trip.t, prd[0], prd[1], prd[2], prd[3], prd[4], prd[5], prd[6], prd[7],(int)(prd[8]), t.trip.dist, t.trip.id));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				int c = counter.incrementAndGet();
				if (c % 1000000 == 0)
					System.out.printf("Process %d testing in thread %d\n", c, id);
			} catch (InterruptedException e1) {
				System.err.printf("lbq.take() failed in SimpleTripEvaluator %d.", id);
				e1.printStackTrace();
			}			
		}
		
		System.out.printf("Thread %d closed -- %d trips processed\n", id, counter.get());
		if( NumClosedEvaluator.incrementAndGet() == Raster.EXTRACTOR_THREAD_NUM )
			try {
				SimpleTripEvaluationWriter.outbuffer.put("quit");
			} catch (InterruptedException e) {
				System.err.printf("outbuffer.put(quit) failed in SimpleTripEvaluator %d.\n", id);
				e.printStackTrace();
			}
		else
			try {
				lbq.put(new EvaluatorTask(null, null));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	}
}




class SimpleTripEvaluationWriter extends Thread {
	
	public static LinkedBlockingQueue<String> outbuffer = new LinkedBlockingQueue<>(3000);
	
	private BufferedWriter bw;
	private int cnt;
	
	public SimpleTripEvaluationWriter(BufferedWriter bw) {
		this.bw = bw;
		cnt = 0;
		System.out.println("Thread SimpleTripEvaluationWriter starts.");
	}
	
	public void run() {
		while (true) {
			String l;
			try {
				l = outbuffer.take();
				// poison pillar 
				if (l.equals("quit"))
					break;
				
				try {
					this.bw.write(l);
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (cnt ++ % 1000000 == 0)
					System.out.printf("Write %d testing results\n", cnt);
			} catch (InterruptedException e1) {
				System.err.println("outbuffer.take() failed in SimpleTripEvaluationWriter.");
				e1.printStackTrace();
			}
		}
		System.out.printf("Output all testing results %d\n", cnt);			
	}
}

