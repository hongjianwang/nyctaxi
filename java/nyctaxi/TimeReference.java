package nyctaxi;


import java.io.*;
import java.util.*;



/**
 * Build the temporal reference with regard to different time slots.
 * 
 * We use the average speed as a measure. We create an n-by-1 vector as the time reference
 * for n time slots in one period (a day or a week).
 * @author Hongjian
 *
 */
public class TimeReference {
	
	/* ===========================================
	 * Variables for relative time reference
	 * ===========================================
	 */
	/**
	 * number of time slots
	 */
	public static int nTimeSlots = Trip.nTimeSlots;
	
	
	/**
	 * the actual reference (speed)
	 */
	double[] relaRefSum;
	int[] relaRefCnt;
	ArrayList<LinkedList<Double>> relas; 
	
	
	boolean ENOUGH_SAMPLE;
	int total_trip;
	
	/* ===========================================
	 * Variables for absolute time reference
	 * ===========================================
	 */
	
	HashMap<Short, Double> absSpeedRef;
	HashMap<Short, Integer> absRefCnt;
	
	/**
	 * Constructor of timeReference
	 * 
	 * The number of time slots is determined in the {@link Trip} class.
	 */
	public TimeReference() {
		// initialize relative reference
		relaRefSum = new double[nTimeSlots];
		relaRefCnt = new int[nTimeSlots];
		relas = new ArrayList<>();
		for (int i = 0; i < 24 * 7; i++) {
			relas.add(new LinkedList<Double>());
		}
		
		
		// initialize absolute reference
		absSpeedRef = new HashMap<>();
		absRefCnt = new HashMap<>();
		
		ENOUGH_SAMPLE = true;
		total_trip = 0;
	}

	
	
	/**
	 * Calculate the temporal reference given the complete set of trips.
	 * 
	 * This function should not be directly called on large data set, cause it's memory consuming.
	 * 
	 * @param trips the complete set of trips
	 * @return the reference vector
	 */
	public double[] calculateReferenceFromAllTrips( LinkedList<Trip> trips ) {
		double[] relaSpeedRef = new double[nTimeSlots];;
		for (Trip t : trips) {
			relaRefSum[t.htwIdx] += t.v;
			relaRefCnt[t.htwIdx] += 1;
		}
		
		for (int i = 0; i < relaSpeedRef.length; i++)
			relaSpeedRef[i] = relaRefSum[i] / relaRefCnt[i];
		return relaSpeedRef;
	}
	
	
	/**
	 * update the relative and absolute reference by one trip
	 * @param t given trip
	 */
	public void updateReferenceByOneTrip( Trip t ) {
		// update relative reference
		relaRefSum[t.htwIdx] += t.v;
		relaRefCnt[t.htwIdx] += 1;
		relas.get(t.htwIdx).add((double) t.v);
		
		// update absolute reference
		if (absSpeedRef.containsKey(t.htyIdx)) {
			absSpeedRef.put(t.htyIdx, absSpeedRef.get(t.htyIdx) + t.v);
			absRefCnt.put(t.htyIdx, absRefCnt.get(t.htyIdx) + 1);
		} else {
			absSpeedRef.put(t.htyIdx, (double) t.v);
			absRefCnt.put(t.htyIdx, 1);
			//System.out.println(absSpeedRef.size());
		}
	}
	
	
	public void initialize_statisticsFields() {
		ENOUGH_SAMPLE = true;
		total_trip = 0;
		for (short i : absRefCnt.keySet()) {
			if (absRefCnt.get(i) < SimpleTrip.NUM_NEIGHBOR_REGION)
				ENOUGH_SAMPLE = false;
			total_trip += absRefCnt.get(i);
		}
		
		if (absRefCnt.size() < 8750)
			ENOUGH_SAMPLE = false;
	}
	
	
	/**
	 * Generate the time series of absolute reference as ARIMA training input.
	 * 
	 * For each region, we output the absolute speed reference in a separate file. Thus, we can
	 * train ARIMA model on this time series.
	 * @param fn - the region pair ID, denotes different pair of regions
	 * @return two integers. The first is a indicator of whether enough samples are collected.
	 * 		The second is the total number of trips in current region.
	 */
	public int[] generateRegionBasedAbsRef_learnARIMA(short fn) {
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(Raster.tripByMonthFolder + 
				"ARIMA/" + Short.toString(fn) ))) {
			
			initialize_statisticsFields();

			for (short i = 0; i <= 8759; i++) {
				double ref = 0;
				// if the current region has sufficient total number of trips,
				// and the current time slot i has trips,
				// and the number of trips in current time slot is larger than 5.
				// Then we trust the current reference.
				if ( ENOUGH_SAMPLE ) {
					ref = absSpeedRef.get(i) / absRefCnt.get(i);
					bw.write(String.format("%d,%g\n",i, ref));
				} else {
					if (Raster.tr.get(Raster.ALL).absSpeedRef.containsKey(i)) {
						ref = Raster.tr.get(Raster.ALL).absSpeedRef.get(i) 
								/ Raster.tr.get(Raster.ALL).absRefCnt.get(i);
						bw.write(String.format("%d,%g\n",i, ref));
					} else {
						bw.write(String.format("%d,NA\n", i));
						System.err.printf("%d does not exist\n", i);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			// run external Rscript to get parameters
			// the results will be written in ../R/arima_parameters
//			String cmd = String.format("%s %s %s", "Rscript", "../R/ARIMA_train.R", fn);
//			System.out.println(cmd);
			ProcessBuilder pb = new ProcessBuilder().inheritIO().command("Rscript", "../R/ARIMA_train.R", 
					Raster.tripByMonthFolder + "ARIMA/", Short.toString(fn));	// 
			Process p = pb.start();
			p.waitFor();
//			System.out.println("External process finished with id " + fn);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new int[]{ ENOUGH_SAMPLE ? 1 : 0, total_trip};
	}
	
	
	/**
	 * Write the current Time reference to external file.
	 * 
	 * First output the relative reference 24x7
	 * Second output the absolute reference with a sparse representation
	 * @param key	the region key of current TimeReference
	 * @param bw	the external file
	 * @throws Exception
	 */
	public void writeToFile(Short key, BufferedWriter bw) throws Exception {
		// relative reference
		bw.write(Short.toString(key) + "\t");
		for (int h = 0; h < Trip.nTimeSlots; h++) {
			double mean = this.relaRefSum[h] / this.relaRefCnt[h];
			double variance = 0;
			for (double s : this.relas.get(h)) {
				variance += (s - mean) * (s - mean);
			}
			variance /= this.relaRefCnt[h];
			bw.write(String.format("%d,%g,%g\t", this.relaRefCnt[h], this.relaRefSum[h], variance));
		}
		// absolute reference
		bw.write("\n" +  key + "\t");
		for (short htyIdx : this.absRefCnt.keySet()) {
			bw.write(String.format("%d:%d,%g\t", htyIdx, this.absRefCnt.get(htyIdx), this.absSpeedRef.get(htyIdx)));
		}
		bw.write("\n");
	}
	
	
	
	/**
	 * Experimental functions: compare the time reference of different days.
	 * 
	 * The results should support the argument <b>the absolute reference is more preferable than the relative reference</b>.
	 */
	public static void CompareReferences() {
		String[] fnames = {"2013-02-03", "2013-02-04", "2013-02-05", "2013-02-06", "2013-02-07", "2013-02-08", "2013-02-09", 
				"2013-02-10", "2013-02-11", "2013-02-12", "2013-02-13", "2013-02-14", "2013-02-15", "2013-02-16"};
		double[][] refs = new double[fnames.length][];
		
		for (int i = 0; i < fnames.length; i++) {
			LinkedList<Trip> ts = Trip.retrieveAllTrips(fnames[i] + ".trip");
			TimeReference tr = new TimeReference();
			refs[i] = tr.calculateReferenceFromAllTrips(ts);
		}
		
		try (BufferedWriter bw = new BufferedWriter( new FileWriter("ref_comp"))) {
			for (int i = 0; i < refs.length; i++) {
				for (int j = 0; j < refs[i].length; j++) {
					bw.write(Double.toString(refs[i][j]) + '\t');
				}
				bw.write("\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	

	
	public static void main(String[] argv) {
		CompareReferences();
	}
}
