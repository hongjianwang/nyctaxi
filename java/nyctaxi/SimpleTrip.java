package nyctaxi;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.io.*;
import java.nio.ByteBuffer;


/**
 * This SimpleTrip is similar to class {@link Trip}. The purpose is fast evaluation given its neighbors.
 * 
 * We build SimpleTrip from the concise binary representation of trips.
 * 
 * @author Hongjian
 *
 */
public class SimpleTrip {
	
	protected static final int nTimeSlots = Trip.nTimeSlots;
	protected static final SimpleDateFormat sdf = Trip.sdf;
	protected static Calendar calen = Calendar.getInstance();
	public static final int NUM_NEIGHBOR_REGION = 5;
	public static final int RELA_NUM_NEIGHBOR = 5;
	public static final int ABS_NUM_NEIGHBOR = 5;
	
	/**
	 * ID of the trips
	 */
	protected int id;
	
	/**
	 * Travel time
	 */
	protected float t;
	
	/**
	 * Travel distance
	 */
	protected float dist;
	
	/**
	 * Index trip by the relative hour of a week
	 */
	protected short htwIdx;
	
	/**
	 * Index trip by the absolute hour of a year
	 */
	protected short htyIdx;
	
	/**
	 * Index of NTA pair -- source to destination NTA
	 * 
	 * source/destination are stored in a byte. we combine them to get this short field.
	 */
	protected short ntaID;
	
	
	public SimpleTrip() {
		// deliberately empty, inheritance purpose
	}
	
	/**
	 * Constructor
	 * @param id
	 * @param t
	 * @param dist
	 * @param htyIdx
	 * @param pickup
	 * @param dropoff
	 */
	public SimpleTrip(int id, float t, float dist, short htyIdx, byte pickup, byte dropoff) {
		this.id = id;
		this.t = t;
		this.dist = dist;
		this.htyIdx  = htyIdx;
		this.htwIdx = (short) ((htyIdx + 24 * 2) % (24 * 7));	// first day in 2013 is Tuesday, our week starts from Sunday6
		this.ntaID = ByteBuffer.wrap(new byte[] {pickup, dropoff}).getShort();
	}
	
	/**
	 * Construct from the binary file.
	 * 
	 * Input field orders are as follows:
	 * 1. trip ID		(int)
	 * 2. travel time	(float)
	 * 3. travel distance (float)
	 * 4. hour to year index (short)
	 * 5. pick up grid ID (byte)
	 * 6. drop off grid ID (byte)
	 * @param fin
	 * @throws EOFException
	 */
	public SimpleTrip(DataInputStream fin) throws Exception {
		this.id = fin.readInt();
		// skip the coordinates (4 doubles)
		for (int i = 0; i < 4; i++)
			fin.readDouble();
		this.t = fin.readFloat();
		this.dist = fin.readFloat();
		this.htyIdx = fin.readShort();
		this.htwIdx = (short) ((htyIdx + 24 * 2 ) % ( 24 * 7 ));
		this.ntaID = fin.readShort();
	}
	
	
	/**
	 * Estimate the travel time by current trips' neighbor.
	 * 
	 * The input is one line from the results of {@link nyctaxi.Raster#extractNeighborByMonth()}.
	 * 
	 * @param neighbor a string of neighbors with their 
	 * @return
	 * @throws Exception 
	 */
	public double[] estimateT(LinkedList<NeededNeighborInfo> neighbor) throws Exception{
		
		double Tavg = 0;
		int cnt = 0;
		double SrelaW = 0;
		double SrelaW_r = 0;
		double SabsW = 0;
		double SabsW_R = 0;
		double SabsW_R_ARIMA = 0;
		double Sum_dist =0;
		
		// initialize the reference to the global time reference
		double[] relaSpeedRef = Raster.tr.get(Raster.ALL).relaRefSum;
		int[] relaRefCnt = Raster.tr.get(Raster.ALL).relaRefCnt;
		HashMap<Short, Integer> absRefCnt = Raster.tr.get(Raster.ALL).absRefCnt;
		HashMap<Short, Double> absSpeedRef = Raster.tr.get(Raster.ALL).absSpeedRef;
		// reference to region-based speed reference
		double[] relaSpeedRef_r = Raster.tr.get(this.ntaID).relaRefSum;
		int[] relaRefCnt_r = Raster.tr.get(this.ntaID).relaRefCnt;
		// ========== relative reference of current Trip ========== 
		double rela_vref = relaSpeedRef[this.htwIdx] / relaRefCnt[this.htwIdx];
		double rela_R_vref = 0;
		if (relaRefCnt_r[this.htwIdx] >= RELA_NUM_NEIGHBOR)
			rela_R_vref = relaSpeedRef_r[this.htwIdx] / relaRefCnt_r[this.htwIdx];
		else
			rela_R_vref = rela_vref;
		
		
		// ========== ARIMA estimate the absolute reference of current Trip ========== 
		short[] idxes = {(short) (this.htyIdx -1), (short) (this.htyIdx - 2), 
				(short) (this.htyIdx - 168), (short) (this.htyIdx - 168 -1), (short) (this.htyIdx - 168 - 2)};
		double[] vref = new double[5];
		for (int i = 0; i < vref.length; i++) {
			if (absSpeedRef.containsKey(idxes[i])) {
				vref[i] = absSpeedRef.get(idxes[i]) / absRefCnt.get(idxes[i]);
			} else {
				// the trip is too early, we cannot get its previous estimation
				// System.out.println(idxes[i]);
				return new double[]{-1, -1, -1, -1, -1, -1, -1, -1, 0};
			}
		}
		double ar1 = Raster.ARIMA_paras.get(Raster.ALL)[0];
		double ar2 = Raster.ARIMA_paras.get(Raster.ALL)[1];
		double arima_vref = ar1 * (vref[0] - vref[3]) + ar2 * (vref[1] - vref[4]) + vref[2];
		
		// select region-based reference, estimate by ARIMA
		double[] vref_r = new double[5];
		boolean use_global_ARIMA = false;
		for (int i = 0; i < idxes.length; i++) {
			if (!Raster.tr.get(this.ntaID).ENOUGH_SAMPLE || !Raster.tr.get(this.ntaID).absSpeedRef.containsKey(idxes[i]) || Raster.tr.get(this.ntaID).absRefCnt.get(idxes[i]) < NUM_NEIGHBOR_REGION) {
				use_global_ARIMA = true;
				break;
			} else {
				vref_r[i] = Raster.tr.get(this.ntaID).absSpeedRef.get(idxes[i]) / Raster.tr.get(this.ntaID).absRefCnt.get(idxes[i]);
			}
		
				
//			if (Raster.tr.get(this.ntaID).ENOUGH_SAMPLE) {
//				vref_r[i] = Raster.tr.get(this.ntaID).absSpeedRef.get(idxes[i]) / Raster.tr.get(this.ntaID).absRefCnt.get(idxes[i]);
//			} else {
//				vref_r[i] = absSpeedRef.get(idxes[i]) / absRefCnt.get(idxes[i]);
//			}
		}
		double ar1_r = Raster.ARIMA_paras.get(this.ntaID)[0];
		double ar2_r = Raster.ARIMA_paras.get(this.ntaID)[1];
		double arima_vref_r = 0; 
		if (use_global_ARIMA)
			arima_vref_r = arima_vref;
		else
			arima_vref_r = ar1_r * (vref_r[0] - vref_r[3]) + ar2_r * (vref_r[1] - vref_r[4]) + vref_r[2];
		
		// ========== absolute reference of current Trip ========== 
		double abs_vref = 0;
		if (absSpeedRef.containsKey(this.htyIdx) && absRefCnt.get(this.htyIdx) >= ABS_NUM_NEIGHBOR)
			abs_vref = absSpeedRef.get(this.htyIdx) / absRefCnt.get(this.htyIdx);
		else {
			System.out.println(this.htyIdx);
			return new double[] {-1, -1, -1, -1, -1, -1, -1, -1, 0};
		}

		// ========== region-based absolute reference of current Trip ==========
		double abs_R_vref = 0;
		boolean use_global_ORACLE = false;
		double[] res = getLocalAbsRef(this.htyIdx, false);
		abs_R_vref = res[0];
		use_global_ORACLE = res[1] > 0 ? true : false;
		
		
		
		
		for (NeededNeighborInfo neigh : neighbor) {
				short hty = neigh.htyIdx;
				int htw = get_hourIdx(hty);
				double travelTime = neigh.time;
				double travelDist = neigh.distance;
				// average
				Tavg += travelTime;
				// global relative weight
				SrelaW += travelTime * relaSpeedRef[htw] / relaRefCnt[htw];
				if (relaRefCnt_r[htw] >= RELA_NUM_NEIGHBOR)
					SrelaW_r += travelTime * relaSpeedRef_r[htw] / relaRefCnt_r[htw];
				else
					SrelaW_r += travelTime * relaSpeedRef[htw] / relaRefCnt[htw];
				// global absolute weight
				if (!absSpeedRef.containsKey(hty)) {
					return new double[] {-1, -1, -1, -1, -1, -1, -1, -1, 0};
				}
				SabsW += travelTime * absSpeedRef.get(hty) / absRefCnt.get(hty);
				// local absolute weight
				SabsW_R += travelTime * getLocalAbsRef(hty, use_global_ORACLE)[0];
				SabsW_R_ARIMA += travelTime * getLocalAbsRef(hty, use_global_ARIMA)[0];
				// count total neighbor
				cnt += 1;
				// use distance / vref
				Sum_dist += travelDist;
		}
		
//		System.out.printf("cnt: %d\tSrelaW: %g\trela_vref: %g\n", cnt, SrelaW, rela_vref);
		double[] estimation = new double[9];
		estimation[0] = Tavg / cnt;					// use average
		estimation[1] = SrelaW / cnt / rela_vref;	// use relative reference
		estimation[2] = SrelaW_r / cnt / rela_R_vref;	// use relative reference + region
		estimation[3] = SabsW / cnt / abs_vref;		// use absolute reference (oracle)
		estimation[4] = SabsW / cnt / arima_vref;	// use absolute reference (ARIMA guess)
		estimation[5] = SabsW_R / cnt / abs_R_vref;	// use absolute reference (oracle) + regional difference
		estimation[6] = SabsW_R_ARIMA / cnt / arima_vref_r; // use absolute reference (ARIMA guess) + region
		estimation[7] = Sum_dist / cnt / abs_vref * 3600;	// use distance / vref instead of time * v / vref
//		String[] ls = ntaID.split(",");
//		estimation[8] = Integer.parseInt(ls[0]) * 100 + Integer.parseInt(ls[1]);	// number of neighbors
		estimation[8] = cnt;
		return estimation;
	}
	
	
	public static short get_hourIdx(int hty) {
		return (short) ((hty + 24 * 2) % (24 * 7));	// first day in 2013 is Tuesday, our week starts from Sunday
	}
	
	
	/**
	 * Get the NTA-based absolute speed reference
	 * 
	 * If does not exist or not significant, use the global absolute speed reference instead.
	 * @return the local absolute speed reference
	 */
	protected double[] getLocalAbsRef(short hty, boolean useG) {
		HashMap<Short, Integer> glb_absRefCnt = Raster.tr.get(Raster.ALL).absRefCnt;
		HashMap<Short, Double> glb_absSpeedRef = Raster.tr.get(Raster.ALL).absSpeedRef;
		if (useG == false) {
			HashMap<Short, Integer> loc_absRefCnt = null;
			HashMap<Short, Double> loc_absSpeedRef = null;
			
			// get the global reference, if the local one does not exist or has a small support		
			if (Raster.tr.containsKey(ntaID)) {
				loc_absRefCnt = Raster.tr.get(ntaID).absRefCnt;
				loc_absSpeedRef = Raster.tr.get(ntaID).absSpeedRef;
				
				// match the condition of ARIMA parameter learning.
	//			if (Raster.tr.get(ntaID).ENOUGH_SAMPLE)
				if (loc_absRefCnt.containsKey(hty) && loc_absRefCnt.get(hty) >= NUM_NEIGHBOR_REGION) 
					return new double[] { loc_absSpeedRef.get(hty) / loc_absRefCnt.get(hty), 0};
			} 
		}
		return new double[] { glb_absSpeedRef.get(hty) / glb_absRefCnt.get(hty), 1};
	}
	
	/**
	 * Test various assumptions.
	 * 
	 * Use time reference:
	 * Test the assumption 1: the ratio of travel time equals to the ratio of average travel time
	 * Test the assumption 2: neighbors' distance equal to current trip's
	 * Test the assumption 5: the current trip's travel time equals to the average travel time
	 * 
	 * Use Speed reference:
	 * Test the assumption 3: the ratio of travel speed equals to the ratio of average travel speed.
	 * Test the assumption 2: neighbors' distance equal to current trip's
	 * Test the assumption 4: the current trip's travel speed equals to the average speed.
	 * 
	 * @param neighs
	 * @return
	 * 
	 */
	protected LinkedList<double[]> generate_pairVariables_for_assumption(LinkedList<NeededNeighborInfo> neighs) {
		LinkedList<double[]> res = new LinkedList<>();
		
		double v = this.dist / this.t * 3600;
		HashMap<Short, Integer> absRefCnt = Raster.tr.get(Raster.ALL).absRefCnt;
		HashMap<Short, Double> absSpeedRef = Raster.tr.get(Raster.ALL).absSpeedRef;
		if (absRefCnt == null)
			System.out.println("absRefCnt is null");
		if (absSpeedRef == null)
			System.out.println("absSpeedRef is null");
		
		double vref = absSpeedRef.get(this.htyIdx) / absRefCnt.get(this.htyIdx);
		for (NeededNeighborInfo neig : neighs) {
			short hty = neig.htyIdx;
			double t = neig.time;
			double dist = neig.distance;
			double vn = dist / t * 3600;
			double vnref = absSpeedRef.get(hty) / absRefCnt.get(hty);
			double[] pairs = new double[] { v/ vn, vref/vnref, this.dist, dist, v, vref };
			res.add(pairs);
		}
		
		return res;
	}
}



/**
 * In the intermediate *.bneibors file, we only need
 * the neighboring trips' travel time, hour to year index, and the distance.
 * @author hxw186
 *
 */
class NeededNeighborInfo {
	short htyIdx;
	float distance;
	float time;
	
	public NeededNeighborInfo( Trip t ) {
		htyIdx = t.htyIdx;
		distance = t.dist;
		time = t.t;
	}
	
	
	public NeededNeighborInfo(DataInputStream din) throws IOException {
		htyIdx = din.readShort();
		distance = din.readFloat();
		time = din.readFloat();
	}
	
	
	public void writeToBinaryFile(DataOutputStream dout) throws IOException {
		dout.writeShort(htyIdx);
		dout.writeFloat(distance);
		dout.writeFloat(time);
	}
}
