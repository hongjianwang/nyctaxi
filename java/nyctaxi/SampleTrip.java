package nyctaxi;


import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashSet;
import java.io.*;

/**
 * Pre-process the raw data.
 * 
 * 1) Divide the trips into each month by {@link #sampleByDate()} function, so that we can evaluate month-by-month in memory.
 * 2) Split trip files and store them in binary form by {@link #sampleAndCompressTrip()} function. This helps save space and time.
 * @author Hongjian
 *
 */
public class SampleTrip {
	
	static String forg = Raster.rawFile;
	static String outputDir = Raster.tripByMonthFolder;
	

	/**
	 * Merge the results of two linear regression filters.
	 * 
	 * Preprocess step 1 - get non-outliers.
	 * 
	 * The first filter use the regression of actual distance and L1 distance.
	 * The second filter use the regression of log actual time and log L1 distance.
	 * 
	 * We merge the non-outlier results by ID.
	 */
	static public void mergeFilteredTrip() {
		String f1 = "../../../dataset/sortti_new_manh_dist_l1_info_id_nonoutliers";
		String f2 = "../../../dataset/sortti_new_manh_logtime_logl1_info_id_nonoutliers";
		String fout = "../../../dataset/merged_nonoutliers_manh_id";
		
		try (BufferedReader br = new BufferedReader(new FileReader(f1));
				BufferedReader br2 = new BufferedReader(new FileReader(f2));
				BufferedWriter bw = new BufferedWriter( new FileWriter(fout))) {
			// map
			HashSet<Integer> ids = new HashSet<>();
			int cnt = 0;
			
			// go through the first file construct index
			long t1 = System.currentTimeMillis();
			System.out.println("Construct dictionary");
			String l1 = null;
			while((l1 = br.readLine()) != null) {
				String[] segs = l1.split("[,\t]");
				int id = Integer.parseInt(segs[12]);	// the trip ID is segs[12], which is the 13th filed
				ids.add(id);
				if (cnt++ % 1000000 == 0)
					System.out.println(cnt);
			}
			long t2 = System.currentTimeMillis();
			System.out.printf("Construct dictionary in %d seconds.\n", (t2-t1) / 1000);
			
			// go through the second file
			cnt = 0;
			System.out.println("Merge and write");
			while((l1 = br2.readLine()) != null) {
				String[] segs = l1.split("[\t,]");
				int id = Integer.parseInt(segs[12]);
				if (ids.contains(id))
					bw.write(l1 + '\n');
				if (cnt++ % 1000000 == 0)
					System.out.println(cnt);
			}
			long t3 = System.currentTimeMillis();
			System.out.printf("Merge finished in %d seconds", (t3-t2)/1000);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	
	
	
	/* ==============================================================
	 * Sampling one:
	 * 
	 * Sample the complete trips of a subset of days.
	 * ==============================================================
	 */
	/**
	 * Sample a subset of trips by dates.
	 * 
	 * Given a list of dates, find the corresponding trips and store the trips in separate files.
	 * 
	 * The dates can be in different granularity, such as in month "2013-01", or in day "2013-12-21".
	 * 
	 * @deprecated This function should be replaced by {@link #sampleAndCompressTrip()}.
	 * Several disadvantages: i) it takes a lot of space, ii) the NTA information is not parsed.
	 */
	static public void sampleByDate() {
		// we already have "2013-02-09", "2013-02-10", "2013-02-11", "2013-02-12", "2013-02-13", "2013-02-14"
		String[] dates = {"2013-01-16", "2013-01-17", "2013-01-18", "2013-01-19", "2013-01-20", 
				"2013-01-21", "2013-01-22", "2013-01-23"};
		
		
		ArrayList<String> ds = new ArrayList<>(Arrays.asList(dates));		
		
		try {
			BufferedReader br = new BufferedReader( new FileReader(forg));
			ArrayList<BufferedWriter> abw = new ArrayList<>();
			for (String d : dates) {
				BufferedWriter bw = new BufferedWriter( new FileWriter(outputDir + d + ".trip") );
				abw.add(bw);
			}
			
			String line = null;
			int cnt = 0;
			while ( (line = br.readLine()) != null ) {
				int idx = matchDate(line, ds);	// -1 if not match
				if (idx != -1)
					abw.get(idx).write(line + '\n');
				
				if (cnt++ % 1_000_000 == 0)
					System.out.println(cnt);
			}
			
			br.close();
			for (BufferedWriter bw : abw)
				bw.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
				
	}
	
	
	
	/**
	 * Sample and compress a subset of trips by dates.
	 * 
	 * Given a list of dates, find the corresponding trips and store the trips in separate files.
	 * 
	 * The dates can be in different granularity, such as in month "2013-01", or in day "2013-12-21".
	 * 
	 * The output file is a concise presentation of the trip, including the following fileds:
	 * 1. trip ID		(int)
	 * 2. travel time	(float)
	 * 3. travel distance (float)
	 * 4. hour to year index (short)
	 * 5. pick up grid ID (byte)
	 * 6. drop off grid ID (byte)
	 * 
	 * This function will take place {@link #sampleByDate()} and {@link Raster#identifyTripNTA()}.
	 */
	static public void sampleAndCompressTrip() {
		ArrayList<NTA> ntas = NTA.getAllNeighborhoods();
		String[] dates = {"2013-01", "2013-02", "2013-03", "2013-04", "2013-05", "2013-06", "2013-07", "2013-08", "2013-09","2013-10", "2013-11", "2013-12"};
		ArrayList<String> ds = new ArrayList<>(Arrays.asList(dates));		
		
		try {
			long t1 = System.currentTimeMillis();
			System.out.println("Start split trips into months");
			
			BufferedReader br = new BufferedReader( new FileReader(forg));
			ArrayList<DataOutputStream> abw = new ArrayList<>();
			for (String d : dates) {
				DataOutputStream bw = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(outputDir + d + ".btrip")));
				abw.add(bw);
			}
			
			String line = null;
			int cnt = 0;
			while ( (line = br.readLine()) != null ) {
				int idx = matchDate(line, ds);	// -1 if not match
				if (idx != -1) {
					Trip trp = new Trip(line);
					trp.NTAbelonging(ntas);
					trp.writeToBinaryFile(abw.get(idx));
				}
				
				if (cnt++ % 1_000_000 == 0)
					System.out.println(cnt);
			}
			
			br.close();
			for (DataOutputStream bw : abw)
				bw.close();
			
			long t2 = System.currentTimeMillis();
			System.out.printf("Finished in %d seconds\n", (t2-t1)/1000);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
				
	}
	
	/**
	 * Assistant function for {@link #SampleTrip()}. 
	 * @param line  original line of trip record
	 * @param dates  a list of dates to match
	 * @return	If matched, then return the index of matched date, otherwise -1.
	 */
	static private int matchDate(String line, ArrayList<String> dates){
		String[] segs = line.split("[\t,]");	// 1-actual distance; 2-log L1; 4-duration; 6-pickup time
		
		String cdate = segs[6].substring(0, dates.get(0).length());
//		System.out.println(cdate);
//		System.out.println(dates.indexOf(cdate));
		return dates.indexOf(cdate);
	}
	
	
	
	static public void sampleTripByNeighborhood( ) {
		NTA lincoln = new NTA((byte)80, "Lincoln-Center", 
				new double[][]{{40.771054, -73.988119}, {40.775799, -73.988355}, 
				 {40.775571, -73.979536}, {40.769510, -73.979257}});
		
		NTA MSG = new NTA((byte) 81, "Madison-Square-Garden", 
				new double[][]{{40.748121, -73.999792}, {40.754119, -73.999814}, 
				{40.753598, -73.987777}, {40.746738, -73.987691}});
		
	
		String ntaName = "lincoln-MSG";
		int[][] tout = new int[2][8760];
		int[][] tin = new int[2][8760];
		int cnt = 0;
		
		try (	BufferedWriter bw = new BufferedWriter(new FileWriter(ntaName + "-out-in"));
				BufferedReader br = new BufferedReader( new FileReader(forg));
				){
			long t1 = System.currentTimeMillis();
			
			String l = null;
			while ( (l = br.readLine()) != null) {
				cnt ++;
				Trip trp = new Trip(l);
				
				if (lincoln.contains(trp.pickup)) {
					tout[0][trp.htyIdx] ++;
				}
				if (MSG.contains(trp.pickup))
					tout[1][trp.htyIdx] ++;
				if (lincoln.contains(trp.dropoff)) {
					tin[0][trp.htyIdx] ++;
				}
				if (MSG.contains(trp.dropoff))
					tin[1][trp.htyIdx] ++;
				
				if (cnt % 1_000_000 == 0)
					System.out.printf("%d millin\n", cnt / 1_000_000);
			}
			long t2 = System.currentTimeMillis();
			System.out.printf("Finished in %d seconds", (t2-t1)/1000);
			
			for (int[] k : tout) {
				for (int i : k)
					bw.write(Integer.toString(i) + "\t");
				bw.write("\n");
			}
			
			for (int[] list : tin) {
				for (int k : list)
					bw.write(Integer.toString(k) + "\t");
				bw.write("\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	static public void main (String argv[]) {
//		sampleByDate();
		sampleAndCompressTrip();
//		mergeFilteredTrip();
//		sampleTripByNeighborhood();
	}
	
	
}
