package nyctaxi;

import java.io.DataInputStream;
import java.util.HashMap;
import java.util.LinkedList;

public class SimpleTripUseDurationAsReference extends SimpleTrip {
	
	
	
	public SimpleTripUseDurationAsReference(DataInputStream test) throws Exception {
		super(test);
	}

	/**
	 * Estimate the travel time by current trips' neighbor.
	 * 
	 * The input is one line from the results of {@link nyctaxi.Raster#extractNeighborByMonth()}.
	 * 
	 * @param neighbor a string of neighbors with their 
	 * @return
	 * @throws Exception 
	 */
	@Override
	public double[] estimateT(LinkedList<NeededNeighborInfo> neighbor) throws Exception{
		
		double Tavg = 0;
		int cnt = 0;
		double SrelaW = 0;
		double SrelaW_r = 0;
		double SabsW = 0;
		double SabsW_R = 0;
		double Sum_dist =0;
		
		// initialize the reference to the global time reference
		double[] relaTimeRef = Raster.tr.get(Raster.ALL).relaRefSum;
		int[] relaRefCnt = Raster.tr.get(Raster.ALL).relaRefCnt;
		HashMap<Short, Integer> absRefCnt = Raster.tr.get(Raster.ALL).absRefCnt;
		HashMap<Short, Double> absTimeRef = Raster.tr.get(Raster.ALL).absSpeedRef;
		// reference to region-based speed reference
		double[] relaTimeRef_r = Raster.tr.get(this.ntaID).relaRefSum;
		int[] relaRefCnt_r = Raster.tr.get(this.ntaID).relaRefCnt;
		// ========== relative reference of current Trip ========== 
		double rela_vref = relaTimeRef[this.htwIdx] / relaRefCnt[this.htwIdx];
		double rela_R_vref = relaTimeRef_r[this.htwIdx] / relaRefCnt_r[this.htwIdx];
		// ========== ARIMA estimate the absolute reference of current Trip ========== 
		short[] idxes = {(short) (this.htyIdx -1), (short) (this.htyIdx - 2), (short) (this.htyIdx - 168),
				(short) (this.htyIdx - 168 -1), (short) (this.htyIdx - 168 - 2)};
		double[] vref = new double[5];
		for (int i = 0; i < vref.length; i++) {
			if (absTimeRef.containsKey(idxes[i])) {
				vref[i] = absTimeRef.get(idxes[i]) / absRefCnt.get(idxes[i]);
			} else {
				// the trip is too early, we cannot get its previous estimation
				// System.out.println(idxes[i]);
				return new double[]{-1, -1, -1, -1, -1, -1, -1, 0};
			}
		}
		double ar1 = Raster.ARIMA_paras.get(this.ntaID)[0];
		double ar2 = Raster.ARIMA_paras.get(this.ntaID)[1];
		double arima_vref = ar1 * (vref[0] - vref[3]) + ar2 * (vref[1] - vref[4]) + vref[2];
		// ========== absolute reference of current Trip ========== 
		double abs_vref = 0;
		if (absTimeRef.containsKey(this.htyIdx))
			abs_vref = absTimeRef.get(this.htyIdx) / absRefCnt.get(this.htyIdx);
		else {
			System.out.println(this.htyIdx);
			return new double[] {-1, -1, -1, -1, -1, -1, -1, 0};
		}
		// ========== region-based absolute reference of current Trip ==========
		double abs_R_vref = getLocalAbsRef(this.htyIdx, false)[0];
		
		for (NeededNeighborInfo neigh : neighbor) {
				short hty = neigh.htyIdx;
				int htw = get_hourIdx(hty);
				double travelTime = neigh.time;
				double travelDist = neigh.distance;
				// average
				Tavg += travelTime;
				// global relative weight
				SrelaW += travelTime /( relaTimeRef[htw] / relaRefCnt[htw] );
				SrelaW_r += travelTime /( relaTimeRef_r[htw] / relaRefCnt_r[htw] );
				// global absolute weight
				if (!absTimeRef.containsKey(hty)) {
					return new double[] {-1, -1, -1, -1, -1, -1, -1, 0};
				}
				SabsW += travelTime / (absTimeRef.get(hty) / absRefCnt.get(hty));
				// local absolute weight
				SabsW_R += travelTime / getLocalAbsRef(hty, false)[0];
				// count total neighbor
				cnt += 1;
				// use distance / vref
				Sum_dist += travelDist;
		}

//		System.out.printf("cnt: %d\tSrelaW: %g\trela_vref: %g\n", cnt, SrelaW, rela_vref);
		double[] estimation = new double[8];
		estimation[0] = Tavg / cnt;					// use average
		estimation[1] = SrelaW / cnt * rela_vref;	// use relative reference
		estimation[2] = SrelaW_r / cnt * rela_R_vref;// use relative reference + region
		estimation[3] = SabsW / cnt * abs_vref;		// use absolute reference (oracle)
		estimation[4] = SabsW / cnt * arima_vref;	// use absolute reference (ARIMA guess)
		estimation[5] = SabsW_R / cnt * abs_R_vref;	// use absolute reference + regional difference
		estimation[6] = Sum_dist / cnt * abs_vref * 3600;	// use distance / vref instead of time * v / vref
		estimation[7] = cnt;
		return estimation;
	}

}
