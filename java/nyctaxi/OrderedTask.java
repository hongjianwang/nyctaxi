package nyctaxi;

import java.io.*;
import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class OrderedTask extends Thread {
	public static LinkedBlockingQueue<Integer> bq = new LinkedBlockingQueue<>();
	public static AtomicInteger counter = new AtomicInteger(0);
	
	private int id;
	
	public OrderedTask(int i) {
		this.id = i;
	}
	
	
	public void run() {
		while (counter.get() < 100) {
			int v = -1;
			if (!bq.isEmpty())
				v = bq.poll();
			while( v!= -1 ) {
				if (v == counter.get()) {
					try {
						Thread.sleep(30);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					System.out.printf("Val: %d\tID: %d\tCounter: %d\n", v, this.id, counter.get());
					counter.incrementAndGet();					
					break;
				}
			}
		}
		System.out.printf("thread %d closed\n", this.id);
	}
	
	
	public static void testBinaryWriter() {
		try {
			long t1 = System.currentTimeMillis();
			BufferedReader br = new BufferedReader( new FileReader("../../../dataset/2013-02.trip"));
			DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("test-02.btrip")));
			
			
			String l = null;
			while ((l = br.readLine()) != null) {
				Trip trp = new Trip(l);
				dos.writeInt(trp.id);
				dos.writeFloat(trp.t);
				dos.writeFloat(trp.dist);
				dos.writeShort(trp.htyIdx);
			}
			
			br.close();
			dos.close();
			long t2 = System.currentTimeMillis();
			System.out.printf("Finished in %d seconds", (t2-t1)/1000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void MultiThreading_mainThread() {
		ArrayList<OrderedTask> ots = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			OrderedTask ot = new OrderedTask(i);
			ots.add(ot);
			ot.start();
		}
		

		for (int i = 0; i < 100; i++)
			bq.offer(i);

		
		try {
			for (OrderedTask ot : ots)
				ot.join();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] argv) {
		testBinaryWriter();
	}
	
}
