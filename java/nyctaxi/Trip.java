package nyctaxi;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import shtaxi.SHTrip;
import shtaxi.BaiduMapQuery;

/**
 * Trip class provides base functions over one trip sample.
 * @author Hongjian
 */
public class Trip {
	static public int nTimeSlots = 24*7;	
	
	static public final short EMPTY_NTAID = -1;
	static public int cnt = 0;
	static final String timeFormat = "yyyy-MM-dd HH:mm:ss";
	static final SimpleDateFormat sdf = new SimpleDateFormat(timeFormat);
	
	
	//=============================
	// Attribute read in from file
	//=============================
	int id;
	double[] pickup;	// lat, lon
	double[] dropoff;
	
	/**
	 * trip travel duration
	 */
	float t;
	
	/**
	 * actual trip distance
	 */
	float dist;
	
	/**
	 * index trip by the relative hour of a year
	 */
	short htyIdx;
	

	/**
	 * index of NTA pair -- src NTA to dst NTA.
	 * First byte is source NTA, and the second is destination NTA
	 */
	short ntaID;
	
	
	//==============================================
	// Attribute calculated based on previous ones
	//==============================================

	/**
	 * index trip by the relative hour of a day
	 * 
	 * calculated from <em>htyIdx</em>
	 */
	short htwIdx;
	
	float v; // the travel speed
	
	/**
	 * Grid information are initialized in {@link Raster#reverseIndexTrip(Trip)}.
	 */
	Grid pikGrid;
	Grid drpGrid;
	
	

	/**
	 * Constructor of Trip class
	 * @param line one sample in the raw data source
	 */
	public Trip(String line) {
		cnt ++;
		String[] segs = line.split("[\t,]");	// 1 feature one, 2 feature two, 4 duration, 5 distance, 6 pickup time, 

		id = Integer.parseInt(segs[12]);
		pickup = new double[]{ Double.parseDouble(segs[9]), Double.parseDouble(segs[8]) };
		dropoff = new double[] { Double.parseDouble(segs[11]), Double.parseDouble(segs[10]) };
		t = Float.parseFloat(segs[4]);
		dist = Float.parseFloat(segs[5]);	// 0 is L2 distance, 5 is actual distance
		ntaID = EMPTY_NTAID;
		
		Date d;
		try {
			d = sdf.parse(segs[6]); //[6]
			Calendar c = Calendar.getInstance();
			c.setTime(d);
			
			/**
			 * Get the <b>absolute</b> time reference (hour of year)
			 */
			htyIdx = (short) (c.get(Calendar.DAY_OF_YEAR) * 24 - 24 + c.get(Calendar.HOUR_OF_DAY));
			
			
			/**
			 * Get the <b>relative</b> time reference (hour of year)
			 */
			if (nTimeSlots == 24 * 7) {
				// one-week periodicity, 24 time slots per day
				htwIdx = (short) (c.get(Calendar.DAY_OF_WEEK) * 24 + c.get(Calendar.HOUR_OF_DAY) - 24);	// Sunday is the first day, starts with 1
//				htwIdx += c.get(Calendar.MINUTE) > 30 ? 1 : 0;
			} else if (nTimeSlots == 24 ) {	
				// 24 hour periodicity, 24 time slots
				htwIdx = (short) c.get(Calendar.HOUR_OF_DAY);
			} else if (nTimeSlots == 48 ) { 
				// weekday/weekend periodicity, 24 time slots per day
				/*
				 * further differentiate the weekends and weekdays
				 * 
				 * The weekdays have hourIdx 0 - 23, and the weekends have hourIdx 24 - 47
				 */
				int dow = c.get(Calendar.DAY_OF_WEEK);
				if (dow == Calendar.SUNDAY || dow == Calendar.SATURDAY)
					htwIdx = (short) (c.get(Calendar.HOUR_OF_DAY) + 24);
				else
					htwIdx = (short) (c.get(Calendar.HOUR_OF_DAY));
			} else {
				htwIdx = 0;
			}
			//System.out.printf("%s %d %d %d\n", segs[6], c.get(Calendar.DAY_OF_WEEK), c.get(Calendar.HOUR_OF_DAY), htyIdx);
			
		} catch (Exception e) {
			System.out.println(line);
			System.out.println(segs[6]);
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		if (Raster.Ref == REFTYPE.SPEED)
			v = dist/t * 3600;
		else if (Raster.Ref == REFTYPE.DURATION)
			v = t;
		
//		v = (Float.parseFloat(segs[1]) + Float.parseFloat(segs[2]))/2;
	}
	
	
	/**
	 * Create trip and initialize the ntaID
	 * @param line
	 * @param ntas
	 */
	public Trip(String line, ArrayList<NTA> ntas) {
		this(line);
		NTAbelonging(ntas);
	}
	
	
	public Trip(DataInputStream dataFin) throws IOException {
		cnt++;
		
		id = dataFin.readInt();
		pickup = new double[] {dataFin.readDouble(), dataFin.readDouble()};
		dropoff = new double[] {dataFin.readDouble(), dataFin.readDouble()};
		t = dataFin.readFloat();
		dist = dataFin.readFloat();
		htyIdx = dataFin.readShort();
		ntaID = dataFin.readShort();
		
		if (nTimeSlots == 24 * 7)
			this.htwIdx = (short) ((htyIdx + 24 * 2) % (24 * 7));	// first day in 2013 is Tuesday, our week starts from Sunday
		else if (nTimeSlots == 24)
			this.htwIdx = (short) (htyIdx % 24);
		
		if (Raster.Ref == REFTYPE.SPEED)
			v = dist/t * 3600;
		else if (Raster.Ref == REFTYPE.DURATION)
			v = t;
		
		/**
		 * This part guarantee the speed to be non-infinity.
		 * 
		 * If the input trips are not filtered by speed related filter, there
		 * is a great chance that the speed will be infinity. Here we hard-code
		 * to avoid such case.
		 */
		if (Float.isInfinite(v))
			v = 255;
	}
	
	
	
	/**
	 * Find the NTA src/dst pair of given trips
	 * @param ntas all NTAs
	 * @return a string of src,dst NTA ids
	 */
	public byte[] NTAbelonging(ArrayList<NTA> ntas) {
		byte[] res;
		if (ntas != null) {
			byte pickNTA = 0, dropNTA = 0;
			for (NTA n : ntas) {
				if (n.contains(this.pickup))
					pickNTA = n.id;
				if (n.contains(this.dropoff))
					dropNTA = n.id;
			}
			res = new byte[] {pickNTA, dropNTA};
		} else  {
			res =  new byte[] {0,0};
		}
		ntaID = ByteBuffer.wrap(res).getShort();
		return res;
	}
	
	
	
	
	/**
	 * Write one trip concisely and elegantly to binary file.
	 * 
	 * 1. trip ID		(int)
	 * 2. pickup / dropoff coordinates double[]
	 * 3. travel time	(float)
	 * 4. travel distance (float)
	 * 5. hour to year index (short)
	 * 6. ntaID	-- a short: first byte is pickup NTA, last byte is dropoff NTA
	 * @param fout
	 * @throws Exception
	 * 
	 * The trip can be recovered by {@link #Trip(DataInputStream)}
	 * and {@link SimpleTrip#SimpleTrip(DataInputStream)}.
	 */
	public void writeToBinaryFile(DataOutputStream fout) throws Exception {
		fout.writeInt(this.id);
		fout.writeDouble(this.pickup[0]);
		fout.writeDouble(this.pickup[1]);
		fout.writeDouble(this.dropoff[0]);
		fout.writeDouble(this.dropoff[1]);
		fout.writeFloat(this.t);
		fout.writeFloat(this.dist);
		fout.writeShort(this.htyIdx);
		fout.writeShort(this.ntaID);
	}
	
	
	@Override
	public String toString() {
		byte srcID = (byte) (ntaID>>8 & 0xff);
		byte dstID = (byte) (ntaID & 0xff);
		return String.format("%d,(%g,%g)-(%g,%g),t:%g,d:%g,hty:%d,htw:%d,NTA:%d(%d,%d)", 
				id, pickup[0], pickup[1], dropoff[0], dropoff[1], t, dist, 
				htyIdx, htwIdx, ntaID, srcID, dstID);
	}
	
	
	
	///============================================================================
	///				Estimation Part
	///============================================================================
	/**
	 * Estimate the travel time of current trip with its neighboring trips
	 * @param neighbors a set of neighboring trips
	 * @param method the estimation method, it could be "avg" (average), "weight" (weight by time reference,
	 * 	or "mixed" (a mix of the previous two)
	 * @return the travel time
	 * @throws Exception 
	 */
	public double estimateT( HashSet<Trip> neighbors, String method) throws Exception {
		double T = 0;
		
		if ( method.equals("avg") ) {
			for (Trip t : neighbors)
				T += t.t;
			T =  T / neighbors.size();
		} else if (method.equals("median")) {
			double ts[] = new double[neighbors.size()];
			int i = 0;
			for (Trip t : neighbors) {
				ts[i] = t.t;
				i++;
			}
			Arrays.sort(ts);
			T = ts[ts.length / 2];
		} else {
			// initialize the reference to the global time reference
			double[] relaSpeedRef = Raster.tr.get(Raster.ALL).relaRefSum;
			int[] relaRefCnt = Raster.tr.get(Raster.ALL).relaRefCnt;
			HashMap<Short, Integer> absRefCnt = Raster.tr.get(Raster.ALL).absRefCnt;
			HashMap<Short, Double> absSpeedRef = Raster.tr.get(Raster.ALL).absSpeedRef;
			
			
			if (method.equals("mixed")) {
				// balance between #neighbors
				HashSet<Trip> sameSlotTrips = new HashSet<>();
				double tmpT = 0;
				for (Trip t : neighbors)
					if (t.htwIdx == htwIdx) {
						sameSlotTrips.add(t);
						tmpT += t.t;
					}
				
				if (sameSlotTrips.size() > 2)
					T = tmpT / sameSlotTrips.size();
				else {
					double vref = relaSpeedRef[this.htwIdx] / relaRefCnt[this.htwIdx];
					double w;
					for (Trip t: neighbors) {
						w = relaSpeedRef[t.htwIdx] / relaRefCnt[t.htwIdx];
						T += w * t.t;
					}
					T = T / neighbors.size() / vref;
				}
			} else if (method.equals("relaweight")) {	// relative weight		
				double vref = relaSpeedRef[this.htwIdx] / relaRefCnt[this.htwIdx];
				double w;
				for (Trip t: neighbors) {
					w = relaSpeedRef[t.htwIdx] / relaRefCnt[t.htwIdx];
					T += w * t.t;
				}
				T = T / neighbors.size() / vref;
			} else if (method.equals("absweight")) {
				double vref = absSpeedRef.get(this.htyIdx) / absRefCnt.get(this.htyIdx);
				double w;
				for (Trip t: neighbors) {
					w = absSpeedRef.get(t.htyIdx) / absRefCnt.get(t.htyIdx);
					T += w * t.t;
				}
				T = T / neighbors.size() / vref;
	//		} else if (method == "spascale-absweight") {
	//			double vref = absSpeedRef.get(this.htyIdx) / absRefCnt.get(this.htyIdx);
	//			double w;
	//			for (Trip t: neighbors) {
	//				w = absSpeedRef.get(t.htyIdx) / absRefCnt.get(t.htyIdx);
	//				T += w * t.t * this.l1 / t.l1;	// spatial scale by L1
	//			}
	//			T = T / neighbors.size() / vref;
			} else if (method.equals("spasig-absweight")) {
				double vref = absSpeedRef.get(this.htyIdx) / absRefCnt.get(this.htyIdx);
				double w;
				double W = 1;
				for (Trip t: neighbors) {
					w = absSpeedRef.get(t.htyIdx) / absRefCnt.get(t.htyIdx);
					double sig = this.significance(t);
					T += w * t.t * sig;
					W += sig;
				}
				T = T / vref / W; 
	//		} else if (method == "spascale-spasig-absweight") {
	//			double vref = absSpeedRef.get(this.htyIdx) / absRefCnt.get(this.htyIdx);
	//			double w;
	//			double W = 1;
	//			for (Trip t: neighbors) {
	//				w = absSpeedRef.get(t.htyIdx) / absRefCnt.get(t.htyIdx);
	//				double sig = this.significance(t);
	//				T += w * t.t * sig * this.l1 / t.l1;	// spatial scale
	//				W += sig;	// weighted significance
	//			}
	//			T = T / vref / W; 
			} else if (method.equals("arima2-absweight")) {
				double vref_1 = 0, vref_2 = 0, vref_s = 0, vref_s1 = 0, vref_s2 = 0;
				vref_1 = absSpeedRef.get(this.htyIdx-1) / absRefCnt.get(this.htyIdx-1);
				vref_2 = absSpeedRef.get(this.htyIdx-2) / absRefCnt.get(this.htyIdx-2);
				vref_s = absSpeedRef.get(this.htyIdx-168) / absRefCnt.get(this.htyIdx-168);
				vref_s1 = absSpeedRef.get(this.htyIdx-168-1) / absRefCnt.get(this.htyIdx-168-1);
				vref_s2 = absSpeedRef.get(this.htyIdx-168-2) / absRefCnt.get(this.htyIdx-168-2);
				double ar1 = 0.6000772;
				double ar2 = 0.1837098;
				double vref = ar1 * (vref_1 - vref_s1) + ar2 * (vref_2 - vref_s2) + vref_s;
						
				double w;
				for (Trip t: neighbors) {
					w = absSpeedRef.get(t.htyIdx) / absRefCnt.get(t.htyIdx);
					T += w * t.t;
				}
				T = T / neighbors.size() / vref;
			} else if (method == "localspeed-absweight") {			
				double vref = getLocalAbsRef();
				double w;
				for (Trip t: neighbors) {
					w = t.getLocalAbsRef();
					T += w * t.t;
				}
				T = T / neighbors.size() / vref;
			}else if (method == "localspeed-relaweight") {			
				double vref = getLocalRelaRef();
				double w;
				for (Trip t: neighbors) {
					w = t.getLocalRelaRef();
					T += w * t.t;
				}
				T = T / neighbors.size() / vref;
			} else {
				throw new Exception("No method match");
			}
		}
		return T;
	}
	
	/**
	 * Get the NTA-based absolute speed reference
	 * 
	 * If does not exist or not significant, use the global absolute speed reference instead.
	 * @return the local absolute speed reference
	 */
	double getLocalAbsRef() {
		HashMap<Short, Integer> glb_absRefCnt = Raster.tr.get(Raster.ALL).absRefCnt;
		HashMap<Short, Double> glb_absSpeedRef = Raster.tr.get(Raster.ALL).absSpeedRef;
		HashMap<Short, Integer> loc_absRefCnt = Raster.tr.get(ntaID).absRefCnt;
		HashMap<Short, Double> loc_absSpeedRef = Raster.tr.get(ntaID).absSpeedRef;
		
		// get the global reference, if the local one does not exist or has a small support
		if (loc_absRefCnt.containsKey(htyIdx) && loc_absRefCnt.get(htyIdx) >= 5)
			return loc_absSpeedRef.get(htyIdx) / loc_absRefCnt.get(htyIdx);
		else
			return glb_absSpeedRef.get(htyIdx) / glb_absRefCnt.get(htyIdx);
	}
	
	/**
	 * Get the NTA-pair-based relative speed reference.
	 * 
	 * If does not exist or not significant, use the global relative speed reference instead.
	 * @return the local relative speed reference
	 */
	double getLocalRelaRef() {
		int[] glb_relaRefCnt = Raster.tr.get(Raster.ALL).relaRefCnt;
		double[] glb_relaSpeedRef = Raster.tr.get(Raster.ALL).relaRefSum;
		int[] loc_relaRefCnt = Raster.tr.get(ntaID).relaRefCnt;
		double[] loc_relaSpeedRef = Raster.tr.get(ntaID).relaRefSum;
		
		// get the global reference, if the local one does not exist or has a small support
		if (loc_relaRefCnt[htwIdx] >= 5 )
			return loc_relaSpeedRef[htwIdx] / loc_relaRefCnt[htwIdx];
		else
			return glb_relaSpeedRef[htwIdx] / glb_relaRefCnt[htwIdx];
	}
	
	
	
	/**
	 * Calculate the significance of other trip regarding current trip.
	 * 
	 * This significance tells us how much we should rely on this observation.
	 * The significance function is 
	 * 		y = exp ( - 0.00002 * x^2 )
	 * @param o the other trip.
	 * @return the other trip's significance.
	 */
	public double significance(Trip o) {
		double dv = (this.pickup[0] - o.pickup[0]) * 69.0093;	// the distance difference over 1 latitude
		double dh = (this.pickup[1] - o.pickup[1]) * 52.4366;	// the distance difference over 1 longitude
		double dv2 = (this.dropoff[0] - o.dropoff[0]) * 69.0093;
		double dh2 = (this.dropoff[1] - o.dropoff[1]) * 52.4366;
		
		double delta = Math.sqrt(dv * dv + dh * dh) * 1609.34; 	// convert to meters
		double delta2 = Math.sqrt(dv2* dv2 + dh2 * dh2) * 1609.34;
		
		double p = Math.exp(- 0.00002 * delta*delta);
		double p2 = Math.exp(- 0.0002 * delta2*delta2);
		
		return p * p2;
	}
	

	
	private short get_hourIdx(int hty) {
		return htwIdx = (short) ((hty + 24 * 2) % (24 * 7));	// first day in 2013 is Tuesday, our week starts from Sunday
	}
	

	/**
	 * Retrieve all trips from given path
	 * @param filePath the raw data path
	 * @return a linked list of all trips
	 */
	public static LinkedList<Trip> retrieveAllTrips(String filePath) {
		LinkedList<Trip> trips = new LinkedList<>();
		
		try (BufferedReader bf = new BufferedReader( new FileReader(filePath) )) {
			String l = null;
			while ((l = bf.readLine()) != null) {
				Trip t = new Trip(l);
				trips.add(t);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return trips;
	}
	
	
	/**
	 * Retrieve all trips in the given ID set
	 * @param filePath the raw data path
	 * @param ids the ID set in a HashSet
	 * @return a linked list of all trips
	 */
	public static LinkedList<Trip> retrieveAllTrips(String filePath, HashSet<Integer> ids) {
		LinkedList<Trip> trips = new LinkedList<>();
		
		try (BufferedReader bf = new BufferedReader( new FileReader(filePath) )) {
			String l = null;
			while ((l = bf.readLine()) != null) {
				Trip t = new Trip(l);
				if (ids.contains(t.id))
					trips.add(t);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return trips;
	}
	

	
	
	
	/**
	 * For the testing trips in Shanghai data, we crawl Baidu service as one baseline to compare with.
	 * 
	 * Use the results of {@link SHTrip#generateNYCformattedData()}.
	 * 
	 * Depends on the functions:
	 * {@link SHTrip#getIDSet(String)} for testing trip IDs
	 * {@link #retrieveAllTrips(String)} for testing trips 
	 */
	public static void query_Baidu_service_SH_testingtrips() {
		HashSet<Integer> ids = SHTrip.getIDSet("prediction-sampled");
		LinkedList<Trip> trips = retrieveAllTrips("../../../dataset/SH_taxi_trip", ids);
		BaiduMapQuery bmq = new BaiduMapQuery();
		
		int c = 0;
		try (BufferedWriter bw = new BufferedWriter(new FileWriter("../../../dataset/SH_taxi_trip_Baidu"))) {
			for (Trip t : trips) {
				double[] s = bmq.queryTimeDistance(t.pickup, t.dropoff);
				if (s != null)
					bw.write(String.format("%d,%g,%g\n", t.id, s[0], s[1])); 	// duration, distance
				if (c++ % 100 == 0)
					System.out.println(c);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	/**
	 * Sample the December trip
	 */
	public static void sample_December_trip_for_Bing() {
		try (DataInputStream fin = new DataInputStream(new BufferedInputStream(
				new FileInputStream(Raster.tripByMonthFolder + "2013-12.btrip")));
				BufferedWriter bw = new BufferedWriter(new FileWriter(Raster.tripByMonthFolder 
						+ "2013-12.Bing.trip"))) {
			boolean eof = false;
			Random rnd = new Random();
			while (!eof) {
				try{
					Trip t = new Trip(fin);
					int dow = t.htwIdx / 24;	// Starts at Sunday(0)
					int htd = t.htwIdx % 24;
					if (t.htyIdx < 8592 && (dow == 0 || dow == 1 || dow == 2 || dow == 6))
						if (rnd.nextFloat() < 0.05)
							bw.write(String.format("%g,%g,%g,%g,%d,%d,%d\n", t.pickup[0], t.pickup[1],
									t.dropoff[0], t.dropoff[1], dow, htd, t.id));
				} catch (IOException e) {
					eof = true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	//====================================================================
	// 				Test function
	//====================================================================
	
	/**
	 * Testing function - parse the various fields of a raw Trip
	 */
	public static void test_trip() {
		String fin = "../../../dataset/training2";
		try (BufferedReader br = new BufferedReader( new FileReader(fin)) ) {
			String l = br.readLine();
			
			String[] segs = l.split("[,\t]");
			for (int i = 0; i < segs.length; i++) 
				System.out.printf("%d\t%s\n", i, segs[i]);
			Trip trip = new Trip(l);
			
			System.out.printf("htw: %d\tcalculated htw: %d", trip.htwIdx, trip.get_hourIdx(trip.htyIdx));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Testing function - parse the date
	 * 
	 * ! in SimpleDateFormat, D and d is different.
	 */
	public static void test_dateParse() {
		try {
			Date t = sdf.parse("2013-09-30 23:08:27");
			System.out.println(t);
			Calendar c = Calendar.getInstance();
			c.setTime(t);
			System.out.printf("%d-%d-%d %d:%d:%d\n", c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_YEAR),
					c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), c.get(Calendar.SECOND));
		} catch (ParseException e) {
			System.out.println(e.getMessage());
		}
	}
	
	
	public static void extract_LR_parameters() {
		try (DataInputStream dis = new DataInputStream(new BufferedInputStream(
				new FileInputStream(Raster.tripByMonthFolder + "2013-11.btrip-outlier")));
				BufferedWriter bw = new BufferedWriter(new FileWriter(Raster.tripByMonthFolder + 
						"2013-11.LR.trip-outlier"))) {
			boolean eof = false;
			while (!eof){
				try {
					Trip t = new Trip(dis);
					bw.write(String.format("%g,%g\n", t.dist, t.t));
				} catch (IOException e) {
					eof = true;
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	//====================================================================
	// 				Main function
	//====================================================================
	
	public static void  main( String[] argv ) {
//		test_trip();
//		test_dateParse();
		extract_LR_parameters();
//		query_Baidu_service_SH_testingtrips();
//		sample_December_trip_for_Bing();
	}
	
	
	
	
}
