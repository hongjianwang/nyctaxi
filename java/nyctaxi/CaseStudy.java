package nyctaxi;

import java.io.*;
import java.util.HashSet;
import org.apache.commons.lang3.*;

public class CaseStudy {


	Trip target;
	HashSet<Trip> neighbors;
	BufferedWriter bw;
	
	
	public CaseStudy() {
		try {
			bw = new BufferedWriter(new FileWriter("cases-neighbor-list"));
			bw.write("var cases=[");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void addCase(Trip t, HashSet<Trip> ns) {
		target = t;
		neighbors = ns;
	}
	
	public void getNeighborsInDetail() {
		TimeReference tr = Raster.tr.get(Raster.ALL);
		System.out.format("Target trip %d has $i neigbhboring trips%n", target.id, neighbors.size());
		System.out.println(StringUtils.repeat("=", 90));
		System.out.println(" \tdistance\tduration\tdate\tspeed\t\tRef\t");
		System.out.println(StringUtils.repeat("=", 90));
		double vref = tr.relaRefCnt[target.htwIdx] / tr.relaRefSum[target.htwIdx];
		System.out.format("Target\t %g \t%g\t\tD%d\t\t%d\t%g  \t%g", target.dist, target.t, target.htwIdx / 24, target.htwIdx % 24, 
				target.v, vref);
		
		int i = 0;
		for (Trip t : neighbors) {
			double nvref = tr.relaRefCnt[t.htwIdx] / tr.relaRefSum[t.htwIdx];
			System.out.format("N%d\t %g \t%g\t\tD%d\t\t%d\t%g  \t%g", i, t.dist, t.t, t.htwIdx / 24, t.htwIdx % 24, 
					t.v, nvref);
		}
		System.out.println(StringUtils.repeat("=", 90));
	}
	
	public void saveNeighborIDsToFile() {
		try {
			bw.write(String.format("[%d", target.id));
			for (Trip t : neighbors) {
				bw.write(String.format(",%d", t.id));
			}
			bw.write("],");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void closeFile() {
		try {
			bw.write("]");
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
