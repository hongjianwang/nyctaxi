// package nyctaxi
import nyctaxi.*;


class CaseStudy {
	Trip target
	HashSet<Trip> neighbors
	BufferedWriter bw
	
	CaseStudy() {
		File f = new File("cases-neighbor-list")
		bw = new BufferedWriter(new FileWriter(f))
		bw.write("var cases = [")
	}
	
	
	def addCase(Trip t, HashSet<Trip> ns) {
		target = t
		neighbors = ns
	}
	
	
	def getNeighborsInDetail() {
		TimeReference tr = Raster.tr.get(Raster.ALL)
		println "Target trip ${target.id} has ${neighbors.size()} neighboring trips."
		println "=" * 90
		println " \tdistance\tduration\tdate\tspeed\t\tRef\t"
		println "-" * 90
		def vref = tr.relaRefSum[target.htwIdx] / tr.relaRefCnt[target.htwIdx]
		println "Target\t${target.dist} \t${target.t}\t\tD${target.htwIdx.intdiv(24)} H${target.htwIdx%24}\t${target.v}  \t$vref"
		neighbors.eachWithIndex { it, idx ->
			def nvref = tr.relaRefSum[it.htwIdx] / tr.relaRefCnt[it.htwIdx]
			println "N$idx\t${it.dist}  \t${it.t}\t\tD${it.htwIdx.intdiv(24)} H${it.htwIdx%24}\t${it.v}  \t$nvref"
		}
		println "=" * 90
	}
	
	
	def saveNeighborIDsToFile() {
		bw.write("[${target.id}")
		neighbors.each {
			bw.write(",${it.id}")
		}
		bw.write("],")
	}
	
	
	def closeFile() {
		bw.write("]")
		bw.close()
	}
}
