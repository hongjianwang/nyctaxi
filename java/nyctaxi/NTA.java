package nyctaxi;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import java.awt.geom.Path2D;
import java.io.File;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.*;

/**
 * NTA is the neighborhood tabulation area defined by NYC bureau.
 * @author hxw186
 *
 */
public class NTA {
	
	byte id;
	String title;
	ArrayList<Path2D.Double> boundary;
	
	public NTA( Node n ) {
		// initialize id
		Node idN = n.getFirstChild();
		String id = idN.getFirstChild().getNodeValue();
		// initialize title
		this.id = Byte.parseByte(id.substring(2, 4));
		Node titN = idN.getNextSibling();
		this.title = titN.getFirstChild().getNodeValue();
		// initialize boundary
		boundary = new ArrayList<>();
		Node polyN = titN.getNextSibling();
		do {
			NodeList points = polyN.getChildNodes();
			// construct one Path
			int npoints = points.getLength();
			Path2D.Double p = new Path2D.Double();
			for (int i = 0; i < npoints; i++) {
				Node point = points.item(i);
				double x = Double.parseDouble(point.getLastChild().getFirstChild().getNodeValue());		// lat
				double y = Double.parseDouble(point.getFirstChild().getFirstChild().getNodeValue());	// lon
				if (i == 0)
					p.moveTo(x, y);
				else
					p.lineTo(x, y);
			}
			p.closePath();
			boundary.add(p);
			polyN = polyN.getNextSibling();
		} while (polyN != null);
	}
	
	
	/**
	 * Initiate a region with its boundary
	 * @param coords
	 */
	public NTA (byte id, String name, double[][] coords) {
		this.id = id;
		this.title = name;
		this.boundary = new ArrayList<>();
		Path2D.Double p = new Path2D.Double();
		for (int i = 0; i < coords.length; i++)
			if (i == 0)
				p.moveTo(coords[i][0], coords[i][1]);
			else
				p.lineTo(coords[i][0], coords[i][1]);
		p.closePath();
		boundary.add(p);
	}
	
	/**
	 * Decide whether a point is inside the Polygon.
	 * @param coords
	 * @return
	 */
	public boolean contains(double[] coords) {
		boolean flag = false;
		for (Path2D.Double p : boundary) {
			flag = flag || p.contains(coords[0], coords[1]);
		}
		return flag;
	}
	
	
	@Override
	public String toString() {
		return String.format("id:%s, title:%s, #Polygon: %d", id, title, boundary.size());
	}
	
	public static ArrayList<NTA> getAllNeighborhoods() {
		ArrayList<NTA> ntas = new ArrayList<NTA>();
		try {
			File xmlF = new File(Raster.ntaXML);
			DocumentBuilder dbuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document nta = dbuilder.parse(xmlF);
			// clean the XML trees
			nta.getDocumentElement().normalize();
			
			System.out.println(nta.getDocumentElement().getNodeName());
			
			NodeList nodes = nta.getElementsByTagName("neighborhood");
			
			for (int i = 0; i < nodes.getLength(); i++) {
				NTA n = new NTA(nodes.item(i));
				ntas.add(n);
			}
			System.out.println(nodes.getLength());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ntas;
	}
	
	
	public static void test_parseXML() {
		try {
			File xmlF = new File(Raster.ntaXML);
			DocumentBuilder dbuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document nta = dbuilder.parse(xmlF);
			// clean the XML trees
			nta.getDocumentElement().normalize();
			
			System.out.println(nta.getDocumentElement().getNodeName());
			
			NodeList nodes = nta.getElementsByTagName("neighborhood");
			
			for (int i = 0; i < nodes.getLength(); i++) {
				NTA n = new NTA(nodes.item(i));
				System.out.println(n.toString());
			}
			System.out.println(nodes.getLength());
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}
	
	public static void main(String[] argv) {
		test_parseXML();
	}
	
}
