%{

Date: Oct 11, 2015
Author: Hongjian Wang

This file finds supporting cases for the ICDE revision.

The outputs are figures inserted in the ICDE submission version: Section IV
capturing the temporal dynamics of termporal reference.

Dependency: 

java nyctaxi.Raster temporalScaler 20 30 > 20-30.log
python TemporalScaler.py 20-30.log individual




====================================
To find individual cases.

The python script will generate the ``cases.log'' file as input of this
Matlab script.


====================================
To find grouped cases.

The python script will generate the ``groupCase.log'' file.

%}


FLAG = 'individual';



if strcmp(FLAG, 'individual') == 1
    fid = fopen('cases.log');


    line = fgetl(fid);
    cnt = 0;

    while ischar(line)
        ls = strsplit(line, ';');
        scaledT = sscanf(ls{1}, '%f,');
        rawT = sscanf(ls{2}, '%f,');

        gnd = scaledT(1);
        scaledT = scaledT(2:end);
        gndref = ones(size(scaledT)) * gnd;
        x = 1:length(scaledT);

        figure();
        hold on;
        grid on;
        box on;
        
        stem(x, scaledT, 'r^-', 'linewidth', 4)
        h2 = stem(x+0.2, rawT, 'b--', 'linewidth', 4);
        hbase = get(h2, 'Baseline');
        set(hbase, 'Basevalue', gnd);
        
        hl = refline(0, gnd);
        set(hl, 'color', [0, 0.6, 0], 'linestyle', '-', 'linewidth', 4)

        curYlim = get(gca, 'ylim');
        ylim([0, curYlim(2)])

        set(gca, 'linewidth', 4, 'fontsize', 20)
        legend({'Scaled duration $s_it_i$', 'Original duration $t_i$', 'Target duration $t_q$'}, ...
            'fontsize', 20, 'location', 'best', 'interpreter', 'latex');
        xlabel('Index of neighboring trips', 'fontsize', 28);
        ylabel('Trip travel time (seconds)', 'fontsize', 28);

        print(gcf, '-dpsc', sprintf('x%d.eps', cnt));
        system(sprintf('epstopdf x%d.eps', cnt));
        delete(sprintf('x%d.eps', cnt));
        cnt = cnt + 1;

        line = fgetl(fid);
    end

    fclose(fid);

elseif strcmp(FLAG, 'group') == 1
    
    A = importdata('groupCase.log');
    
    y = A(:,2);
    x = A(:,1);
    
    mdl = LinearModel.fit(x, y);
    coef = mdl.Coefficients.Estimate;
    
    figure()
    hold on
    box on
    grid on
   
    scatter(x, y);
    lf = refline(coef(2), coef(1));
    set(lf, 'color', [0, 0.8, 0], 'linewidth', 4, 'linestyle', '-.');
    lr = refline(1, 0);
    set(lr, 'color', 'r', 'linewidth', 4, 'linestyle', '--');
    
    
    
    legend({'One trip', 'Linear fit', '$y=x$'}, 'interpreter', 'latex', ...
        'location', 'best');
    set(gca, 'linewidth', 4, 'fontsize', 20)
    set(gca, 'Units','normalized', 'Position',[0.15 0.2 0.75 0.7]);
    xlabel('Target trip travel time $t_q$ (seconds)', 'interpreter', 'latex', 'fontsize', 24);
    ylabel('Prediction $\hat{t_q}$ (seconds)', 'interpreter', 'latex', 'fontsize', 24);
    print(gcf, '-dpsc', 'groupFit.eps');
    system('epstopdf groupFit.eps');
    delete('groupFit.eps');
        
 
elseif strcmp(FLAG, 'refRatio') == 1
    
    A = importdata('0-100-ref.log');
    rt = A(:,1);
    rv = A(:,2);
    rV = A(:,3);
    rl = A(:,4);
    rVR = A(:,5);
    
    figure()
    hold on
    grid on
    box on
    
    scatter(rt, rv);
    
    mdl = LinearModel.fit(rt, rv);
    coef = mdl.Coefficients.Estimate;
    
    lf = refline(coef(2), coef(1));
    set(lf, 'color', [0, 0.8, 0], 'linewidth', 4, 'linestyle', '-');
    lr = refline(1, 0);
    set(lr, 'color', 'r', 'linewidth', 4, 'linestyle', '--');
    
    set(gca, 'linewidth', 4, 'fontsize', 20)
    set(gca, 'Units','normalized', 'Position',[0.15 0.2 0.75 0.7]);
    legend({'One pair', 'Linear fit', '$y=x$'}, 'interpreter', 'latex', ...
        'location', 'best');
    xlabel('Travel time ratio $t_q/t_i$', 'interpreter', 'latex', 'fontsize', 24);
    ylabel('Speed ratio $v_i/v_q$', 'interpreter', 'latex', 'fontsize', 24);
    
    set(gcf, 'PaperUnits', 'inches', 'PaperPosition', [1,1,6,6], 'PaperPositionMode', 'manual');
    
    f1name = 'ref-t-v.eps';
    print(gcf, '-dpsc', f1name);
    system(sprintf('epstopdf %s', f1name));
    
    
    
    f2 = figure();
    hold on
    box on
    grid on
    
    scatter(rv, rV);
    mdl = LinearModel.fit(rv, rV);
    coef = mdl.Coefficients.Estimate;
    lf = refline(coef(2), coef(1));
    set(lf, 'color', [0, 0.8, 0], 'linewidth', 4, 'linestyle', '-');
    lr = refline(1, 0);
    set(lr, 'color', 'r', 'linewidth', 4, 'linestyle', '--');
    
    set(gca, 'linewidth', 4, 'fontsize', 20)
    set(gca, 'Units','normalized', 'Position',[0.15 0.2 0.75 0.7]);
    legend({'One pair', 'Linear fit', '$y=x$'}, 'interpreter', 'latex', ...
        'location', 'best');
    xlabel(sprintf('Speed ratio $v_i/v_q$. $R^2=%.3g$', mdl.Rsquared.Ordinary), 'interpreter', 'latex', 'fontsize', 24);
    ylabel('Reference ratio $V(s_i)/V(s_q)$', 'interpreter', 'latex', 'fontsize', 24);
    
    set(gcf, 'PaperUnits', 'inches', 'PaperPosition', [1,1,6,6], 'PaperPositionMode', 'manual');
    
    f1name = 'ref-v-V.eps';
    print(gcf, '-dpsc', f1name);
    system(sprintf('epstopdf %s', f1name));
    
    
    f3 = figure();
    hold on
    box on
    grid on
    
    scatter(rv, rVR);
    mdl = LinearModel.fit(rv, rVR);
    coef = mdl.Coefficients.Estimate;
    lf = refline(coef(2), coef(1));
    set(lf, 'color', [0, 0.8, 0], 'linewidth', 4, 'linestyle', '-');
    lr = refline(1, 0);
    set(lr, 'color', 'r', 'linewidth', 4, 'linestyle', '--');
    
    set(gca, 'linewidth', 4, 'fontsize', 20)
    set(gca, 'Units','normalized', 'Position',[0.15 0.2 0.75 0.7]);
    legend({'One pair', 'Linear fit', '$y=x$'}, 'interpreter', 'latex', ...
        'location', 'best');
    xlabel(sprintf('Speed ratio $v_i/v_q$. $R^2=%.3g$', mdl.Rsquared.Ordinary), 'interpreter', 'latex', 'fontsize', 24);
    ylabel('Reference ratio $V_{i\rightarrow j}(s_i)/V_{i\rightarrow j}(s_q)$', 'interpreter', 'latex', 'fontsize', 24);
    
    set(gcf, 'PaperUnits', 'inches', 'PaperPosition', [1,1,6,6], 'PaperPositionMode', 'manual');
    
    f1name = 'ref-v-VR.eps';
    print(gcf, '-dpsc', f1name);
    system(sprintf('epstopdf %s', f1name));
    
end