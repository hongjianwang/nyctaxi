a = importdata('cluster0.txt');
b = importdata('cluster1.txt');
a = a(:, 121:288);
b = b(:, 121:288);

f1 = figure();
hold on;
box on;
plot(a');
title('Cluster 0');
print(f1, 'cluster0.eps', '-dpsc');
system('epstopdf cluster0.eps');


f2 = figure();
hold on;
box on;
plot(b');
title('Cluster 1');
print(f2, 'cluster1.eps', '-dpsc');
system('epstopdf cluster1.eps');