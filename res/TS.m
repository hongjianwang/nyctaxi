IB = importdata('IB_TS.txt');
OB = importdata('OB_TS.txt');
DB = importdata('DFF_TS.txt');
des = importdata('order.txt');


for i = 1:size(IB,1)
    fid = figure();
    hold on;
    box on;
    
    plot(IB(i,:), 'r:', 'linewidth', 3);
    plot(OB(i,:), 'b-.', 'linewidth', 3);
    plot(DB(i,:), 'g-', 'linewidth', 3);
    legend({'Inbound', 'Outbound', 'Difference'});
    title( des(i) );
    print(fid, ['res', num2str(i), '.png'],  '-dpng');
end

TB = sum(IB, 2) + sum(OB, 2);
[~, ind] = sort(TB, 'descend');
sTB = TB(ind);
sDes = des(ind);
fid = fopen('isolation.txt', 'w');
for i = 1:length(ind)
    fprintf(fid, '%d\t%s\n', sTB(i), sDes{i});
end
fclose(fid);