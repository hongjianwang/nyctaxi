MN31	Lenox Hill-Roosevelt Island
MN04	Hamilton Heights
MN06	Manhattanville
MN01	Marble Hill-Inwood
MN32	Yorkville
MN03	Central Harlem North-Polo Grounds
MN40	Upper East Side-Carnegie Hill
MN33	East Harlem South
MN09	Morningside Heights
MN28	Lower East Side
MN27	Chinatown
MN25	Battery Park City-Lower Manhattan
MN24	SoHo-TriBeCa-Civic Center-Little Italy
MN23	West Village
MN22	East Village
MN21	Gramercy
MN20	Murray Hill-Kips Bay
MN50	Stuyvesant Town-Cooper Village
MN19	Turtle Bay-East Midtown
MN34	East Harlem North
MN13	Hudson Yards-Chelsea-Flatiron-Union Square
MN36	Washington Heights South
MN11	Central Harlem South
MN17	Midtown-Midtown South
MN14	Lincoln Square
MN15	Clinton
MN12	Upper West Side
MN35	Washington Heights North
MN99	park-cemetery-etc-Manhattan
-1	others
