'''
Use Bing Maps service to get the
actual driving distance/duration between
any two given points.

API usage limits:
    125,000 transaction over 12 months.

    
We also need the travel duration considering traffic.
To do so, we need to send the online query to map
service at the given time of a trip.

Hongjian Wang
12/1/2014
'''


from urllib import urlencode, urlopen
import json
import time, sched
import random, sys, os


keys = ( 'ApPQt050IYWujtE62x4TzB3arVmy96n5UIGrOVHyiLhrBYsW4mDdPVy6JJiEKiIJ', 
            'AhbFkDd8fbqTTZxA6xeQq-Ba0AzyHhlQUBwkDzTNWJbAiFT2_7sc8MtsQwqwk_mh',
            'Am_YDPdso_dKNM1hvzjxZ3xdIRRSCa_U85-R2hlPnvrjmzKOkHJR25htIUvWU6ed',
            'AlidNWupJQ3LBXwrsdPOyQZABUIgM2DKK-YBsxtFg2fzS7Z3OODe8qch9bY_L-8b',
            'Aj8JcDoLz-yzBPWdFfjfBA4JumEqoctsCCve125saCIORnamC8hczkd_TJzJCrU0',
            'AovCgQeiajMrTTrKAenww98DJjv6n3RSf2LOrTss906HXhG8PoEDRkpdrWnw800j',
            'Aqb6EmzSH23ZLT6OZIorPLSkpRlmjOcr1zR8K2pNRqnSctJL9uiT9CFAZxpAfGnf',
            'AmVH3OXxhlL4f5uVb4I1sULG6Uki1cMEYtmy2I1hKIMLc4pLfZu-yibEum87FoCf'
            )
key_idx = 0



def getDrivingRoute( lat1, lon1, lat2, lon2 ):
    global keys, key_idx
    src = '{0},{1}'.format( lat1, lon1 )
    dst = '{0},{1}'.format( lat2, lon2 )
    url = 'http://dev.virtualearth.net/REST/v1/Routes?{0}'
    query = {
            'waypoint.1': src,
            'waypoint.2': dst,
            'optimize': 'time', # timeWithTraffic
            'distanceUnit': 'Mile',
            'travelMode': 'Driving',
            'key': keys[key_idx],
            'maxSolutions': '3'
            }
    page = urlopen(url.format( urlencode(query) )).read()
    pageContent = json.loads( page )
    statusCode =  pageContent['statusCode']
    # with open('test.json', 'w') as fout:
        # fout.write(page)
    
    if statusCode == 200:
        cnt = pageContent['resourceSets'][0]['estimatedTotal']
        dist = []
        time = []
        timeWT = []
        for i in range(cnt):
            route = pageContent['resourceSets'][0]['resources'][i]
            dist.append( route['travelDistance'] )
            time.append( route['travelDuration'] )
            timeWT.append( route['travelDurationTraffic'] )
        res = [ sum(dist) / len(dist), sum(time) / len(time), sum(timeWT) / len(timeWT) , len(dist) ]
        return res + dist + time + timeWT
    elif statusCode == 404:
        raise Exception("no resource found, skip")
    else:
        print statusCode, pageContent['statusDescription']
        if key_idx < len(keys):
            key_idx += 1
            return getDrivingRoute(lat1, lon1, lat2, lon2)
        else:
            raise Exception("key exhausted") 
    

    
def getDrivingRouteInfo( params ):
    return getDrivingRoute( params[0], params[1], params[2], params[3] )
            

def parseLine( l ):
    ls = l.strip().split(',')
    routeQueryParam = ( float( ls[0] ), float( ls[1] ), float( ls[2] ), float( ls[3] ) )
    
    # transform the pickup time to this week
    dow = int( ls[4] ) - 1 # Sunday (0) => Monday (0)
    htd = int( ls[5] )
    today = time.localtime()
    
    weekDiff = dow - today.tm_wday # the difference of weekdays in day
    if weekDiff < 0:
        weekDiff += 7
    
    pickupsec = time.mktime( (today.tm_year, today.tm_mon, today.tm_mday, 
            htd, 10, 0, 
            today.tm_wday, today.tm_yday, today.tm_isdst ) )
    pickupsec += weekDiff * 3600 * 24
    
    return pickupsec, routeQueryParam
    

    
def extendTripInfo(fout, l, routeQueryParam):
    try:
        res = getDrivingRouteInfo( routeQueryParam )
        str_res = ','.join( map( str, res ) )
        fout.write("{0},{1}\n".format( l.strip(), str_res ))
    except Exception as e:
        print str(e)
        if str(e) == "key exhausted":
            sys.exit()

    
    
def main_sample_records( n ):
    finName = '../../dataset/goodrecs-new'
    foutName = '../../dataset/goodrecs-new-{0}k'.format(n/1000)
    
    cnt = 0
    with open(finName) as fin, open(foutName, 'w') as fout:
        for l in fin:
            if random.random() < 0.003:
                fout.write(l)
                cnt += 1
                if cnt == n:
                    break
    
            


def main_schedule_job_run():
    s = sched.scheduler( time.time, time.sleep )
    oneweek = 3600 * 24 * 7 # in seconds
    
    finName = '../../dataset/7filter/2013-12.Bing.trip'
    foutName = '../../dataset/7filter/2013-12.Bing-query.trip'
    
    num_lines = 0
    if os.path.exists(foutName):
        with open(foutName) as f:
            num_lines = sum( 1 for l in f )
    
    cnt = 0
    with open(finName) as fintaxi, open(foutName, 'a') as fout:
        t1 = time.time()
        print "Start scheduling"
        for l in fintaxi:
            if cnt >= num_lines:
                t, params = parseLine(l)
                
                if t > time.time() + 30:
                    s.enter(t - time.time() , 1, extendTripInfo, (fout, l, params))
                else:
                    s.enter(t + oneweek - time.time(), 1, extendTripInfo, (fout, l, params ))
            
            cnt += 1
            if cnt % 10000 == 1:
                print cnt
                
        t2 = time.time()
        print "Scheduling finished in {0} seconds".format( t2-t1 )
        print "Event queue length: {0}".format( len(s.queue) )
        
        
        s.run()
    

if __name__ == '__main__':

    # print getDrivingRoute(40.711526, -74.015782, 40.749104, -73.982089)
    
    # main_sample_records( 250000 )
    
    main_schedule_job_run()

            
    
