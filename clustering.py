from sklearn.cluster import *
from scipy.spatial.distance import euclidean
from math import pow
from sklearn import metrics
from numpy import *
from sklearn.neighbors import DistanceMetric
from scipy.stats.stats import pearsonr
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from numpy.fft import fft
from scipy.signal import find_peaks_cwt
import os

from retrieveNTCProfile import Census



def normalize( si ):
    m = float(max( map(abs, si) ))
    si_ = []
    for i in si:
        si_.append( i / m )
    return si_

    

def kmeansVariance( km, X ):
    centers = km.cluster_centers_
    print centers
    labels = km.labels_
    print labels
    paras = km.get_params()
    m = paras['n_clusters']
    n = [ 0 for i in range(m) ]
    for i in labels:
        n[i] += 1
    
    variance = 0
    for i, s in enumerate(X):
        print i, labels[i]
        variance += 1 / float(n[labels[i]] - m) * pow(euclidean(s, centers[labels[i]]), 2)
    return n
    


def retrieveTimeSeries( Tparam = 'hour' ):
    """
    Retrive time series for each neighborhoods
    """        
    X = []
    neighborhoods = []
    names = []
    with open('res/IB_TS_{0}.txt'.format( Tparam) ) as fib, open('res/OB_TS_{0}.txt'.format( Tparam) ) as fob, \
        open('res/DFF_TS_{0}.txt'.format( Tparam) ) as fdif, open('res/order_{0}.txt'.format( Tparam) ) as fname:
        for line in fib:
            si = []
            ls = line.split()
            for attr in ls:
                si.append( float(attr) )
            
            si_ = normalize(si)
            X.append(si_)
        
        for idx, line in enumerate(fob):
            so = []
            ls = line.split()
            for attr in ls:
                so.append( float(attr) )
            
            so_ = normalize(so)
            # X[idx] += so
            # X.append(so_)
            
        for idx, line in enumerate(fdif):
            sd = []
            ls = line.split()
            for attr in ls:
                sd.append( float(attr) )
            
            sd_ = normalize(sd)
            # X[idx] += sd
            # X.append(sd_)
        
        for line in fname:
            ls = line.strip().split('\t', 1)
            neighborhoods.append( ls[0] )
            names.append(ls[1])
            
        return X, neighborhoods, names
    


def KMeansCluster( X, neighborhoods, names ):
    # find the optimal parameter --- number of clusters
    # then apply the K-means clustering based on Euclidean distance
    data = array(X)
    maxscore = 0
    model = None
    for n in range(2,7):
        km = KMeans(n_clusters = n, random_state = 1 )
        km.fit(data)
        score = metrics.silhouette_score(data, km.labels_, metric = 'euclidean')
        if score > maxscore:
            model = km
            maxscore = score
       
       
    n_clusters = model.get_params()['n_clusters']
    print 'K-Means uses {0} clusters.'.format( n_clusters )
    
    
    clusters = { i: [] for i in range(n_clusters) }
    
    for clt in range(n_clusters):
        print '\nCluster {0} \n'.format( clt )
        with open('res/cluster{0}.txt'.format(clt), 'w') as fout:
            for i, l in enumerate( model.labels_ ):
                if l == clt:
                    print neighborhoods[i], names[i]
                    for val in X[i]:
                        fout.write('{0}\t'.format(val))
                    fout.write( '\n' )
                    clusters[clt].append( neighborhoods[i] )
    return clusters
  


def hrchCluster( X, neighborhoods ):
    """
    Find time series based features first, then apply the
    hierarchical clustering method
    """
    data = array(X)
    NUM_CLUSTER = 4
    
    for x in data:
        peak_ind = find_peaks_cwt(x, arange(3, 16))
        np = len(peak_ind)
    
    
    f = fft(data, n=512)
    f = absolute(f[0:255])
    feats = list(f)
    plt.plot(f[0])
    plt.show()
    feats = feats.append(np)
   
    hc = AgglomerativeClustering(n_clusters = NUM_CLUSTER, compute_full_tree=True)
    hc.fit(data)
    
    print 'Hiearchicla clustering uses {0} clusters.'.format( NUM_CLUSTER )
    clusters = { i: [] for i in range(NUM_CLUSTER) }
    
    for clt in range(NUM_CLUSTER):
        print '\nCluster {0} \n'.format( clt )
        with open('res/h_cluster{0}.txt'.format(clt), 'w') as fout:
            for i, l in enumerate( hc.labels_ ):
                if l == clt:
                    print neighborhoods[i]
                    for val in X[i]:
                        fout.write('{0}\t'.format(val))
                    fout.write( '\n' )
                    clusters[clt].append( neighborhoods[i] )
                    
                    
    return clusters, hc



  

def clusterProperty( c, clusters, Tparam ):
    """
    Find the averaged feature of each cluster, and write in file
    """
    avg = {}
    
    with open('res/avg-{0}.txt'.format(Tparam), 'w') as fout:
        for i in clusters:
            avg[i] = c.aggregate( clusters[i] )
            fout.write('cluster {0}\n'.format(i) )
            fout.write(str(avg[i]))
    return avg

    
    
    
def pairwise_featureSimilarity( regions_ts, regions_census, neighborhoods1, neighborhoods2 = None ):

    dist = DistanceMetric.get_metric('euclidean')
    dist_ts = []
    dist_age = []
    dist_race =[]
    dist_employ = []
    dist_income = []
    
    for i in range(len(neighborhoods1)):
        i_idx = neighborhoods1[i]
        if neighborhoods2 == None:
            j_range = range( i+1, len(neighborhoods1) )
        else:
            j_range = range(len(neighborhoods2))
            
        for j in j_range:
            if neighborhoods2 == None:
                j_idx = neighborhoods1[j]
            else:
                j_idx = neighborhoods2[j]

            p_ts = [ regions_ts[i_idx], regions_ts[j_idx] ]
            dist_ts.append( dist.pairwise(p_ts)[0][1] )
            
            p_age = [regions_census[i_idx].age, regions_census[j_idx].age ]
            dist_age.append( dist.pairwise(p_age)[0][1] )
            
            p_race = [regions_census[i_idx].race, regions_census[j_idx].race]
            dist_race.append( dist.pairwise(p_race)[0][1] )
            
            p_employ = [regions_census[i_idx].employ, regions_census[j_idx].employ ]
            dist_employ.append( dist.pairwise(p_employ)[0][1] )
            
            p_income = [regions_census[i_idx].income, regions_census[j_idx].income ]
            dist_income.append( dist.pairwise(p_income)[0][1] )    
    
    return [dist_ts, dist_age, dist_race, dist_employ, dist_income]
    
    
    

def correlationPlot( regions_ts, regions_census, clusters ):  

    DISTS1 = pairwise_featureSimilarity(regions_ts, regions_census, clusters[0])
    DISTS2 = pairwise_featureSimilarity(regions_ts, regions_census, clusters[1])
    DISTS3 = pairwise_featureSimilarity(regions_ts, regions_census, clusters[0], clusters[1])
    LABELS = ['TS', 'AGE', 'RACE', 'EMPLOY', 'INCOME' ]
    for i in range(5):
        for j in range(i+1, 5):
            plt.figure()
            l1 = plt.scatter(DISTS1[i], DISTS1[j], c='b')
            l2 = plt.scatter(DISTS2[i], DISTS2[j], marker='x', c='r')
            l3 = plt.scatter(DISTS3[i], DISTS3[j], marker='^', c='y')
            
            cc1 = pearsonr(DISTS1[i], DISTS1[j])
            cc2 = pearsonr(DISTS2[i], DISTS2[j])
            cc3 = pearsonr(DISTS3[i], DISTS3[j])
            plt.figlegend( (l1, l2, l3), ('Cluster 0 -- {0}'.format(cc1), 
                    'Cluster 1 -- {0}'.format(cc2), 'Cluster 0 & 1 -- {0}'.format(cc3)), 
                    loc = 'upper right')
            
            plt.xlabel( LABELS[i] )
            plt.ylabel( LABELS[j] )
            plt.show(block=False)
            plt.savefig(''.join( [LABELS[i], '-', LABELS[j], '.pdf'] ), format='pdf')


            
def writeScaler( s, fout ):
    for i, m in enumerate(s.mean_):
        fout.write('{0}\t{1}\n'.format(m, s.std_[i]))
    fout.write('\n')
            
def meanVariance( regions_census, clusters ):
    scaler = StandardScaler()
    with open('temp_res.txt', 'w') as fout:
        for i in clusters:
            rid = clusters[i]
            ages = [ regions_census[i].age for i in rid ]
            a = scaler.fit(ages)
            writeScaler(a, fout)
            
            races = [ regions_census[i].race for i in rid ]
            a = scaler.fit(races)
            writeScaler(a, fout)
            
            employs = [ regions_census[i].employ for i in rid ]
            a = scaler.fit(employs)
            writeScaler(a, fout)
            
            incomes = [ regions_census[i].income for i in rid ]
            a = scaler.fit(incomes)
            writeScaler(a, fout)
        
    

if __name__ == '__main__':
    Tparam = 'hour'

    # retrieve the time series feature
    X, neighborhoods, names = retrieveTimeSeries( Tparam )
    regions_ts = { neighborhoods[i] : X[i] 
                for i in range(len(neighborhoods)) if neighborhoods[i] != '-1'}
    X = [ X[i] for i in range(len(neighborhoods)) if neighborhoods[i] != '-1' ]
    neighborhoods = [ neighborhoods[i] for i in range(len(neighborhoods)) if neighborhoods[i] != '-1']
    names = [ names[i] for i in range(len(neighborhoods)) if neighborhoods[i] != '-1']
    
    # retrieve the census features
    c = Census()
    c.getRegions()
    regions_census = c.regions
    
  
    # cluster neighborhoods by time series feature
    # option 1
    # K-Means
#    clusters = KMeansCluster( X, neighborhoods, names )   
    # option 2
    clusters, hc= hrchCluster(X, neighborhoods)
    tree_cld = hc.children_
    
    
    with open('res/hierarchical_tree.dot', 'w') as ftree:
        N = len(X)
        ftree.write('digraph G {\n')
        for idx, children in enumerate( tree_cld ):
            for child in children:
                if child < N:
                    ftree.write('{0} -> N{1}\nN{2} [shape=box, label={3} ];\n'.format(
                        idx, child, child, neighborhoods[child]))
                else:
                    child = child - N
                    ftree.write('{0} -> {1};\n'.format(idx, child))
        ftree.write('}\n')
        
    
    # plot graph with graphviz -> dot
    os.system('dot -Tpdf res/{0}.dot -o res/{1}.pdf'.format('hierarchical_tree', 'hierarchical_tree'))
    
    
    avg = clusterProperty( c, clusters, Tparam )

    # meanVariance(regions_census, clusters)
    
    # similarity analysis
#    correlationPlot( regions_ts, regions_census, clusters )
