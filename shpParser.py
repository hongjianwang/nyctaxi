import shapefile
from xml.etree.ElementTree import Element, tostring



class NTA:
    """Neighborhood Tabulation Area"""
    
    def __init__( self, shp, rec ):
        self.id = rec[3]
        self.title = rec[4]
        
        self.boundary = []
        for idx, p_start in enumerate(shp.parts):
            ring = []
            if len(shp.parts) > idx + 1:
                p_end = shp.parts[idx+1]
            else:
                p_end = len(shp.points)
            for i in range(p_start, p_end):
                point = shp.points[i]
                ring.append( (point[0], point[1]) )
            
            self.boundary.append(ring)
            
            
    def saveToFile(self, f):
        try:
            root = Element('neighborhood')
            idNode = Element('id')
            idNode.text = self.id
            root.append(idNode)            
            titleNode = Element('title')
            titleNode.text = self.title
            root.append(titleNode)            
            
            for ring in self.boundary:
                boundary = Element('polygon')
                root.append(boundary)
                for x, y in ring:
                    point = Element('point')
                    lonNode = Element('lon')
                    lonNode.text = str(x)
                    latNode = Element('lat')
                    latNode.text = str(y)
                    point.append(lonNode)
                    point.append(latNode)
                    boundary.append(point)
                    
            f.write( tostring(root, method='xml') )
        except Exception as e:
            print self.id, e
            
                
            

if __name__ == '__main__':
    sf = shapefile.Reader('../../dataset/nynta_14c/wgs84.shp')
    
    shps = sf.shapes()
    recs = sf.records()
    cnt = 0
    
    ntas = []
    
    for i, s in enumerate(shps):
        rec = recs[i]
        if rec[1] == 'Manhattan':
            cnt += 1
            n = NTA(s, rec)
            ntas.append(n)
            
    print cnt
            
    with open('data/NTA.xml', 'w') as fout:
        fout.write("<?xml version='1.0' encoding='UTF-8'?>\n<neighborhoods>\n")
        for n in ntas:
            n.saveToFile(fout)
        fout.write('</neighborhoods>\n')
            
    
