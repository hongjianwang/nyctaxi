import os


fin = open('res/traffic.graph')
g = eval(fin.read())
with open('res/tg.dot', 'w') as fout:
    fout.write('digraph G {\n')
    for i, row in enumerate(g):
        for j in range(i+1, len(g)):
            fout.write( '"{0}" -> "{1}" [label ="{2}"];\n'.format(i, j, g[i][j]))
    fout.write('}')       
    
    
os.system('dot -Tpdf res/tg.dot -o res/tg.pdf')