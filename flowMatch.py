from shapely.geometry import Polygon, Point, MultiPolygon
from xml.etree.ElementTree import parse
from retrieveGeo import generateFileName
import os
from time import strptime



def queryIDFromElement( ele ):
    """Return the ID of neighborhood from element object"""
    return ele.find('id').text

class Neighborhood:

    def __init__( self, id = -1, title = 'others', polygon = None, t_interval = "day" ):
        self.id = id
        self.title = title
        self.polygon = polygon
        self.t_interval = t_interval
        self.outbound = {}
        
    
    def createFromWikimapia( self, ele ):
        """Generate an Neighborhood instance from the boundary.xml element"""
        self.id = ele.find("id").text
        self.title = ele.find("title").text
        coords = []
        for e in list(ele.find("polygon")):
            lon = float(e.find("lon").text)
            lat = float(e.find("lat").text)
            coords.append( (lon, lat) )
            
        self.polygon = Polygon(coords)
        
        return self
        
        
    def createFromNYCNTA( self, ele ):
        """Generate an Neighborhood instance from the NTA.xml element"""
        self.id = ele.find('id').text
        self.title = ele.find('title').text
        coords = []
        for polygon in ele.findall('polygon'):
            ring = []
            for e in list(polygon):
                lon = float(e.find('lon').text)
                lat = float(e.find('lat').text)
                ring.append( (lon, lat) )
            coords.append( (ring, []) )
        
        self.polygon = MultiPolygon( coords )
        
        return self
        
        
        
        
    def outBoundInc( self, dst, hour_idx = -1 ):
        """Increase the outbound count by 1.
           hour_idx = -1   ---  count out bound flow by day
           hour_idx != -1  ---  the flow falls in the indexed hour interval"""
        if hour_idx == -1:
            if dst.id in self.outbound:
                self.outbound[dst.id] += 1
            else:
                self.outbound[dst.id] = 1
        else:
            if dst.id in self.outbound:
                self.outbound[dst.id][hour_idx] += 1
            else:
                self.outbound[dst.id] = [ 0 for i in range(24) ]
                self.outbound[dst.id][hour_idx] = 1
            
        
        
        
            
    def contains( self, point ):
        return self.polygon.contains(point)
        
    def totalOutBoundFlow( self ):
        """Return outbound flow of one neighborhood
            If time interval is day, return an integer flow
            If time interval is hour, return a list of [ flow_1, ... , flow_24 ]"""
        if self.t_interval == 'day':
            OB = 0
            for dst in self.outbound:
                OB += self.outbound[dst]
        elif self.t_interval == 'hour':
            OB = [ 0 for i in range(24) ]
            for dst in self.outbound:
                for hour_idx in range(24):
                    OB[hour_idx] += self.outbound[dst][hour_idx]
                
        return OB
  



class Manhattan:
    
    IDsPath = 'data/Manhattan.txt'
    # boundaryPath = 'data/boundary.xml'
    boundaryPath = 'data/NTA.xml'
    neighborhoods = {}  # all the neighborhoods in Manhattan
    
    @classmethod
    def getNeighborhoods( cls, t_interval = 'day', boundary = 'NTA' ):
        manhattan = []
        cls.t_interval = t_interval
        tree = parse(cls.boundaryPath)
        
        if boundary == 'wikimapia':
            with open(cls.IDsPath) as manhin:
                for line in manhin:
                    manhattan.append(line.strip())
     
            for node in list(tree.getroot()):
                if queryIDFromElement(node) in manhattan:
                    neighborhood = Neighborhood( t_interval = cls.t_interval )
                    neighborhood.createFromWikimapia( node )
                    cls.neighborhoods[neighborhood.id] = neighborhood
        elif boundary == 'NTA':
            for node in list(tree.getroot()):
                neighborhood = Neighborhood( t_interval = cls.t_interval )
                neighborhood.createFromNYCNTA( node )
                cls.neighborhoods[neighborhood.id] = neighborhood
                    
                 
        cls.oth = Neighborhood( t_interval = cls.t_interval )
        cls.neighborhoods[-1] = cls.oth
        return cls.neighborhoods, cls.oth
        
        
        
    @classmethod
    def addPoint( cls, line ):
        ls = line.split(',')
        pickup = Point(float(ls[10]), float(ls[11]))    # lon, lat
        dropoff = Point(float(ls[12]), float(ls[13]))
        src = None
        dst = None
        srcflag = False
        dstflag = False
        for n in cls.neighborhoods:
            if not srcflag and n != -1 and cls.neighborhoods[n].contains( pickup ):
                src = cls.neighborhoods[n]
                srcflag = True
            if not dstflag and n != -1 and cls.neighborhoods[n].contains( dropoff ):
                dst = cls.neighborhoods[n]
                dstflag = True
            if srcflag and dstflag:
                break
                
        if not srcflag:
            src = cls.oth
        if not dstflag:
            dst = cls.oth
        
        if cls.t_interval == 'day':
            src.outBoundInc( dst )
        elif cls.t_interval == 'hour':
            pickupT = strptime( ls[5], '%Y-%m-%d %H:%M:%S' )
            hour_idx = pickupT.tm_hour
            src.outBoundInc(dst, hour_idx)
            
        

    @classmethod
    def totalInBoundFlow( cls ):
        """Return inbound flow
            If time interval is day, return a dictionary of { regID: flow }
            If time interval is hour, return a dictionary of { regID: [ flow_1, ... , flow_24 ] }"""
        inbound = {}
        if cls.t_interval == 'day':
            for n in cls.neighborhoods:
                for dst in cls.neighborhoods[n].outbound:        
                    if dst in inbound:
                        inbound[dst] += cls.neighborhoods[n].outbound[dst]
                    else:
                        inbound[dst] = cls.neighborhoods[n].outbound[dst]
        elif cls.t_interval == 'hour':
            for n in cls.neighborhoods:
                n_out = cls.neighborhoods[n].outbound
                for dst in n_out:
                    for hour_idx in range(24):
                        ob_h = n_out[dst]                        
                        if dst in inbound:
                            inbound[dst][hour_idx] += ob_h[hour_idx]
                        else:
                            inbound[dst] = [ 0 for i in range(24) ]
                            inbound[dst][hour_idx] = ob_h[hour_idx]
                    
        return inbound
    
    
    @classmethod
    def getTrafficGraph( cls ):
        """Return traffic transition graph
            Size is n by n, where n is number of neighborhoods.
            All data will be aggregated into one network"""
        nn = len(cls.neighborhoods)
        G = [ [] for i in range(nn) ]
        ID = { id: i for i, id in enumerate(cls.neighborhoods) }
        if cls.t_interval == 'day':
            for src_id in cls.neighborhoods:
                for dst_id in cls.neighborhoods[src_id].outbound:
                    traf = cls.neighborhoods[src_id].outbound[dst_id]
                    if len(G[ID[src_id]]) == 0:
                        G[ID[src_id]] = [ 0 for i in range(nn) ]
                    
                    G[ID[src_id]][ID[dst_id]] += traf
                    
        elif cls.t_interval == 'hour':
            for src_id in cls.neighborhoods:
                for dst_id in cls.neighborhoods[src_id].outbound:
                    traf = sum( cls.neighborhoods[src_id].outbound[dst_id] )
                    if len(G[ID[src_id]]) == 0:
                        G[ID[src_id]] = [ 0 for i in range(nn) ]
                    
                    G[ID[src_id]][ID[dst_id]] += traf
        
        cls.G = G
        return ID, G
        
        
        
    
def dotPlot( neighborhoods, fname ):
     # generate dot file as graph descriptor
    with open('res/{0}.dot'.format(fname), 'w') as fout:
        fout.write('digraph G {\n')
        for n in neighborhoods:
            for dstid in neighborhoods[n].outbound:
                weight = neighborhoods[n].outbound[dstid]
                if weight >= 1500:
                    fout.write( '"{0}" -> "{1}" [label ="{2}"];\n'.format(neighborhoods[n].title, neighborhoods[dstid].title, weight) )
        fout.write('}')       
    
    
    # plot graph with graphviz -> dot
    os.system('dot -Tpdf res/{0}.dot -o res/{1}.pdf'.format(fname, fname))


if __name__ == '__main__':

    neighborhoods, oth = Manhattan.getNeighborhoods(t_interval = 'hour')     # other option 'hour'

    
    inbound = {}
    outbound = {}
    
    for i in range(1,2): #32):       # default 1,32 meaning day 1 to day 31
        finName = generateFileName(i)
            
        with open(finName) as fintaxi:
            cnt = 0
            # head = [next(fintaxi) for i in range(5000)]
            for l in fintaxi:
                Manhattan.addPoint(l)
                cnt += 1
                if cnt % 1000 == 1:
                    print cnt
        
        # generate dot file and plot the pdf with Graphviz
        # dotPlot( neighborhoods, 'graphP-{0}'.format(i) )

        
        IB = Manhattan.totalInBoundFlow( )
        for nid in IB:
            ibf = IB[nid]
            if isinstance( ibf, int ):
                if nid in inbound:
                    inbound[nid].append(IB[nid])
                else:
                    inbound[nid] = [IB[nid]]
            elif isinstance( ibf, list ):
                if nid in inbound:
                    inbound[nid] += ibf
                else:
                    inbound[nid] = ibf
        
        for n in neighborhoods:
            obf = neighborhoods[n].totalOutBoundFlow()
            if isinstance( obf, int ):
                if n in outbound:
                    outbound[n].append( obf )
                else:
                    outbound[n] = [ obf ]
            elif isinstance( obf, list ):
                if n in outbound:
                    outbound[n] += obf     # list concatenate
                else:
                    outbound[n] = obf
            neighborhoods[n].outbound.clear()
        
        
    
    # write out the time series of inbound and outbound
    Tparam = Manhattan.t_interval
    with open('res/IB_TS_{0}.txt'.format( Tparam ), 'w') as f1, open('res/OB_TS_{0}.txt'.format( Tparam ), 'w') as f2, \
        open('res/DFF_TS_{0}.txt'.format( Tparam ), 'w') as f3, open('res/order_{0}.txt'.format( Tparam ), 'w') as f4:
        for nid in inbound:
            print nid
            f4.write('{0}\t{1}\n'.format(nid, neighborhoods[nid].title))
            for idx, d in enumerate(inbound[nid]):
                f1.write('{0}\t'.format(d))
                f2.write('{0}\t'.format(outbound[nid][idx]))
                f3.write('{0}\t'.format(d - outbound[nid][idx]))
            f1.write('\n')
            f2.write('\n')
            f3.write('\n')
          
            
    ID, G = Manhattan.getTrafficGraph()
    with open('res/traffic.graph', 'w') as fout:
        fout.write(str(ID) + '\n')
        fout.write(str(G))
            