class Trip:
    cnt = 0                                                             
    def __init__(self, line):                                                      
        Trip.cnt += 1
        ls = line.strip().split(',')
        self.pickup = [float(ls[0]), float(ls[1])]   # latitude, longitude      
        self.dropoff = [float(ls[2]), float(ls[3])]                            
#        self.t = float(ls[3])                   # Reflection: why take log on this? <=====
#        self.d = float(ls[4])                                                   
#        self.l1 = float(ls[1])                                
        self.id = int(ls[6])
        self.bingT = float( ls[8] )
        self.bingTwithTraffic = float( ls[9] )

if __name__ == '__main__':
    finName = '../../dataset/7filter/2013-12.Bing-test'
    with open(finName) as fin, open('test', 'w') as fout:
        for l in fin:
            t = Trip(l)
            fout.write('{0},{1},{2}\n'.format(t.bingT, t.bingTwithTraffic,  t.id))
            if Trip.cnt % 10000 == 0:
                print Trip.cnt
       

