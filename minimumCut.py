from flowMatch import Manhattan
from retrieveGeo import generateFileName
import networkx as nx
from networkx.algorithms.flow import edmonds_karp



if __name__ == '__main__':

    neighborhoods, oth = Manhattan.getNeighborhoods()
    
    
    finName = generateFileName(1)
            
    with open(finName) as fintaxi:
        cnt = 0
        head = [next(fintaxi) for i in range(2000)]
        for l in head:
            Manhattan.addPoint(l)

    
    # create the graph
    G = nx.DiGraph()
    for nid in neighborhoods:
        nb_outflow = neighborhoods[nid].outbound
        for oid in nb_outflow:
            G.add_edge( nid, oid, capacity = nb_outflow[oid] )
            
            
    # cut the graph
    G2 = G.to_undirected()
    edges = nx.minimum_edge_cut(G2)
    for e in edges:
        G2.remove_edge( e[0], e[1] )
        
    t = nx.connected_components(G2)
    for i in list(t):
        for j in i:
            print('{0}\t'.format(j))
        print ''
    
            
            
            
            