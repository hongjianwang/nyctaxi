from shapely.geometry import Polygon, Point
from retrieveGeo import generateFileName
from heapq import *
import numpy as np
from math import log, exp, sqrt, pi
from retrieveMapDT import getDrivingRoute
import time
import random



class Grid:
    """
    One Gird is the smallest location unit of study
    """
    
    def __init__( self, southwest, height, width ):
        self.southwest = southwest
        self.northeast = [southwest[0] + height, southwest[1] + width]
        self.pickup = [{} for i in range(24)]
        self.dropoff = {}
        

    def mergeGrid( self, g ):
        self.pickup.update(g.pickup)
        self.dropoff.update(g.dropoff)
        
        
    def center( self ):
        lat = (self.southwest[0] + self.northeast[0]) / 2.0
        lon = (self.southwest[1] + self.northeast[1]) / 2.0
        return [lat, lon]
        
        
    
class Raster:
    """
    The Manhattan of NYC is represented as a Raster
    
    A Raster is consisted of many Grids      
    """
    SouthWest = [ 40.680120, -74.061088 ]       # latitude, longitude
    NorthEast = [ 40.879000, -73.777503 ]
    
    speedRef = [ 0 for i in xrange(24) ]
    speedRefCnt = [0 for i in xrange(24) ]
    
    def __init__( self, H = 500, W = 500):
        self.raster = {}
        self.Grids = []
        self.gridH = (Raster.NorthEast[0] - Raster.SouthWest[0] ) / H
        self.gridW = (Raster.NorthEast[1] - Raster.SouthWest[1] ) / W
        # for i in range(H):
            # rows = []
            # for j in range(W):
                # org = [ Raster.SouthWest[0] + i * self.gridH, Raster.SouthWest[1] + j * self.gridW ]
                # rows.append( Grid(org, self.gridH, self.gridW))                
            # self.raster.append(rows)
     
    
    def updateSpeedReference( self, t ):
        Raster.speedRef[t.hourIdx] += t.v
        Raster.speedRefCnt[t.hourIdx] += 1
        
        
    def getGrid(self, rowID, colID):
        ''' return the grid indexed by rowID and columnID, if it exists
            otherwise, create that grid and return it.'''
        if rowID in self.raster:
            if colID in self.raster[rowID]:
                return self.raster[rowID][colID], True
            else:
                org = [ Raster.SouthWest[0] + rowID * self.gridH, Raster.SouthWest[1] + colID * self.gridW ]
                g = Grid(org, self.gridH, self.gridW)
                self.Grids.append( g )
                self.raster[rowID][colID] = g
                return g, False
        else:
            org = [ Raster.SouthWest[0] + rowID * self.gridH, Raster.SouthWest[1] + colID * self.gridW ]
            g = Grid(org, self.gridH, self.gridW)
            self.Grids.append( g )
            self.raster[rowID] = {}
            self.raster[rowID][colID] = g
            return g, False

            
            
    def addPoint( self, line ):
        t = Trip(line)
        self.updateSpeedReference(t)
        
        self.indexTrip( t )
        
        return t
        
   
        
    def conditioned_addingPoing( self, line ):
        t = Trip(line)
        self.updateSpeedReference(t)
        
        r1, c1 = self.mapCoordinate( t.pickup )
        r2, c2 = self.mapCoordinate( t.dropoff )
        
        if self.tripGridExist( r1, c1, r2, c2 ):
            self.indexTrip(t)
            return t
        return None
        
        
    
        


    def indexTrip(self, t):
        r, c = self.mapCoordinate( t.pickup )    # lat, lon
        g, flag = self.getGrid( r, c )
        g.pickup[t.hourIdx][t.id] = t
        t.pg = g
        
        r, c = self.mapCoordinate( t.dropoff )
        g, flag = self.getGrid( r, c )  # lat, lon
        g.dropoff[t.id] = t
        t.dg = g
      
      
    def reverseIndexTrip(self, t):
        r, c = self.mapCoordinate( t.pickup )   # build and return corresponding grid
        g, flag = self.getGrid( r, c )
        t.pg = g
        
        r, c = self.mapCoordinate( t.dropoff )
        g, flag = self.getGrid( r, c )
        t.dg = g
        
        
      
    def mapCoordinate( self, coords ):
        '''lat, lon => grid rowID, colID'''
        rowID = int( ( coords[0] - Raster.SouthWest[0] ) / self.gridH )
        colID = int( ( coords[1] - Raster.SouthWest[1] ) / self.gridW )
        return rowID, colID
        
    
    def tripGridExist( self, r1, c1, r2, c2 ):
        '''judge whether the src/dst grids of trip exist'''
        f1 = r1 in self.raster and c1 in self.raster[r1]
        f2 = r2 in self.raster and c2 in self.raster[r2]
        
        return f1 and f2
        
        
        
        

class Trip:
    cnt = 0
    def __init__(self, line):
        self.id = Trip.cnt
        Trip.cnt += 1
        segs = line.strip().split('\t')
        ls = segs[1].split(',')
        self.pickup = [float(ls[8]), float(ls[7])]   # latitude, longitude
        self.dropoff = [float(ls[10]), float(ls[9])]
        self.t = float(ls[3])                   # Reflection: why take log on this? <=====
        
        self.hourIdx = time.strptime(ls[5], "%Y-%m-%d %H:%M:%S").tm_hour
        self.v = self.getSpeed( ls[0], ls[3] )
        # self.d = float(ls[0])
        # self.l1 = float(ls[1])
        # self.bt = float(segs[3])
        # self.bd = float(segs[2])
        
        
    def matchSD(self, src, dst):
        return src.contains(Point( self.pickup )) and dst.contains(Point( self.dropoff ))
        
        
    def getWeightedT( self, trip ):
        w = trip.l1 / self.l1
        return w * trip.t, w
        
    
    def estimateTD( self, Trajs, method = 'avg' ):
        T = 0
        Tv = 0
        if method == 'avg':
            Ts = np.array( [ traj.t for traj in Trajs ] )
            T = Ts.mean()
            Tv = Ts.var()
#            Ds = np.array( [ traj.d for traj in Trajs ] )
#            D = Ds.mean()
#            Dv = Ds.var()
        elif method == 'weight':
            Ts = np.array( [ traj.t for traj in Trajs ] )
            vref = Raster.speedRef[self.hourIdx] / Raster.speedRefCnt[self.hourIdx]
            for trip in Trajs:
                w = Raster.speedRef[trip.hourIdx] / Raster.speedRefCnt[trip.hourIdx]
                T += w * trip.t
            T = T / len(Trajs) / vref
        return T
        
        
    def getSpeed( self, dstr, tstr ):
        d = float(dstr)
        t = float(tstr)
        return d / t * 3660
        


def writeRegressionFile():
    '''
    Regression of
    L1 distance and actual travel time
    '''
    finName = '../../dataset/goodrecs'
    with open(finName) as fintaxi, open('res/reg', 'w') as fout:
        for line in fintaxi:
            ls = line.strip().split('\t')
            ls = ls[1].split(',')
            t = float(ls[3])
            d = float(ls[0])
            l1 = float(ls[1])
            fout.write('{0}\t{1}\n'.format(l1, t))





def main_CrossValidation_time():
    Ttrain = []
    Ttest = []
    
    finName = '../../dataset/training2'
    trn_id = '../../dataset/training2_idx'
    
    trainLines = []
    
    with open(finName) as fin, open('res/paircv.txt', 'w') as fout, open(trn_id, 'w') as ftid:
        r = Raster()
        
        print 'build testing trips and create grids'
        cnt = 1
        for l in fin:
            if random.random() < 0.1:
                t = Trip(l)
                r.reverseIndexTrip(t)
                Ttest.append(t)
            else:
                trainLines.append(l)
                ftid.write(str(cnt) + '\n')
            
            cnt += 1
                
        print 'process training trips'
        for l in trainLines:
            t = r.conditioned_addingPoing(l)
            if t != None:
                Ttrain.append(t)
            if Trip.cnt % 50000 == 0:
                print Trip.cnt, len(Ttrain)
                
                
        cnt = 0
        print 'make prediction'
        for t in Ttest:
            gsrc = t.pg
            gdst = t.dg
            
            ts = []
            for i in range(24):
#                Trajs = []
                for id in gsrc.pickup[i]:
                    if gsrc.pickup[i][id].dg == gdst:
                        ts.append( gsrc.pickup[i][id] )
                        
                        
            if len(ts) > 0:
                fout.write( '\t'.join(map(str, [t.estimateTD(ts, 'avg'), t.estimateTD(ts, "weight"), len(ts)])) + '\n' )
            else:
                fout.write('-1\t-1\t0\n')
                    
                
            cnt += 1
            if cnt % 50000 == 0:
                print cnt
                
  

def main_testWith_Bing():
    Ttrain = []
    Ttest = []
    
    # finName = '../../dataset/NYC_Taxi/manh_nonfiltered_feature_dist_L1_info1'
    finName = '../../dataset/training2'
    testFN = '../../dataset/goodrecs-wID-250k-mapinfo'

    with open(finName) as fintaxi, open('res/pair000.res', 'w') as fout, open(testFN) as ftest:
            
        r = Raster()
        print 'Build testing file and create grids'
        for l in ftest:
            t = Trip(l)
            r.reverseIndexTrip( t )
            Ttest.append(t)
            
        
        # head = [next(fintaxi) for i in range(2000)]
        for l in fintaxi:
            t = r.conditioned_addingPoing( l )
            if t != None:
                Ttrain.append(t)
            if Trip.cnt % 50000 == 0:
                print Trip.cnt, len(Ttrain)
    
        
        
        cnt = 0
        # go through all the testing trips
        for t in Ttest:
            gsrc = t.pg
            gdst = t.dg
            
            ts = []
            for i in range(24):
#                Trajs = []
                for id in gsrc.pickup[i]:
                    if gsrc.pickup[i][id].dg == gdst:
                        ts.append( gsrc.pickup[i][id] )
#                        Trajs.append( gsrc.pickup[i][id] )
                # if len(Trajs) > 1:
                    # t = np.array( [x.t for x in Trajs] )
                    # ts.append( t.mean() )
                # else:
                    # ts.append(0)
                    
            # if sum(ts) > 0:
                # fout.write( '\t'.join( map( str, ts ))+ '\n') 
            
            if len(ts) > 0:
                fout.write( '\t'.join(map(str, [t.estimateTD(ts, 'avg'), t.estimateTD(ts, "weight"), len(ts)])) + '\n' )
            else:
                fout.write('-1\t-1\t0\n')
                    
                
            cnt += 1
            if cnt % 50000 == 0:
                print cnt
                
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
            # dist, time = getDrivingRoute( t.pickup[0], t.pickup[1], \
                    # t.dropoff[0], t.dropoff[1] )
            # f1.write('{0}\t{1}\t{2}\t{3}\n'.format( t.pickup[0], t.pickup[1], \
                    # t.dropoff[0], t.dropoff[1] ))
            # f1.write('{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\n'.format( \
                    # t.pickup[0], t.pickup[1], t.dropoff[0], \
                    # t.dropoff[1], dist, t.d, time, t.t ))
            # TT, Tv, DD, Dv = t.estimateTD(Trajs, 'avg')
            
            # fout.write('{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\n'.format(len(Trajs), TT, Tv, DD, Dv, t.t, t.d, t.l1))
              

if __name__ == '__main__':
    # src = Polygon( [(40.761783, -73.979087), (40.760418, -73.975772), (40.761068, -73.975418), \
        # (40.762433, -73.978733)] )
    # dst = Polygon( [(40.750243, -73.994869), (40.749072, -73.991994), (40.750308, -73.991087), \
        # (40.751503, -73.993936)] )
    
    main_CrossValidation_time()