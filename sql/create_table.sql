create DATABASE if not exists shtaxi;

create user 'shtaxi'@'%' identified by 'shtaxi';
grant all on shtaxi.* to 'shtaxi'@'%';


use shtaxi;


create table if not exists trip (
	id INTEGER NOT NULL UNIQUE,
	duration FLOAT,	-- in seconds
	pickupRecID INTEGER REFERENCES record(id),
	dropoffRecID INTEGER REFERENCES record(id),
	primary key (id)
);

create table if not exists record (
	id INTEGER NOT NULL AUTO_INCREMENT,
	latitude FLOAT,
	lontitude FLOAT,
	time DATETIME,
	tripid INTEGER NOT NULL REFERENCES trip(id),
	PRIMARY KEY (id)
);
