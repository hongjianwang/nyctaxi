"""
For IST 557 Project

dependancy:

-----------------------------
res/regressFeed.txt
    This file is created in retrieveNTCProfile.py
    
-----------------------------
res/traffic.graph
    This file is created in flowMatch.py
    
    
res/tweet.graph
    This file is created in flowMatchTweets.py
    
"""

import xlrd
from sklearn.linear_model import LinearRegression, Ridge
from sklearn.tree import DecisionTreeRegressor
from sklearn.svm import SVR

from sklearn.metrics import mean_absolute_error as l1error
from sklearn.metrics import mean_squared_error as l2error
from sklearn.metrics import r2_score

from numpy import array, zeros, mean, concatenate, sort, float_
from sklearn.cross_validation import KFold, ShuffleSplit
        
        
from clustering import retrieveTimeSeries, normalize

import matplotlib.pyplot as plt
from numpy import sqrt
from sklearn.preprocessing import scale


def plotAvgIncome( income ):
    t = list( sort(income) )
    t.reverse()
    f1 = plt.figure()
    plt.plot(t, linewidth=3)
    plt.show(block=False)
    plt.xlabel('Neighborhood Tabulation Areas', fontsize='xx-large')
    plt.ylabel('Income', fontsize='xx-large')
    plt.savefig('res/income.pdf', bbox_inches='tight')

    

def evaluateError( rm, kfold, X, Y, FeatureName ):
    '''
    regression model, K-fold, X, Y, FeatureName
    '''
    
    # Y_raw = Y
            
            
    l1ers = []
    mse = []
    # mre = []
    # mre1 = []
    # for i in range(Y_raw.shape[1]):
        # Y = Y_raw[:,i]
        
    for i in range(NITER):
        for train, test in kfold[i]:
            rm.fit( X[train], Y[train] )
            y = rm.predict( X[test] )
            
            y.reshape( (1, y.size) )
            y_act = array( Y[test] )
            y_act.reshape( (1, y_act.size) )
            # mre.append( meanRelativeError(y_act, y) )
            l1ers.append( l1error(y_act, y) )
            mse.append( l2error(y_act, y) )
            # mre1.append( l1error(y_act, y) / mean(y_act) )
            
            # print '=>', t1, t2
                
    print FeatureName, mean(l1ers) , mean(sqrt(mse)) #, mean(mre), mean(mre1)
    
    
def meanRelativeError( y_act, y_prd ):
    mse = l2error( y_act, y_prd )
    return sqrt( mse ) / mean( y_act )
    
        
if __name__ == '__main__':

    with open('res/regressFeed.txt') as fin:
        ids = eval( fin.readline() )
        
        # Y is average income of each region
        Y = array( eval( fin.readline() ) ) / 100
        # plotAvgIncome(Y)
        
        Age = fin.readline()
        Xa = array( eval(Age) ) / 100
        
        Race = fin.readline()
        Xr = array( eval(Race) ) / 100
        
        Employ = fin.readline()
        Xe = array( eval(Employ) ) / 100
        
        
        
        NITER = 200;
        kf = [ [] for i in range(NITER) ]
        for i in range(NITER):
            kf[i] = ShuffleSplit( n = len( Xa ), n_iter = 5, test_size = 0.2, train_size = 0.8 )
        
        # ========================================
        # Try various models in this section
        # ========================================
        
        # build the linear regression model
        lr = LinearRegression( fit_intercept = True, normalize = False )
        
        # build the Decision Tree regression model
        # lr = DecisionTreeRegressor(max_depth = 2, max_features = 'auto')
        
        # build the Support-Vector Regression
        # lr = SVR()
        
        # build the Ridge Regression Model
        # lr = Ridge(alpha=0.5, fit_intercept=True, solver='auto')
        
        
        # ================================
        # age => income
        # ================================
        
        Xa = scale(Xa)
        evaluateError(lr, kf, Xa, Y, 'Age')
        
        
        
        # ================================
        # race => income
        # ================================
        
        Xr = scale(Xr)
        evaluateError(lr, kf, Xr, Y, 'Ethnicity')
        
        
        # ================================
        # employ => income
        # ================================
        
        Xe = scale(Xe)
        evaluateError(lr, kf, Xe, Y, 'Occupation')
        
        
        
        # ================================
        # traffic time series => income
        # ================================
        X, neighborhoods, names = retrieveTimeSeries( "day" )
        regions_ts = { neighborhoods[i] : X[i] 
                for i in range(len(neighborhoods)) if neighborhoods[i] != '-1'}
                
        Xtts = []
        for id in ids:
            Xtts.append( regions_ts[id] )
            
        # extract weekly time series
        # Xtts = array(Xtts)
        # Xts = zeros(( 29, 7) )
        # counter = zeros(7)
        # for i in range(len(Xtts[0])):
            # Xts[:, i%7] += Xtts[:,i]
            # counter[i%7] += 1.0
            
        # for i in range( len(Xts[0]) ):
            # Xts[:, i] /= counter[i]
            
        Xtts = scale(array(Xtts))
        
        evaluateError(lr, kf, Xtts, Y, 'TrafficTimeSeries')
        
        
        # ===============================================
        # traffic transition volume (Taxi) => income
        # ===============================================
        fin = open('res/traffic.graph')
        ID = eval( fin.readline() )
        G = eval( fin.readline() )
        Gtv = []
        for j, id in enumerate( ids ):
            Gtv.append( G[ID[id]] )
        
        # flow out matrix
        Xflowout = scale( array(Gtv, dtype='float'), axis=0 )
        Xflowin = Xflowout.T
        
        evaluateError( lr, kf, Xflowout, Y, 'TaxiTraffic')
                
        
        # ===============================================
        # traffic transition volume (Twitter) => income
        # ===============================================
        fin = open('res/tweet.graph')
        ID = eval( fin.readline() )
        G = eval( fin.readline() )
        Gtvt = []
        for j, id in enumerate(ids):
            Gtvt.append(G[ID[id]])
        
        # flow out matrix
        Xflowout_tweet = scale( array(Gtvt, dtype='float') )
        Xflowin_tweet = Xflowout_tweet.T
        
        evaluateError(lr, kf, Xflowout_tweet, Y, 'TweetsTraffic')
                
        
        # ===============================================
        # Twitter density => income
        # ===============================================
        with open('res/tweet.density') as fin:
            dmap = eval( fin.read() )
            Xtwden = scale( array( [ dmap[id] for id in ids ], dtype='float' ) )
           
            
        evaluateError( lr, kf, Xtwden, Y, 'TweetsTimeSeries' )
                
            
        # ===============================================
        # Twitter topics => income
        # ===============================================
        with open('res/model-final.result') as fin:
            regionTopics = {}
            for i in range(28):
                id = fin.readline().strip()
                topic = eval(fin.readline())
                regionTopics[id] = topic
                
        Xtopics = scale( array( [ regionTopics[id] for id in ids ] ) )
        
        evaluateError(lr, kf, Xtopics, Y, 'TweetsTopics')
        
        
        
        
        # ================================
        # combine all => income
        # ================================
        Xall = concatenate( (Xa, Xr, Xe, Xtts, Xflowout, Xflowout_tweet, Xtwden, Xtopics), axis=1 )
        
        evaluateError(lr, kf, Xall, Y, 'All')
        
            
        