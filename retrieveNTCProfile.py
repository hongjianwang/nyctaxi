import xlrd
from sklearn.linear_model import LinearRegression



class CensusRegion:
    def __init__(self, id, name):
        self.id = id
        self.name = name
        self.age = [0 for i in range(11, 24)]
        self.race = [0 for i in range(7)]
        self.employ = [0 for i in range(40, 53)]
        self.income = [0 for i in range(62, 72)]
        
        
    def __str__(self):
        res = []
        for a in self.age:
            res.append( str(a) + '\n' )
        res.append('\n')
        
        for a in self.race:
            res.append( str(a) + '\n' )
        res.append('\n')
        
        for a in self.employ:
            res.append( str(a) + '\n' )
        res.append('\n')
        
        for a in self.income:
            res.append( str(a) + '\n' )
        
        return ''.join( res )
        

class Census:

    demoPath = 'data/demographic_NYC.xlsx'
    econPath = 'data/economic_NYC.xlsx'
    soclPath = 'data/social_NYC.xlsx'
    id_columns = range(1, 117, 4)
    race_rows = [44, 45, 46, 51, 59, 64, 65]
    race_fields = {}
    age_rows = range(11, 24)
    age_fields = {}
    employ_rows = range(40, 53)
    employ_fields = {}
    income_rows = range(62, 72)
    income_fields = {}
    
    def __init__( self ):
        xlsbook1 = xlrd.open_workbook( Census.demoPath )
        self.demosheet = xlsbook1.sheet_by_name('Manhattan')
        xlsbook2 = xlrd.open_workbook( Census.econPath )
        self.econsheet = xlsbook2.sheet_by_name('Manhattan')
        xlsbook3 = xlrd.open_workbook( Census.soclPath )
        self.soclsheet = xlsbook3.sheet_by_name('Manhattan')
        self.regions = {}
        # initialize all the regions
        self.getRegions()
    
    
    def getRegions( self ):
        for column in Census.id_columns:
            content = self.demosheet.cell( 5, column )
            content_list = content.value.strip().split( ' ', 1 )
            id = content_list[0]
            name = content_list[1]
            r = CensusRegion( id, name )
            r.age, r.avg_age = self.ageData( column )
            r.race = self.raceData( column )
            r.employ = self.employData( column )
            r.income, r.avg_income = self.incomeData( column )
            self.regions[id] = r
            
        return self.regions.keys()
    
  

  
    def ageData( self, id_column ):
        column = 2 + id_column
        age = []
        avg_age = 0
        w = [2.5, 7, 12, 17, 22, 29.5, 39.5, 49.5, 57, 62, 69.5, 79.5, 85]
        for i, row in enumerate( Census.age_rows ):
            content = self.demosheet.cell(row, column).value
            age.append( content )
            avg_age += w[i] * content / 100.0
        return age, avg_age
    
   
    def raceData( self, id_column ):
        column = 2 + id_column
        race = []
        for row in Census.race_rows:
            content = self.demosheet.cell(row, column).value
            race.append( content )            
        return race
        
        
        
    def employData(self, id_column):
        column = id_column + 2
        employ = []
        for row in Census.employ_rows:
            content = self.econsheet.cell(row, column).value
            employ.append( content )
        return employ
        
        
    def incomeData(self, id_column):
        column = id_column + 2
        income = []
        avg_income = 0
        w = [5000, 12499.5, 19999.5, 29999.5, 42499.5, 62499.5, 87499.5, 
            124999.5, 174999.5, 300000]
        for i, row in enumerate( Census.income_rows ):
            content = self.econsheet.cell(row, column).value
            if isinstance(content, float):
                income.append(content)
            else:
                content = 0
                income.append(0)
            avg_income += content * w[i] / 100.0
        return income, avg_income
        
        
    def aggregate(self, ids ):
        avg_r = CensusRegion(0, 'avg')
        if '-1' not in ids:
            cnt = len(ids)
        else:
            cnt = len(ids) - 1
        for id in ids:
            if id != '-1':
                r = self.regions[id]
                for i, a in enumerate(r.age):
                    avg_r.age[i] += a
                for i, ra in enumerate(r.race):
                    avg_r.race[i] += ra
                for i, e in enumerate(r.employ):
                    avg_r.employ[i] += e
                for i, ic in enumerate(r.income):
                    avg_r.income[i] += ic
                
        # average
        for i, a in enumerate(r.age):
            avg_r.age[i] /= cnt
        for i, ra in enumerate(r.race):
            avg_r.race[i] /= cnt
        for i, e in enumerate(r.employ):
            avg_r.employ[i] /= cnt
        for i, ic in enumerate(r.income):
            avg_r.income[i] /= cnt
            
        return avg_r
        
        
        
        
    def getFields( self ):
        # age fields
        for row in Census.age_rows:
            content = self.demosheet.cell(row, 0).value.strip()
            Census.age_fields[row] = content
            print content
            
        # race fields
        for row in Census.race_rows:
            content = self.demosheet.cell(row, 0).value.strip()
            Census.race_fields[row] = content
            print content
            
        # employ fields
        for row in Census.employ_rows:
            content = self.econsheet.cell(row, 0).value.strip()
            Census.employ_fields[row] = content
            print content
            
        # income fields
        for row in Census.income_rows:
            content = self.econsheet.cell(row, 0).value.strip()
            Census.income_fields[row] = content
            print content
        

if __name__ == '__main__':
    d = Census()
    rs = d.getRegions()
    r0 = d.regions[rs[0]]
    d.getFields()
    
    
    # write out
    Y = []
    Age = []
    Race = []
    Employ = []
    
    del d.regions['MN99']
    
    with open('res/regressFeed.txt', 'w') as fout:
        regKeys = d.regions.keys()
        fout.write( str(regKeys) + '\n' )
        for i in regKeys:
            Y.append( d.regions[i].income )     # not average
        
        fout.write( str(Y) )
        fout.write('\n')
        
        
        for i in regKeys:
            Age.append( d.regions[i].age )
        
        fout.write( str(Age) )
        fout.write('\n')
        
        
        
        for i in regKeys:
            Race.append( d.regions[i].race )
        fout.write( str(Race) )
        fout.write('\n')
        
        
        for i in regKeys:
            Employ.append(d.regions[i].employ)
        fout.write( str(Employ) )
        fout.write('\n')
        
        

    
       
   


