// load the initializer function at the beginning
google.maps.event.addDomListener( window, 'load', initialize );


var tripSet = [];
var shownMarkers = [];
var map;


function initialize() {
	var mapOptions = {
		center: new google.maps.LatLng( 31.213192, 121.454094 ),
		zoom: 12
	};
	map = new google.maps.Map(document.getElementById("map-canvas"),
			mapOptions);
}



function addMarker( point )
{
	var marker = new google.maps.Marker({
			position: point,
			map: map
	});
	shownMarkers.push(marker);
}




function showTripMarkers(event) {
	var points = this.getPath();
	
	for (var i = 0; i < points.getLength(); i++)
	{
		var point = points.getAt(i);

		// add a new marker
		addMarker( point );
	}

}


function removeShownMarkers(event) {
	for ( i = 0; i < shownMarkers.length; i++ )
	{
		shownMarkers[i].setMap(null);
	}
	shownMarkers = [];
}





function plotTrips( trips ) {
	var casetrips = []
	// map one case onto the map
	for (var i = 0; i < trips.length; i++)	// each trip in current case
	{
		var color;
		if (i==0)
			color = '#9370DB';
		else
			color = '#DC143C';
		var googlePoints = [];
		for (var j = 0; j < trips[i].length; j++)	// each GPS sample in Trip
		{
			var p = new google.maps.LatLng( trips[i][j][0], trips[i][j][1] );
			googlePoints.push(p);
		}
		
		var tripPath = new google.maps.Polyline({
			path: googlePoints,
			geodesic: true,
			strokeColor: color,
			strokeOpacity: 1.0,
			strokeWeight: 2,
			editable: true,
			map: map
		});
			
		google.maps.event.addListener(tripPath, 'mouseover', showTripMarkers);
		google.maps.event.addListener(tripPath, 'mouseout', removeShownMarkers);
		
		// save trips in global space.
		casetrips.push(tripPath);
	}
}


function plotCase( idx ) {
	caseTripSet = cases[idx];
	plotTrips(caseTripSet);
}