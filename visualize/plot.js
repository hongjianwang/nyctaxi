// load the initializer function at the beginning
google.maps.event.addDomListener( window, 'load', initialize );

var map;
var isGameDay = true;
var gdtrips = [];
var ngdtrips = [];
var cntClick = 0;
var orgLatLngs = [];
var dstLatLngs = [];
var orgRegion = null;
var drawOrgFlag = -1;
var dstRegion = null;
var drawDstFlag = -1;
var shownMarkers = [];

var testVar = null;

var heatMap = null;
var NTA = {};


function initialize() {
	var mapOptions = {
		center: new google.maps.LatLng( 40.800039, -73.949508 ),
		zoom: 12
	};
	map = new google.maps.Map(document.getElementById("map-canvas"),
			mapOptions);
	getNTAboundary();
}


/* get NTA regions from xml file (using AJAX) */
function getNTAboundary() {
	$.ajax({
		type: 'get',
		url: 'ntaxml',
		datatype: 'xml',
		success: function( xml ) {
			var xmlDoc = $.parseXML(xml);
			
			// initialize each NTA
			$(xmlDoc).find('neighborhood').each( 
				function() {
					var id = $(this).find('id').text();
					var title = $(this).find('title').text();
					var polygons = [];
					$(this).find('polygon').each( function() {
						var polygon = [];
						$(this).find('point').each( function() {
							lat = parseFloat( $(this).find('lat').text() );
							lng = parseFloat( $(this).find('lon').text() );
							var p = new google.maps.LatLng(lat, lng);
							polygon.push(p);
						});
						polygons.push(polygon);
					});
					// plot 
					var nta = new google.maps.Polygon({
						paths: polygons,
						geodesic: false,
						strokeColor: '#FF0000',
						strokeOpacity: 1.0,
						strokeWeight: 2,
						fillColor: '#FF0000',
						fillOpacity: 0.15,
						editable: false,
						map: map,
						visible: false
					});
					nta.id = id;
					nta.title = title;
					
					google.maps.event.addListener(nta, 'mouseover', highlightNTA);
					google.maps.event.addListener(nta, 'mouseout', dehighlightNTA);
					google.maps.event.addListener(nta, 'click', floatInfo);
					NTA[id] = nta;
				});
			}
		});
}




//============== NTA plot function ===================

function plotNTAs()
{
	clusters = [['MN40', 'MN33', 'MN09', 'MN25', 'MN24', 'MN19', 'MN13', 'MN17', 'MN99'],
	['MN31', 'MN21', 'MN20', 'MN14', 'MN15', 'MN12'],
	['MN04', 'MN06', 'MN01', 'MN03', 'MN34', 'MN36', 'MN35'],
	['MN32', 'MN28', 'MN27', 'MN23', 'MN22', 'MN50', 'MN11']];
	
	color = ['#FF0000',  '#00FF00',  '#000000', '#0000FF' ];
	hideNTAs();
	c = $('select[name=cluster]')[0].selectedIndex;

	if (c < 4) {
		cls = clusters[c];
		for (var k in cls)
		{
			// plot the NTA on Maps
			n = NTA[cls[k]];
			n.setOptions( {'strokeColor': color[c], 'fillColor': color[c] } );
			n.setVisible( true );
		}
	} else if (c == 4) {
		for (i in clusters) {
			cls = clusters[i];
			for (k in cls) {
				n = NTA[cls[k]];
				n.setOptions( {'strokeColor': color[i], 'fillColor': color[i] } );
				n.setVisible( true );
			}
		}
	} else if (c == 5) {
		hideNTAs()
	}
}


function hideNTAs()
{
	for (i in NTA)
	{
		NTA[i].setVisible(false);
	}
}


function plotHeatMap()
{
	// if (heatMap != null)
		// heatMap.setMap(null);
		
	day = parseFloat( $('input[name=day]').val() );
	hour = parseFloat( $('input[name=hour]').val() );
	hour_idx = 0;
	if (day >=1 && day <= 31)
		if (hour >= 0 && hour <= 23)
			hour_idx = hour + (day-1) * 24;
		else
			alert('Wrong hour given');
	else
		alert('Wrong day given');
	
	trafType = $('select[name=traffictype]')[0].selectedOptions[0].text;
	$.ajax({
		type: 'get',
		url: 'heatmap',
		datatype: 'json',
		data: {	hour: hour_idx,
					type: trafType },
		success: function( result ) {
			points = JSON.parse(result);
			var heatMapdata = []
			for (i in points) {
				tmpD = { location: new google.maps.LatLng( points[i][0], points[i][1] ),
								weight: points[i][2] };
				heatMapdata.push( tmpD );
			}
			
			if (heatMap == null) {
				heatMap = new google.maps.visualization.HeatmapLayer({
						data: heatMapdata,
						map: map
					});
			} else {
				heatMap.setData(heatMapdata);
				// heatMap.setMap(map);
			}
		}
	});
}


function plotNextHourHeatMap()
{
	day = parseFloat( $('input[name=day]').val() );
	hour = parseFloat( $('input[name=hour]').val() );
	if (hour < 23) {
		nhour = hour+1;
		$('input[name=hour]').val( nhour );
	} else {
		nhour = 0;
		nday = day + 1;
		$('input[name=hour]').val( nhour );
		$('input[name=day]').val( nday );
	}
	
	// plot the new heat map
	plotHeatMap();
}




function plotPickupCluster() {
	$.ajax({
		type: 'get',
		url: 'pickupgrids',
		success: function(data) {
			res = JSON.parse(data);
			gridsPlot( res );
			message('Pickup clusters');
		}
	});
}


function plotDropoffCluster() {
	$.ajax({
		type: 'get',
		url: 'dropoffgrids',
		success: function(data) {
			res = JSON.parse(data);
			gridsPlot( res );
			message('Dropoff clusters');
		}
	});
}


function gridsPlot( data ) {
	color = ['#FF0000',  '#00FF00',  '#000000', '#0000FF', '#FFFF00', '#800080' ];
	for (i in data) {
		sw = data[i][0];
		ne = data[i][1];
		col = color[ data[i][2] ];
		ply = new google.maps.Rectangle({
			strokeColor: col,
			strokeOpacity: 0.8,
			strokeWeight: 2,
			fillColor: col,
			fillOpacity: 0.35,
			map: map,
			bounds: new google.maps.LatLngBounds(
			  new google.maps.LatLng(sw[0], sw[1]),
			  new google.maps.LatLng(ne[0], ne[1]))
		  });
	}
}


//============== NTA event handler ===================


function highlightNTA( event ) {
	this.setOptions( {'fillOpacity': 0.9} );
	message( 'ID: ' + this.id + '    Title: ' + this.title );
	$('#'+this.id).css('background-color', '#90EE90');
}


function dehighlightNTA( event ) {
	this.setOptions( {'fillOpacity': 0.15 } );
	message( '' );
	$('#'+this.id).css('background-color', '#D8BFD8');
}

function floatInfo( event ) {
	ntaid = this.id;
	ntatitle = this.title;
	$.ajax({
		type: 'get',
		url: 'nta',
		datatype: 'jason',
		data: this.id,
		success: function(ret_d) {
			n = JSON.parse(ret_d);
			t = $('<div id=' + ntaid + ' class="floatpanel">'+ntaid+'</div>').draggable();
			t.nid = ntaid;
			t.title = ntatitle;
			tt = t.append('<table> </table>');
			tt.append( '<tr><td>age</td><td>' + n.age.toFixed(2) + '</td></tr>' );
			tt.append( '<tr><td>income</td><td>' + n.income.toFixed(2) + '</td></tr>' );
			tt.append( '<tr><th>R</th><th>%</th></tr>' );
			raceFields = ['white', 'black', 'Native', 'Asian', 'Hawaiian', 'Other', 'Two+'];
			for (i in raceFields) {
				tt.append('<tr><td>'+ raceFields[i] +'</td><td>'+n.race[i].toFixed(2) + '</td></tr>');
			}
			
			$('#map-canvas').append(t);
			
			// double click to remove the elements
			t.dblclick( function() {
				$(this).trigger('mouseleave');
				$(this).remove();
			});
			
			// show title and ID when hover
			t.bind( 'mouseenter', function() {
				nid = this.id;
				message( 'ID: ' + nid + '    Title: ' + NTA[nid].title);
				NTA[nid].setOptions( {'fillOpacity': 0.9} );
			});
			
			t.bind( 'mouseleave', function() {
				message( '' );
				NTA[this.id].setOptions( {'fillOpacity': 0.15} );
			});
		}
	});
}



//============== message panel interaction function ===================

function message( content )
{
	$('#line').html( content )
}


function avg( a ) {
	l = a.length;
	sum = 0;
	for (var i = 0; i < l; i++)
		sum += a[i];
	return sum / l;
}