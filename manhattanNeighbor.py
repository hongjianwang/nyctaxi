from shapely.geometry import Point, MultiPolygon
from xml.etree.ElementTree import parse
#from retrieveGeo import generateFileName
#import os


import os


INPUT_FILE_DIR = "../../dataset/"
#INPUT_FILE = "../data/manh_sample_10m.txt"
#INPUT_FILE = "../data/manh-150k-map"
INPUT_FILE = "../../dataset/manh_sample_10m_id"
OUTPUT_FILE_DIR = "../../dataset/"
OUTPUT_FILE = "res/manh_sample_10m_id_neiighborhood"
DISCREPANCY = 60 #the trip time in sec and the dropoff time - pickup time differs in 60 seconds
OLS_RATE_CODE = 1
if not os.path.exists(OUTPUT_FILE_DIR):
    os.mkdir(OUTPUT_FILE_DIR)

qualified_trip = 0
manh_to_manh_trip = 0
manh_to_outside_trip = 0
outside_to_manh_trip = 0
outside_to_outside_trip = 0


def queryIDFromElement( ele ):
    """Return the ID of neighborhood from element object"""
    return int(ele.find('id').text)

class Neighborhood:

    def __init__( self, Id = -1, title = 'others', polygon = None ):
        self.id = Id
        self.title = title
        self.polygon = polygon
        self.outbound = {}

    def createFromNYCNTA( self, ele ):
        """Generate an Neighborhood instance from the NTA.xml element"""
        self.id = int(ele.find('id').text[2:])
        self.title = ele.find('title').text
        coords = []
        for polygon in ele.findall('polygon'):
            ring = []
            for e in list(polygon):
                lon = float(e.find('lon').text)
                lat = float(e.find('lat').text)
                ring.append( (lon, lat) )
            coords.append( (ring, []) )

        self.polygon = MultiPolygon( coords )

        return self

    def contains( self, point ):
        return self.polygon.contains(point)


class Manhattan:

    #IDsPath = 'Neighborhood.txt'
    boundaryPath = 'data/NTA.xml'
    neighborhoods = {}  # all the neighborhoods in Manhattan

    __nMan_to_Man_trips = 0 #static 
    __nMan_to_Out_trips = 0
    __nOut_to_Man_trips = 0
    __nOut_to_Out_trips = 0

    @classmethod
    def getStatics(self):
        return self.__nMan_to_Man_trips, self.__nMan_to_Out_trips, self.__nOut_to_Man_trips, self.__nOut_to_Out_trips

    @classmethod
    def getNeighborhoods( cls ): 

        #manhattan = []
        #with open(cls.IDsPath) as manhin:
        #    for line in manhin:
        #        manhattan.append(int(line))

        tree = parse(cls.boundaryPath)

        for node in list(tree.getroot()):
            #if queryIDFromElement(node) in manhattan:
            neighborhood = Neighborhood()
            neighborhood.createFromNYCNTA( node )
            cls.neighborhoods[neighborhood.id] = neighborhood


        cls.oth = Neighborhood()
        cls.neighborhoods[-1] = cls.oth
        return cls.neighborhoods, cls.oth



    @classmethod
    def tripFlow(cls, pickup_longi, pickup_lati, dropoff_longi, dropoff_lati):


        pickup = Point(float(pickup_longi), float(pickup_lati))

        dropoff = Point(float(dropoff_longi), float(dropoff_lati))


        srcflag = False
        dstflag = False
        src_id = "0"
        dst_id = "0"
        for n in cls.neighborhoods:
            if not srcflag and n != -1 and cls.neighborhoods[n].contains( pickup ):
                src_id = cls.neighborhoods[n].id
                srcflag = True
            if not dstflag and n != -1 and cls.neighborhoods[n].contains( dropoff ):
                dst_id = cls.neighborhoods[n].id
                dstflag = True
            if srcflag and dstflag:
                Manhattan.__nMan_to_Man_trips  += 1
                result_file.write(str(src_id)+","+str(dst_id)+"\n")
                return 1
        result_file.write(str(src_id)+","+str(dst_id)+"\n")
        #print pickup_lati+","+pickup_longi+" to "+str(float(dropoff_lati))+","+dropoff_longi
        #print str(src_id)+","+str(dst_id)
        if not srcflag and not dstflag:
            Manhattan.__nOut_to_Out_trips += 1
        if not srcflag and dstflag:
            Manhattan.__nOut_to_Man_trips += 1
        if srcflag and not dstflag:
            Manhattan.__nMan_to_Out_trips += 1
        return 0

if __name__ == '__main__':

    for k in range(1,13):
        neighborhoods, oth = Manhattan.getNeighborhoods()
        csvfile = open( INPUT_FILE_DIR + "2013-{0:02}.trip".format(k), "rb" )

        result_file = open( OUTPUT_FILE_DIR + "2013-{0:02}.nta".format(k) , 'w')
        i = 0
        for line in csvfile :
    
            pickup_longi = line.split(',')[7]  
            pickup_lati = line.split(',')[8]
    
            dropoff_longi = line.split(',')[9] 
            dropoff_lati = line.split(',')[10]
    
            i = i + 1
    
            if i%10000 == 0:
                print i
    
            res = Manhattan.tripFlow(pickup_longi, pickup_lati, dropoff_longi, dropoff_lati)
    
        result_file.close() 
    
    
        nMM, nMO, nOM, nOO = Manhattan.getStatics()
        statistics_file = open( OUTPUT_FILE_DIR +'statistic_result_{0}'.format(k) , 'w')
        statistics_file.write("number of lines: %s\n" %(str(i-1)))
        statistics_file.write("n manh to manh trips: %s\n" %(str(nMM)))
        statistics_file.write("n manh to outside trips: %s\n" %(str(nMO)))
        statistics_file.write("n outside to manh trips: %s\n" %(str(nOM)))
        statistics_file.write("n outside to outside trips: %s\n" %(str(nOO)))
        statistics_file.close()

