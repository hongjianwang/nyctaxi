import numpy
from time import strptime


GRAN = 7;      # 1 for overall, 7 for day of week, 24 for hour of day
SPEED_OR_COVAR = False      # True for SPEED analysis, False for Covariance analysis
        
class Trip:
    filePath = '..\\..\\dataset\\NYC_Taxi\\trip_Jan.csv'
    """Each line is a trip with following information:
        medallion (0), hack, vendor, rate_code, store_forward_flag, pickup_time(5),
        dropoff_time(6), passenger_cnt, trip_time_in_secs(8), trip_distance(9),
        pickup_lon(10), pickup_lat(11), dropoff_lon(12), dropoff_lat(13), payment_type,
        fare_amount(15), surcharge, mta_tax, tip, tolls, total_amount"""
        
    time = [ [] for i in range(GRAN) ]
    distance = [ [] for i in range(GRAN) ]
    fare = [ [] for i in range(GRAN) ]
    speed = [ [] for i in range(GRAN) ]
    
    def __init__( self, line ):
        ls = line.split(',')
        # self.id = int(ls[0])
        t = int( ls[8] )
        if t > 0:
            if GRAN != 1:
                pickupT = strptime( ls[5], '%Y-%m-%d %H:%M:%S' )
                if GRAN == 7:
                    ind = pickupT.tm_wday
                elif GRAN == 24:
                    ind = pickupT.tm_hour
            else:
                ind = 0
            
            d = float( ls[9] )
            Trip.time[ind].append( t )
            Trip.distance[ind].append( d )
            Trip.fare[ind].append( float( ls[15] ) )
            Trip.speed[ind].append( d / t * 3600.0 )
                   
        
if __name__ == '__main__':
    
    fin = open( Trip.filePath )
    
    for l in fin:
        t = Trip(l)
    
    
    for ind, val in enumerate(Trip.time):
        if SPEED_OR_COVAR:
            D = sum(Trip.distance[ind])
            T = sum(Trip.time[ind])
            S = D / T * 3600.0
        
        
            ms = numpy.mean(Trip.speed[ind])
            vs = numpy.var(Trip.speed[ind])
            print S, ms, vs
        else:
            ts = numpy.corrcoef( Trip.time[ind], Trip.distance[ind] )
            tf = numpy.corrcoef( Trip.time[ind] , Trip.fare[ind] )
            
            print ts[0,1], '\t', tf[0,1]